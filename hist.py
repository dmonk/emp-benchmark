import numpy as np
from matplotlib import pyplot as plt

bins = np.loadtxt("hist.txt", delimiter=",", dtype=int)
plt.scatter(bins[:,0], bins[:,1])
#plt.yscale('log')
#plt.ylim(0.1,10000)
plt.show()
