library ieee;
use ieee.std_logic_1164.all;
 
package ttc_pg_pkg is
  
  type ttc_pg_ctrl_type is
  record
  clk         : std_logic;
  we          : std_logic;
  waddr       : std_logic_vector( 9 downto 0);
  wdata       : std_logic_vector(31 downto 0);
  en          : std_logic;
  cyclic      : std_logic;
  end record;

  type ttc_pg_stat_type is
  record
  rdata       : std_logic_vector(31 downto 0);
  busy        : std_logic;
  eop         : std_logic; 
  end record;  
  
    
  constant ttc_pg_ctrl_null : ttc_pg_ctrl_type:=
  (
  clk         => '0',
  we          => '0' ,
  waddr       => (others => '0'),
  wdata       => (others => '0'),
  en          => '0' ,
  cyclic      => '0'              
  );

  constant ttc_pg_stat_null : ttc_pg_stat_type:=
  (
  rdata       => (others => '0'),
  busy        => '0' ,
  eop         => '0'              
  );  
  
end ttc_pg_pkg;
   
package body ttc_pg_pkg is
end ttc_pg_pkg;
