library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.stim_pkg.all;
use work.ttc_codec_pkg.all;

entity ttc_codec_tb is
end ttc_codec_tb;

architecture sim of ttc_codec_tb is


signal tb_tx_rst          : std_logic:= '1'; 
signal tb_tx_clk160       : std_logic:='0';  
signal tb_tx_info         : ttc_info_type;
signal tb_tx_data         : std_logic:= '0'; 
signal tb_tx_clken        : std_logic:= '0'; 

signal tb_rx_rst          : std_logic:= '1'; 
signal tb_rx_clk160       : std_logic:='0';  
signal tb_rx_data         : std_logic:= '0'; 
signal tb_rx_info         : ttc_info_type;  
signal tb_rx_stat         : ttc_stat_type;

signal tb_stim            : stim_t(0 to 5);

begin

-- reset
tb_tx_rst <= '0' after 100ns; 
tb_rx_rst <= '0' after 100ns; 

-- clock 
clk_gen_t   (tb_tx_clk160, 6.25 ns, 0.0 ns);
clk_gen_t_dc(tb_tx_clken, 25 ns, 0.0 ns, 6.25 ns, '1');

clk_gen_t(tb_rx_clk160, 6.25 ns, 0.0 ns);

-- stimulus
stim_gen: entity work.stim_from_file
generic map
(
  filename    => "C:/work/legacy_ttc/sources/sim/ttc_codec_tb_stim.txt", 
  vector_size => (1, 1, 1, 8, 1, 32) -- vector sizes in the file
)
port map   
(
  clk_i       => tb_tx_clk160, 
  en_i        => tb_rx_stat.ready and tb_tx_clken, --'1', 
  stim_o      => tb_stim
);

-- unbundle the stim vector array --

tb_tx_info.l1accept     <= tb_stim(1)(0); 
tb_tx_info.brc_strobe   <= tb_stim(2)(0);
tb_tx_info.brc_t2       <= tb_stim(3)(7 downto 6);
tb_tx_info.brc_d4       <= tb_stim(3)(5 downto 2);
tb_tx_info.brc_e        <= tb_stim(3)(1);
tb_tx_info.brc_b        <= tb_stim(3)(0);

tb_tx_info.adr_strobe   <= tb_stim(4)(0);
tb_tx_info.adr_a14      <= tb_stim(5)(31 downto 18);
tb_tx_info.adr_e        <= tb_stim(5)(17);
tb_tx_info.adr_s8       <= tb_stim(5)(15 downto  8);
tb_tx_info.adr_d8       <= tb_stim(5)( 7 downto  0);

tb_tx_info.clk40_gated  <= '0';

--==========================--
uut_enc: entity work.ttc_encoder
--==========================--
port map
(
  reset_i        => tb_tx_rst,
  clk160_i       => tb_tx_clk160,
  clken_i        => tb_tx_clken, 
  info_i         => tb_tx_info,
  data_o         => tb_tx_data 
);
 --==========================--



 --==========================--
uut_dec: entity work.ttc_decoder
 --==========================--
 port map
 (
   rst_i         => tb_rx_rst   ,
   clk160_i      => tb_rx_clk160,
   data_i        => tb_rx_data  ,
   info_o        => tb_rx_info  , 
   stat_o        => tb_rx_stat 
 );
 --==========================--


tb_rx_data <= tb_tx_data;

end architecture;