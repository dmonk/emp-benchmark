library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.stim_pkg.all;
use work.ttc_pg_pkg.all;
use work.ttc_codec_pkg.all;
 
entity ttc_pg_tb is
end ttc_pg_tb;

architecture sim of ttc_pg_tb is


signal tb_pg_ctrl         : ttc_pg_ctrl_type:= ttc_pg_ctrl_null;
signal tb_pg_stat         : ttc_pg_stat_type:= ttc_pg_stat_null;

signal tb_tx_rst          : std_logic:='1'; 
signal tb_stim_clk        : std_logic:='0';  
signal tb_tx_clk160       : std_logic:='0';  
signal tb_tx_clken        : std_logic:='0';  
signal tb_tx_data         : std_logic:='0'; 
signal tb_tx_info         : ttc_info_type;  

signal tb_rx_rst          : std_logic:='1'; 
signal tb_rx_clk160       : std_logic:='0';  
signal tb_rx_data         : std_logic:='0'; 
signal tb_rx_info         : ttc_info_type;  
signal tb_rx_stat         : ttc_stat_type;

signal tb_stim            : stim_t(0 to 4):= (others => (others => '0'));
  
begin

-- reset
tb_tx_rst <= '0' after 100ns; 
tb_rx_rst <= '0' after 100ns; 

-- clock 
clk_gen_t(tb_stim_clk,  25.0 ns, 0.0 ns);
clk_gen_t(tb_tx_clk160, 6.25 ns, 0.0 ns);
clk_gen_t_dc(tb_tx_clken, 25 ns, 0.0 ns, 6.25 ns, '1');
clk_gen_t(tb_rx_clk160, 6.25 ns, 0.0 ns);

-- stimulus
stim_gen: entity work.stim_from_file
generic map
(
  filename    => "C:/work/legacy_ttc/sources/sim/ttc_pg_tb_stim.txt", 
  vector_size => (1, 1, 1, 10, 32) -- vector sizes in the file
)
port map   
(
  clk_i       => tb_stim_clk, 
  en_i        => tb_rx_stat.ready, --'1', 
  stim_o      => tb_stim
);

-- unbundle the stim vector array --
tb_pg_ctrl.en     <= tb_stim(0)( 0);  
tb_pg_ctrl.cyclic <= tb_stim(1)( 0);  
tb_pg_ctrl.we     <= tb_stim(2)( 0);
tb_pg_ctrl.waddr  <= tb_stim(3)( 9 downto 0);
tb_pg_ctrl.wdata  <= tb_stim(4)(31 downto 0);
tb_pg_ctrl.clk    <= tb_stim_clk;


--==========================--
uut_pg: entity work.ttc_pg 
--==========================--
port map
(
  rst_i              => tb_tx_rst,  
  clk160_i           => tb_tx_clk160,
  clken_i            => tb_tx_clken, 
  pg_ctrl_i          => tb_pg_ctrl,
  pg_stat_o          => tb_pg_stat,
  pg_o               => tb_tx_info
);
--==========================--



--==========================--
uut_enc: entity work.ttc_encoder
--==========================--
port map
(
  reset_i        => tb_tx_rst,
  clk160_i       => tb_tx_clk160,
  clken_i        => tb_tx_clken, 
  info_i         => tb_tx_info,
  data_o         => tb_tx_data 
);
 --==========================--



 --==========================--
uut_dec: entity work.ttc_decoder
 --==========================--
 port map
 (
   rst_i         => tb_rx_rst   ,
   clk160_i      => tb_rx_clk160,
   data_i        => tb_rx_data  ,
   info_o        => tb_rx_info  , 
   stat_o        => tb_rx_stat 
 );
 --==========================--


tb_rx_data <= tb_tx_data; -- looping back the encoder output to the decoder input

end architecture;