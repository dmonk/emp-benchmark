library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library std;
use std.textio.all; 

use work.stim_pkg.all;

entity stim_from_file is
generic (
  filename    : string := "c:/work/muctpi/common/sim/stim.txt";
  vector_size : size_t := (4, 1, 1) -- define the hex vector size in the stim file
); 
port
(
  clk_i  : in  std_logic;
  en_i   : in  std_logic;
  stim_o : out stim_t:= (others => (others => '0'))
);
end stim_from_file;

architecture Behavioral of stim_from_file is
  constant param_nbr : integer:= vector_size'length;
begin
reading : process
  file     infile    : text is in filename; 
  variable inline    : line;
  variable hex_value : std_ulogic_vector(63 downto 0); -- string of 16 characters/digits
  variable bin_value : std_ulogic;
  variable line_number, line_position : integer := 0;
  variable skip, eof, sof : boolean:= false;

begin
  
  --wait until falling_edge(clk_i);
  wait until rising_edge(clk_i);
  skip := true;
  eof := false;
  
  -- initial line --
  if en_i='0' then
    while (not eof and (not sof) and skip) loop
      eof := endfile(infile);
      if not eof then 
        readline(infile, inline);
        if inline'length = 0 then -- Empty line
          skip := true;
        else
          if inline(1) = '#' then  -- Comment line
            skip := true;
          else
            skip := false;
            sof  := true;
            
            for line_position in 0 to param_nbr-1 loop
              hex_value := (others => '0');
              hread(inline, hex_value(vector_size(line_position)-1 downto 0));
              stim_o(line_position) <= std_logic_vector(hex_value);--  case line_position is      
            end loop;
             
          end if;
        end if;
      end if;
    end loop;
  
  -- all other lines --
  elsif en_i = '1' then 
    -- Skip over empty liines or one with comments.
    skip := true;
    eof := false;
    while (not eof and skip) loop
      eof := endfile(infile);
      if not eof then 
        readline(infile, inline);
        if inline'length = 0 then -- Empty line
          skip := true;
        else
          if inline(1) = '#' then  -- Comment line
            skip := true;
          else
            skip := false;
          end if;
        end if;
      end if;
    end loop;
   
    if (not eof) then
      for line_position in 0 to param_nbr-1 loop
        hex_value := (others => '0');
        hread(inline, hex_value(vector_size(line_position)-1 downto 0));
        stim_o(line_position) <= std_logic_vector(hex_value);--  case line_position is      
      end loop;
    end if;
  end if; 
        
   
end process reading;

end Behavioral;