mkdir [my_working_dir]

cd [my_working_dir]

git clone https://gitlab.cern.ch/ttc/legacy_ttc.git

cd legacy_ttc

git checkout v2.0
