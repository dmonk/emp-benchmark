# Extensible, modular firmware framework for phase-2 upgrades #

This repository contains the common firmware framework - i.e. the input/output buffers with underlying control bus and clocking infrastructure - that has been developed for a wide range of FPGAs related to the phase-2 upgrades.

The user's guide can be found in the wiki attached to this repository; that guide includes instructions on how to integrate custom "physics algorithm" payloads into the framework. This README has been written as a brief guide for people developing the framework firmware.


## Directory structure ##

The repository has 3 main subdirectories:

* `projects` : Contains top-level `.dep` and VHDL files defining entities and parameter values that are unique to a particular project.
* `boards` : Contains board-specific source code and constraints (one subdirectory per board); currently, there are designs for three different boards (as well as modelsim-based simulation):

  * HiTech Global K800 (`boards/k800`)
  * MPUltra PCIe board (`boards/mpultra`)
  * Xilinx VCU118 (`boards/vcu118`)
  * Daughter Cards for Serenity (`boards/serenity`)

* `components` : Contains source code that is applicable to many different boards

| Component name | Description                                                                                         |
| -------------- | --------------------------------------------------------------------------------------------------- |
| `ctrl`         | Resets and clock configuration                                                                      |
| `datapath`     | Modules that instantiate the I/O buffers, and link-related firmware                                 |
| `framework`    | VHDL packages defining various custom types that are used by several other components               |
| `info`         | Set of read-only registers containing values of various build-time constants (e.g. version numbers) |
| `ttc`          | Timing and control interface                                                                        |
| `utils`        | Miscellaneous infrastructural VHDL entities                                                         |

Each of the individual board/component directories has the following internal structure:

| Subdirectory   | Contents                                                                         |
|----------------|----------------------------------------------------------------------------------|
| `addr_table`   | uHAL address table files                                                         |
| `firmware/hdl` | Firmware source code (e.g. Verilog/VHDL)                                         |
| `firmware/cfg` | `.dep` files and `.tcl` scripts that should be run when creating vivado projects |
| `firmware/cgn` | IP core configuration files                                                      |
| `firmware/ucf` | Constraints files                                                                |


## Quick start instructions for developers ##

Make sure that the [Prerequisites](#prerequisites) are satisfied.

### Non-simulation workflow

##### Step 1: Setup the work area

```
ipbb init p2fwk-work
cd p2fwk-work
ipbb add git https://:@gitlab.cern.ch:8443/p2-xware/firmware/emp-fwk.git
ipbb add git https://gitlab.cern.ch/ttc/legacy_ttc.git -b v2.1
ipbb add git https://github.com/ipbus/ipbus-firmware -b v1.8
```

##### Step 2: Create an ipbb project area

There's a wide range of development/example designs:

| Description                                                                 | Project path                              | Top `.dep` file name  |
| --------------------------------------------------------------------------- | ----------------------------------------- | --------------------- |
| MPultra: Buffers for 44 channels, with 'null algo' payload                  | `projects/examples/mpultra`               | `top.dep` (default)   |
| MPultra: Buffers for 4 channels, with 'null algo' payload                   | `projects/examples/mpultra`               | `top_min.dep`         |
| MPultra: Buffers for 72 channels, with 'null algo' payload                  | `projects/examples/mpultra`               | `top_full.dep`        |
| K800: Buffers for 44 channels, with 'null algo' payload                     | `projects/examples/k800`                  | `top.dep` (default)   |
| K800: Buffers for 4 channels, with 'null algo' payload                      | `projects/examples/k800`                  | `top_min.dep`         |
| K800: Buffers for 72 channels, with 'null algo' payload                     | `projects/examples/k800`                  | `top_full.dep`        |
| VCU118: Buffers for 44 channels, with 'null algo' payload                   | `projects/examples/vcu118`                | `top.dep` (default)   |
| VCU118: Buffers for 4 channels, with 'null algo' payload                    | `projects/examples/vcu118`                | `top_min.dep`         |
| VCU118: Buffers for 112 channels, with 'null algo' payload                  | `projects/examples/vcu118`                | `top_full.dep`        |
| Serenity KU115 (TM1/BM1): Buffers for 44 channels, with 'null algo' payload | `projects/examples/serenity/dc_ku115`     | `am1/top.dep`         |
| Serenity KU115 (TM1/BM1): Buffers for 4 channels, with 'null algo' payload  | `projects/examples/serenity/dc_ku115`     | `am1/top_min.dep`     |
| Serenity KU115 (TM1/BM1): Buffers for 72 channels, with 'null algo' payload | `projects/examples/serenity/dc_ku115`     | `am1/top_full.dep`    |
| Serenity KU115 (SO1): Buffers for 44 channels, with 'null algo' payload     | `projects/examples/serenity/dc_ku115`     | `so1/top.dep`         |
| Serenity KU115 (SO1): Buffers for 4 channels, with 'null algo' payload      | `projects/examples/serenity/dc_ku115`     | `so1/top_min.dep`     |
| Serenity KU115 (SO1): Buffers for 72 channels, with 'null algo' payload     | `projects/examples/serenity/dc_ku115`     | `so1/top_full.dep`    |
| Serenity KU15P (SM1 v1): Buffers for 4 channels, with 'null algo' payload   | `projects/examples/serenity/dc_ku15p`     | `sm1_v1/top_min.dep`  |
| Serenity KU15P (SM1 v1): Buffers for 72 channels, with 'null algo' payload  | `projects/examples/serenity/dc_ku15p`     | `sm1_v1/top.dep`      |
| Serenity KU15P (SM1 v2): Buffers for 4 channels, with 'null algo' payload   | `projects/examples/serenity/dc_ku15p`     | `sm1_v2/top_min.dep`  |
| Serenity KU15P (SM1 v2): Buffers for 72 channels, with 'null algo' payload  | `projects/examples/serenity/dc_ku15p`     | `sm1_v2/top.dep`      |
| Serenity KU15P (SO1 v1): Buffers for 4 channels, with 'null algo' payload   | `projects/examples/serenity/dc_ku15p`     | `so1_v1/top_min.dep`  |
| Serenity KU15P (SO1 v1): Buffers for 72 channels, with 'null algo' payload  | `projects/examples/serenity/dc_ku15p`     | `so1_v1/top.dep`      |
| Serenity VU7P (SO1): Buffers for 8 channels, with 'null algo' payload       | `projects/examples/serenity/dc_vu7p`      | `top_min.dep`         |
| Serenity VU7P (SO1): Buffers for 72 channels, with 'null algo' payload      | `projects/examples/serenity/dc_vu7p`      | `top.dep`             |
| MPultra: IPbus example design                                               | `projects/development/ipbus_pcie_test`    | `top_mpultra.dep`     |
| K800: IPbus example design                                                  | `projects/development/ipbus_pcie_test`    | `top_k800.dep`        |
| VCU118: IPbus example design                                                | `projects/development/ipbus_pcie_test`    | `top_vcu118.dep`      |

The project area for any one of these designs can be created as follows:

```
ipbb proj create vivado my_projects_name emp-fwk:<project_path> -t <top_depfile_name>
cd proj/my_projects_name
```

For example, for the 44-channel MPUltra 'null algo' design:

```
ipbb proj create vivado mpu_null_algo emp-fwk:projects/examples/mpultra
cd proj/mpu_null_algo
```

##### Step 3: Setup, build and package the bitfile

```
ipbb vivado project
ipbb vivado synth -j4 impl -j4
ipbb vivado package
```


### Simulation workflow

The repository contains a Questasim-based simulation for the same FPGA as is used on the MPUltra (KU115 flvb1760-2-e). When running this simulation, you can control it using the same software as is used to control firmware running in real FPGAs. However, for the simulation IPbus transactions must be sent and received over UDP rather than PCIe, and so a `ipbusudp-2.0` URI must be used in your connections file.

##### Step 1: Setup the work area

Same as in the non-simulation workflow.

##### Step 2: Create the ipbb project area

```
ipbb proj create sim sim emp-fwk:projects/examples/sim -t top_udp.dep
cd proj/sim
```

##### Step 3: Create the Xilinx simulation libraries

*N.B.* The Xilinx simulation libraries can be shared between different ipbb projects and work areas. By default they are written to `${HOME}/.xilinx_sim_libs`, but they can be written to another directory by defining the environment variable `IPBB_SIMLIB_BASE` before running these two commands, or by adding the `-x` option to end of each command (e.g. `-x /path/to/simlib_directory`).

```
ipbb sim setup-simlib
ipbb sim ipcores
```

##### Step 4: Build the foreign language interface and project

```
ipbb sim fli-udp
ipbb sim make-project
```

##### Step 5: Run the simulation

You should start questasim through the local `vsim` script, using one of the following commands:

 * GUI mode: `./vsim work.top`
 * Command-line mode: `.vsim -c work.top`

You can then start the simulation by typing `run -all` in the resulting prompt.

The simulation is accessible through port 50001, so you can communicate with it using uHAL URI `ipbusudp-2.0://localhost:50001` or `chtcp-2.0://localhost:10203?target=localhost:50001`


##### Alternative method: Including Ethernet simulation

If you need to include the simulation of the IPbus Ethernet interface (not recommended for typical use), then the `top_udp.dep` should be replaced by `top_eth.dep` in step 1, the `fli-udp` subcommand should be run instead of `fli-eth` in step 3, and the following command should be run at the end of step 4:
```
ipbb sim virtualtap
```
This command creates a new virtual Ethernet interface with name `tap0` and assigns it IP address `192.168.201.1` by default; custom names and IP addresses can be specified by supplying further arguments.


## Prerequisites ##

 * Xilinx Vivado: 2018.3
 * Python 2.7 - available on most linux distributions, natively or as [miniconda](https://conda.io/miniconda.html) distribution.
 * ipbb: v0.5.2 or greater - the [IPbus Builder Tool](https://github.com/ipbus/ipbb). Note: a single `ipbb` installation is not work area specific and suffices for any number of projects.
 
```
curl -L https://github.com/ipbus/ipbb/archive/v0.5.2.tar.gz | tar xvz
source ipbb-0.5.2/env.sh
```

