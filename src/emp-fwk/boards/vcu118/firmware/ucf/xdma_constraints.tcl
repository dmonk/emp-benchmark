# PCIe location constraints
set_property LOC PCIE40E4_X1Y0 [get_cells -hierarchical -filter {NAME =~infra/dma/*pcie_4_0_pipe_inst/pcie_4_0_e4_inst}]
set_property USER_CLOCK_ROOT X5Y0 [get_nets -of_objects [get_pins -hierarchical -filter NAME=~infra/dma/*bufg_gt_sysclk/O]]
set_property USER_CLOCK_ROOT X5Y0 [get_nets -of_objects [get_pins -hierarchical -filter NAME=~infra/dma/*/phy_clk_i/bufg_gt_intclk/O]]
set_property USER_CLOCK_ROOT X5Y0 [get_nets -of_objects [get_pins -hierarchical -filter NAME=~infra/dma/*/phy_clk_i/bufg_gt_coreclk/O]]

