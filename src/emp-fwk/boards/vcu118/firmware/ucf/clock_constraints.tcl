
# 300 MHz external oscillator
create_clock -period 3.333 -name osc_clk [get_ports osc_clk_p]


# Clock rate setting for refclks (kind of arbitrary, 156.25MHz here)  -- should be 100MHz
set lRefClkSize [llength [get_ports {refclkn[*]}]]
for {set i 0} {$i < $lRefClkSize} {incr i} {
    create_clock -name refclk_$i -period 6.4 [get_ports refclkn[$i]]
}

