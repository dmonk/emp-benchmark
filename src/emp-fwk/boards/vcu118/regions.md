# EMP datapath regions - VCU118

The position of each EMP datapath region in terms of clock regions and quads is shown in the diagram below:

|     |         X0               | X1 | X2 | X3 | X4 |          X5              |
| --- | :----------------------: | -- | -- | -- | -- | :----------------------: |
| Y14 | Region 14 (~~quad 133~~) |    |    |    |    | Region 13 (quad 233)     |
| Y13 | Region 15 (~~quad 132~~) |    |    |    |    | Region 12 (quad 232)     |
| Y12 | Region 16 (~~quad 131~~) |    |    |    |    | Region 11 (quad 231)     |
| Y11 | Region 17 (~~quad 130~~) |    |    |    |    | Region 10 (~~quad 230~~) |
| Y10 | Region 18 (~~quad 129~~) |    |    |    |    | Region 9 (~~quad 229~~)  |
|     |      *SLR CROSSING*      |    |    |    |    |      *SLR CROSSING*      |
| Y9  | Region 19 (~~quad 128~~) |    |    |    |    | Region 8 (~~quad 228~~)  |
| Y8  | Region 20 (quad 127)     |    |    |    |    | Region 7 (quad 227)      |
| Y7  | Region 21 (quad 126)     |    |    |    |    | Region 6 (quad 226)      |
| Y6  | Region 22 (quad 125)     |    |    |    |    | Region 5 (quad 225)      |
| Y5  | Region 23 (~~quad 124~~) |    |    |    |    | Region 4 (quad 224)      |
|     |      *SLR CROSSING*      |    |    |    |    |      *SLR CROSSING*      |
| Y4  | Region 24 (~~quad 123~~) |    |    |    |    | Region 3 (~~quad 223~~)  |
| Y3  | Region 25 (quad 122)     |    |    |    |    | Region 2 (~~quad 222~~)  |
| Y2  | Region 26 (quad 121)     |    |    |    |    | Region 1 (~~quad 221~~)  |
| Y1  | Region 27 (quad 120)     |    |    |    |    | Region 0 (~~quad 220~~)  |
| Y0  |        *Reserved*        |    |    |    |    |  *PCIe (~~quad 219~~)*   |
