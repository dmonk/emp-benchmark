# EMP datapath regions - HiTech Global K800

The position of each EMP datapath region in terms of clock regions and quads is shown in the diagram below:

|    |            X0            | X1 | X2 | X3 | X4 |           X5            |
| -- | :----------------------: | -- | -- | -- | -- | :---------------------: |
| Y9 | Region 9 (~~quad 133~~)  |    |    |    |    | Region 8 (~~quad 233~~) |
| Y8 | Region 10 (~~quad 132~~) |    |    |    |    | Region 7 (quad 232)     |
| Y7 | Region 11 (~~quad 131~~) |    |    |    |    | Region 6 (quad 231)     |
| Y6 | Region 12 (no quad)      |    |    |    |    | Region 5 (quad 230)     |
| Y5 | Region 13 (no quad)      |    |    |    |    | Region 4 (quad 229)     |
|    |      *SLR CROSSING*      |    |    |    |    |     *SLR CROSSING*      |
| Y4 | Region 14 (quad 128)     |    |    |    |    | Region 3 (quad 228)     |
| Y3 | Region 15 (quad 127)     |    |    |    |    | Region 2 (quad 227)     |
| Y2 | Region 16 (quad 126)     |    |    |    |    | Region 1 (quad 226)     |
| Y1 | Region 17 (no quad)      |    |    |    |    | Region 0 (quad 225)     |
| Y0 |        *Reserved*        |    |    |    |    |    *PCIe (quad 224)*    |
