-- emp device declaration
--
-- Defines constants for the whole device
--
-- Alessandro Thea, April 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.emp_framework_decl.all;

-------------------------------------------------------------------------------
package emp_device_decl is

  constant BOARD_DESIGN_ID : std_logic_vector(7 downto 0) := X"01";

  constant N_REGION       : integer := 18;

  constant N_REFCLK     : integer := 10;
  constant CROSS_REGION : integer := 8;

  constant IO_REGION_SPEC : io_region_spec_array_t(0 to N_REGION - 1) := (
    0  => (io_gth, 4, -1),
    1  => (io_gth, 4, -1),
    2  => (io_gth, 4, -1),
    --3  => kIONoGTRegion,
    4  => (io_gth, 2, -1),
    5  => (io_gth, 2, -1),
    6  => (io_gth, 0, -1),
    7  => (io_gth, 0, -1),
    8  => (io_gth, 0, -1),
    --11  => kIONoGTRegion,
    --12  => kIONoGTRegion,
    13 => (io_gth, 8, -1),
    16 => (io_gth, 6, -1),
    17 => (io_gth, 6, -1),
    others  => kIONoGTRegion
  );

end emp_device_decl;
-------------------------------------------------------------------------------
