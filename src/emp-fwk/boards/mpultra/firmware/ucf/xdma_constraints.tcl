# MPUltra specific layout. The ipbus-pcie GT is located on the opposite side of the chip wrt to the PCIe block
# Lock the PCIe GTH in the top-left corner
set_property LOC GTHE3_CHANNEL_X0Y36 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[33].*gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST}]
