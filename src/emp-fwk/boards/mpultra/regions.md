# EMP datapath regions - MPUltra

The position of each EMP datapath region in terms of clock regions and quads is shown in the diagram below:

|    |            X0            | X1 | X2 | X3 | X4 |           X5            |
| -- | :----------------------: | -- | -- | -- | -- | :---------------------: |
| Y9 |        *Reserved*        |    |    |    |    |    *PCIe (quad 233)*    |
| Y8 | Region 17 (quad 132)     |    |    |    |    | Region 0 (quad 232)     |
| Y7 | Region 16 (quad 131)     |    |    |    |    | Region 1 (quad 231)     |
| Y6 | Region 15 (no quad)      |    |    |    |    | Region 2 (quad 230)     |
| Y5 | Region 14 (no quad)      |    |    |    |    | Region 3 (~~quad 229~~) |
|    |      *SLR CROSSING*      |    |    |    |    |     *SLR CROSSING*      |
| Y4 | Region 13 (quad 128)     |    |    |    |    | Region 4 (quad 228)     |
| Y3 | Region 12 (~~quad 127~~) |    |    |    |    | Region 5 (quad 227)     |
| Y2 | Region 11 (~~quad 126~~) |    |    |    |    | Region 6 (quad 226)     |
| Y1 | Region 10 (no quad)      |    |    |    |    | Region 7 (quad 225)     |
| Y0 | Region 9 (no quad)       |    |    |    |    | Region 8 (quad 224)     |
