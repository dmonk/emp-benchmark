
# PCIe CONNECTIONS
set_property PACKAGE_PIN AL8 [get_ports pcie_sys_clk_p]
set_property PACKAGE_PIN AE19 [get_ports pcie_sys_rst]
set_property PULLUP true [get_ports pcie_sys_rst]
set_property IOSTANDARD LVCMOS18 [get_ports pcie_sys_rst]
set_false_path -from [get_ports pcie_sys_rst]

set_property PACKAGE_PIN AV10 [get_ports {pcie_rxp[0]}]
set_property PACKAGE_PIN AU12 [get_ports {pcie_txp[0]}]


# EXTERNAL TTC CLOCK
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports osc_clk_n]
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports osc_clk_p]
set_property PACKAGE_PIN E20           [get_ports osc_clk_n]
set_property PACKAGE_PIN F20           [get_ports osc_clk_p]


# EXTERNAL TTC DATA
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports ttc_lvds_n]
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports ttc_lvds_p]
set_property PACKAGE_PIN G17           [get_ports ttc_lvds_n]
set_property PACKAGE_PIN H17           [get_ports ttc_lvds_p]


# HEARTBEAT LED
set_property IOSTANDARD LVCMOS18 [get_ports heartbeat_led]
set_property PACKAGE_PIN K26 [get_ports heartbeat_led]


# MGT REF CLOCKS - ASYNC
# Bank 226 refclk0 (interposer: MGT_CLK_05)
set_property PACKAGE_PIN AG8  [get_ports {refclkp[0]}]
# Bank 229 refclk0 (interposer: MGT_CLK_03)
set_property PACKAGE_PIN W8   [get_ports {refclkp[1]}]
# Bank 232 refclk0 (interposer: MGT_CLK_11)
set_property PACKAGE_PIN M10  [get_ports {refclkp[2]}]
# Bank 132 refclk0 (interposer: MGT_CLK_15)
set_property PACKAGE_PIN M33  [get_ports {refclkp[3]}]
# Bank 127 refclk0 (interposer: MGT_CLK_01)
set_property PACKAGE_PIN AF33 [get_ports {refclkp[4]}]


# MGT REF CLOCKS - SYNC 
# Bank 224 refclk1 (interposer: MGT_CLK_06)
set_property PACKAGE_PIN AK10 [get_ports {refclkp[5]}]
# Bank 226 refclk1 (interposer: MGT_CLK_04)
set_property PACKAGE_PIN AF10 [get_ports {refclkp[6]}]
# Bank 229 refclk1 (interposer: MGT_CLK_02)
set_property PACKAGE_PIN U8   [get_ports {refclkp[7]}]
# Bank 232 refclk1 (interposer: MGT_CLK_10)
set_property PACKAGE_PIN L8   [get_ports {refclkp[8]}]
# Bank 132 refclk1 (interposer: MGT_CLK_14)
set_property PACKAGE_PIN K33  [get_ports {refclkp[9]}]
# Bank 127 refclk1 (interposer: MGT_CLK_00)
set_property PACKAGE_PIN AD33 [get_ports {refclkp[10]}]
