# EMP datapath regions - Serenity KU15P daughter cards

The position of each EMP datapath region in terms of clock regions and quads is shown in the diagram below:

|     |          X0          | X1 | X2 | X3 | X4 |         X5          |
| --- | :------------------: | -- | -- | -- | -- | :-----------------: |
| Y10 | Region 10 (quad 134) |    |    |    |    | Region 9 (quad 234) |
| Y9  | Region 11 (quad 133) |    |    |    |    | Region 8 (quad 233) |
| Y8  | Region 12 (quad 132) |    |    |    |    | Region 7 (quad 232) |
| Y7  | Region 13 (quad 131) |    |    |    |    | Region 6 (quad 231) |
| Y6  | Region 14 (quad 130) |    |    |    |    | Region 5 (quad 230) |
| Y5  | Region 15 (quad 129) |    |    |    |    | Region 4 (quad 229) |
| Y4  | Region 16 (quad 128) |    |    |    |    | Region 3 (quad 228) |
| Y3  | Region 17 (quad 127) |    |    |    |    | Region 2 (quad 227) |
| Y2  |      *Reserved*      |    |    |    |    | Region 1 (quad 226) |
| Y1  |      *Reserved*      |    |    |    |    | Region 0 (quad 225) |
| Y0  |      *Reserved*      |    |    |    |    |  *PCIe (quad 224)*  |

On the right-hand side EMP intra-region channel index `m` corresponds to the `MGT*m_n` pin (where `n` is the quad number). On the left-hand side EMP intra-region channel index `m` corresponds to the `MGT*(3-m)_n` pin.
