
# EXTERNAL OSCILLATOR
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports osc_clk_n]
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports osc_clk_p]
set_property PACKAGE_PIN AU12          [get_ports osc_clk_n]
set_property PACKAGE_PIN AT13          [get_ports osc_clk_p]
