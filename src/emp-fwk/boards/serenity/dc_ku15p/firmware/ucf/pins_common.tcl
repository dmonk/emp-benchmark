
set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CONFIG_MODE SPIx8 [current_design]

# BITSTREAM CONFIG OPTIONS
set_property BITSTREAM.CONFIG.CONFIGRATE 51.0 [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 8 [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN Pulldown [current_design]

# PCIe CONNECTIONS (bank 224) 
set_property PACKAGE_PIN AL12 [get_ports pcie_sys_clk_p]   
set_property IOSTANDARD LVCMOS18 [get_ports pcie_sys_rst]
set_property PACKAGE_PIN AN27 [get_ports pcie_sys_rst]
set_property PULLUP true [get_ports pcie_sys_rst]
set_false_path -from [get_ports pcie_sys_rst]

set_property PACKAGE_PIN BB6 [get_ports {pcie_rxp[0]}]
set_property PACKAGE_PIN AW8 [get_ports {pcie_txp[0]}]
