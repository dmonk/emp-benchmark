# EXTERNAL TTC DATA
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports ttc_lvds_n]
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports ttc_lvds_p]
set_property PACKAGE_PIN AY10          [get_ports ttc_lvds_n]
set_property PACKAGE_PIN AY11          [get_ports ttc_lvds_p]


# HEARTBEAT LED
set_property IOSTANDARD  LVCMOS18 [get_ports heartbeat_led]
set_property PACKAGE_PIN AM23     [get_ports heartbeat_led]


# MGT REF CLOCKS - ASYNC
# Bank 225 refclk0 (interposer: MGT_CLK_09)
set_property PACKAGE_PIN AJ12 [get_ports {refclkp[0]}]
# Bank 227 refclk1 (interposer: MGT_CLK_11)
set_property PACKAGE_PIN AD10 [get_ports {refclkp[1]}]
# Bank 230 refclk1 (interposer: MGT_CLK_13)
set_property PACKAGE_PIN V10  [get_ports {refclkp[2]}]
# Bank 233 refclk1 (interposer: MGT_CLK_15)
set_property PACKAGE_PIN M10  [get_ports {refclkp[3]}]
# Bank 133 refclk1 (interposer: MGT_CLK_01)
set_property PACKAGE_PIN N30  [get_ports {refclkp[4]}]
# Bank 130 refclk1 (interposer: MGT_CLK_03)
set_property PACKAGE_PIN W30  [get_ports {refclkp[5]}]
# Bank 128 refclk1 (interposer: MGT_CLK_05)
set_property PACKAGE_PIN AC30 [get_ports {refclkp[6]}]
# BAnk 127 refclk1 (interposer: MGT_CLK_07)
set_property PACKAGE_PIN AE30 [get_ports {refclkp[7]}]


# MGT REF CLOCKS - SYNC
# Bank 224 refclk1 (interposer: MGT_CLK_08)
set_property PACKAGE_PIN AK10 [get_ports {refclkp[8]}]
# Bank 227 refclk0 (interposer: MGT_CLK_10)
set_property PACKAGE_PIN AE12 [get_ports {refclkp[9]}]
# Bank 230 refclk0 (interposer: MGT_CLK_12)
set_property PACKAGE_PIN W12  [get_ports {refclkp[10]}]
# Bank 233 refclk0 (interposer: MGT_CLK_14)
set_property PACKAGE_PIN N12  [get_ports {refclkp[11]}]
# Bank 133 refclk0 (interposer: MGT_CLK_00)
set_property PACKAGE_PIN P32  [get_ports {refclkp[12]}]
# Bank 130 refclk0 (interposer: MGT_CLK_02)
set_property PACKAGE_PIN Y32  [get_ports {refclkp[13]}]
# Bank 128 refclk0 (interposer: MGT_CLK_04)
set_property PACKAGE_PIN AD32 [get_ports {refclkp[14]}]
# Bank 127 refclk0 (interposer: MGT_CLK_06)
set_property PACKAGE_PIN AG30 [get_ports {refclkp[15]}]
