
# EXTERNAL OSCILLATOR
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports osc_clk_n]
set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports osc_clk_p]
set_property PACKAGE_PIN AV14          [get_ports osc_clk_n]
set_property PACKAGE_PIN AU14          [get_ports osc_clk_p]
