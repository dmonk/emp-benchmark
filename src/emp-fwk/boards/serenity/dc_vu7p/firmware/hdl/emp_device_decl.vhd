-- emp_device_decl
--
-- Defines constants for the whole device
--
-- Tom Williams, June 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.emp_framework_decl.all;

-------------------------------------------------------------------------------
package emp_device_decl is

  constant BOARD_DESIGN_ID : std_logic_vector(7 downto 0) := X"45";

  constant N_REGION     : integer := 18;  
  constant N_REFCLK     : integer := 12;
  constant CROSS_REGION : integer := 8;

  constant IO_REGION_SPEC : io_region_spec_array_t(0 to N_REGION - 1) := (
    0  => (io_gty,   0,  6),  -- Bank 225 -- Right column
    1  => (io_gty,   0,  6),  -- Bank 226
    2  => (io_gty,   0,  6),  -- Bank 227 
    3  => (io_nogt, -1, -1),  -- Bank 228 #
    4  => (io_gty,   1,  7),  -- Bank 229
    5  => (io_gty,   1,  7),  -- Bank 230
    6  => (io_gty,   2,  8),  -- Bank 231
    7  => (io_gty,   2,  8),  -- Bank 232
    8  => (io_gty,   2,  8),  -- Bank 233
   -- cross chip
    9  => (io_gty,   3,  9),  -- Bank 133 -- Left column
    10 => (io_gty,   3,  9),  -- Bank 132
    11 => (io_gty,   3,  9),  -- Bank 131
    12 => (io_gty,   4, 10),  -- Bank 130
    13 => (io_gty,   4, 10),  -- Bank 129
    14 => (io_nogt, -1, -1),  -- Bank 128 #
    15 => (io_gty,   5, 11),  -- Bank 127
    16 => (io_gty,   5, 11),  -- Bank 126
    17 => (io_gty,   5, 11),  -- Bank 125
    others  => kIONoGTRegion
  );

end emp_device_decl;
-------------------------------------------------------------------------------

