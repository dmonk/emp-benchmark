delete_pblocks -quiet infra ttc clocks payload
#-------------------------------------


# Constrain TTC
create_pblock ttc
resize_pblock [get_pblocks ttc] -add {CLOCKREGION_X1Y0:CLOCKREGION_X4Y0}
add_cells_to_pblock [get_pblocks ttc] [get_cells -quiet {ttc}]
remove_cells_from_pblock [get_pblocks ttc] [get_cells -quiet {ttc/clocks}]


# Parameters
set lLeftQuadWidth 401
set lRightQuadWidth 401

set lClkBounds [get_XY_bounds [get_clock_regions]]
puts "Clock region boundaries ${lClkBounds}"

lassign [create_quad_pblocks $lLeftQuadWidth $lRightQuadWidth] lNumQuads lLeftBoundary lRightBoundary

# Create the quad p-blocks and store the number of blocks created
puts "Created $lNumQuads quads"

for {set lRegId 0} {$lRegId < 9} {incr lRegId} {
    set q [expr 1 + $lRegId]
    set lQuadBlock [get_pblocks quad_R$q] 
    puts "Populating $lQuadBlock with region $lRegId" 
    
    add_cells_to_pblock $lQuadBlock datapath/rgen\[$lRegId\].region

    constrain_mgts $lRegId $lQuadBlock 1
}

for {set lRegId 9} {$lRegId < 18} {incr lRegId} {
    set q [expr 18 - $lRegId]
    set lQuadBlock [get_pblocks quad_L$q]
    puts "Populating $lQuadBlock with region $lRegId"

    add_cells_to_pblock $lQuadBlock datapath/rgen\[$lRegId\].region

    constrain_mgts $lRegId $lQuadBlock 0
}


# Payload Area assignment

set lPayload [create_pblock payload]
set lPayloadRect [find_rects [get_sites -of [get_clock_regions -f {ROW_INDEX>0}] -f "RPM_X >= $lLeftBoundary && RPM_X <= $lRightBoundary"]]
add_rects_to_pblock $lPayload $lPayloadRect

add_cells_to_pblock [get_pblocks payload] [get_cells -quiet datapath/rgen[*].pgen.*]
