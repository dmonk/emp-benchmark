# PCIe connections
set_property PACKAGE_PIN AW9 [get_ports pcie_sys_clk_p]
set_property IOSTANDARD LVCMOS18 [get_ports pcie_sys_rst]
set_property PACKAGE_PIN AR26 [get_ports pcie_sys_rst]
set_property PULLUP true [get_ports pcie_sys_rst]
set_false_path -from [get_ports pcie_sys_rst]

# EXTERNAL TTC CLOCK -- LVDS02_03
set_property PACKAGE_PIN M32            [get_ports osc_clk_n]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports osc_clk_n]
set_property PACKAGE_PIN M31            [get_ports osc_clk_p]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports osc_clk_p]

# EXTERNAL TTC DATA -- LVDS02_01
set_property PACKAGE_PIN N33            [get_ports ttc_lvds_n]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports ttc_lvds_n]
set_property PACKAGE_PIN N32            [get_ports ttc_lvds_p]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports ttc_lvds_p]


# HEARTBEAT LED
set_property IOSTANDARD  LVCMOS18 [get_ports heartbeat_led]
set_property PACKAGE_PIN BF32     [get_ports heartbeat_led]


# MGT REF CLOCKS - ASYNC (U0:3, U1:3)
# Bank 226 Refclk0 (interposer: MGT_CLK_03)
set_property PACKAGE_PIN AM11 [get_ports {refclkp[0]}]
# Bank 228 Refclk0 (interposer: MGT_CLK_XX)  - Unconnected
#set_property PACKAGE_PIN AD11 [get_ports {refclkp[1]}]
# Bank 229 Refclk0 (interposer: MGT_CLK_05)
set_property PACKAGE_PIN Y11 [get_ports {refclkp[1]}]
# Bank 232 Refclk0 (interposer: MGT_CLK_07)
set_property PACKAGE_PIN H11 [get_ports {refclkp[2]}]
# Bank 132 Refclk0 (interposer: MGT_CLK_09)
set_property PACKAGE_PIN R36 [get_ports {refclkp[3]}]
# Bank 129 Refclk0 (interposer: MGT_CLK_11)
set_property PACKAGE_PIN AG36 [get_ports {refclkp[4]}]
# Bank 128 Refclk0 (interposer: MGT_CLK_XX) - - Unconnected
#set_property PACKAGE_PIN AL36 [get_ports {refclkp[6]}]
# Bank 126 Refclk0 (interposer: MGT_CLK_13)
set_property PACKAGE_PIN AV38 [get_ports {refclkp[5]}]


# MGT REF CLOCKS - SYNC  (U0:2, U1:2)
# Bank 226 Refclk1 (interposer: MGT_CLK_02)
set_property PACKAGE_PIN AK11 [get_ports {refclkp[6]}]
# Bank 228 Refclk1 (interposer: MGT_CLK_XX)  - Unconnected
#set_property PACKAGE_PIN AB11 [get_ports {refclkp[9]}]
# Bank 229 Refclk1 (interposer: MGT_CLK_04)
set_property PACKAGE_PIN V11 [get_ports {refclkp[7]}]
# Bank 232 Refclk1 (interposer: MGT_CLK_06)
set_property PACKAGE_PIN F11 [get_ports {refclkp[8]}]
# Bank 132 Refclk1 (interposer: MGT_CLK_08)
set_property PACKAGE_PIN N36 [get_ports {refclkp[9]}]
# Bank 129 Refclk1 (interposer: MGT_CLK_10)
set_property PACKAGE_PIN AE36 [get_ports {refclkp[10]}]
# Bank 128 Refclk1 (interposer: MGT_CLK_XX) - - Unconnected
#set_property PACKAGE_PIN AJ36 [get_ports {refclkp[14]}]
# Bank 126 Refclk1 (interposer: MGT_CLK_12)
set_property PACKAGE_PIN AU36 [get_ports {refclkp[11]}]
