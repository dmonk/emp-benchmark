-- sim_udp_infra
--
-- Simulation wrapper for ethernet, ipbus, MMC link and associated clock / system reset
--
-- Dave Newbold, June 2013

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.ipbus.all;
use work.ipbus_trans_decl.all;

entity sim_udp_infra is
  port(
    -- IPbus clock and reset (out)
    ipb_clk  : out std_logic;
    ipb_rst  : out std_logic;
    -- IPbus (from / to slaves)
    ipb_in   : in  ipb_rbus;
    ipb_out  : out ipb_wbus;
    -- External resets
    soft_rst : in  std_logic            -- The signal of lesser doom
    );

end sim_udp_infra;

architecture rtl of sim_udp_infra is

  signal clk_ipb, clk_ipb_i, rst, rsti: std_logic;
  signal trans_in: ipbus_trans_in;
  signal trans_out: ipbus_trans_out;  
  --signal clk125_g, ipb_clk_g, clk40_g, rst_g, rst_ctrl, ipb_clk_i                                       : std_logic;
  --signal mac_tx_data, mac_rx_data                                                                       : std_logic_vector(7 downto 0);
  --signal mac_tx_valid, mac_tx_last, mac_tx_error, mac_tx_ready, mac_rx_valid, mac_rx_last, mac_rx_error : std_logic;
  --signal ipb_out_m                                                                                      : ipb_wbus;
  --signal ipb_in_m                                                                                       : ipb_rbus;

begin

-- Clock generation for ipbus, ethernet, POR

  clocks: entity work.clock_sim
    generic map(
      CLK_AUX_FREQ => 40.0
    )
    port map(
      clko125 => open,
      clko25 => clk_ipb_i,
      clko_aux => open,
      nuke => '0', -- no doom today
      soft_rst => soft_rst,
      rsto => rst,
      rsto_ctrl => rsti
    );

  clk_ipb <= clk_ipb_i; -- Best to align delta delays on all clocks for simulation
  ipb_clk <= clk_ipb_i;
  ipb_rst <= rst;

  -- Clocks for rest of logic

  ----clk125    <= clk125_g;
  --ipb_clk_i <= ipb_clk_g;
  --ipb_clk   <= ipb_clk_g;
  --ipb_rst   <= rst_g;

-- sim UDP transport

  udp: entity work.ipbus_sim_udp
    port map(
      clk_ipb => clk_ipb,
      rst_ipb => rsti,
      trans_out => trans_in,
      trans_in => trans_out
    );

-- IPbus transactor

  trans: entity work.transactor
    port map (
      clk => clk_ipb,
      rst => rsti,
      ipb_out => ipb_out,
      ipb_in => ipb_in,
      ipb_req => open,
      ipb_grant => '1',
      trans_in => trans_in,
      trans_out => trans_out,
      cfg_vector_in => (Others => '0'),
      cfg_vector_out => open
    );

end rtl;

