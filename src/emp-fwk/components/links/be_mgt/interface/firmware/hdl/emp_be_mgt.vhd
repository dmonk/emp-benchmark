----------------------------------------------------------------------------------
-- Project - emp_fwk
-- Component - links
-- Description
--    Rev. 0.1 - Integrate mgt16 from Stavros MALLIOUS to emp - Krerk PIROMSOPA , Pitchaya SITTI-AMORN 
--    Rev. 0.2 - Integrate MGTs (gth16, gty16, gty25) - kadamidis
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_project_decl.all;
use work.emp_data_types.all;
use work.package_links.all;

entity emp_be_mgt is
  generic (
     INDEX      : integer;
     N_CHANNELS : integer := 4
     ); 
  port(
    clk             : in std_logic;
    rst             : in std_logic;
    ipb_in          : in ipb_wbus;
    ipb_out         : out ipb_rbus;

    ttc_clk_in      : in  std_logic;
    stable_clk_in   : in std_logic; 
    top_mgtrefclk0  : in std_logic;
           
    buf_rst_in      : in   std_logic_vector(N_CHANNELS - 1 downto 0);
    buf_ptr_inc_in  : in   std_logic_vector(N_CHANNELS - 1 downto 0);
    buf_ptr_dec_in  : in   std_logic_vector(N_CHANNELS - 1 downto 0);
    align_marker_out: out  std_logic_vector(N_CHANNELS - 1 downto 0);
    qplllock        : out  std_logic;
    -- TX & RX clocks for monitoring purposes
    rxclk_mon        : out std_logic;
    txclk_mon        : out std_logic;
    -- Parallel interface data
    txdata_in  : in  ldata(N_CHANNELS - 1 DOWNTO 0);
    rxdata_out : out ldata(N_CHANNELS - 1 DOWNTO 0)
);
end emp_be_mgt;

architecture behavioral of emp_be_mgt is

    signal ctrl : ipb_reg_v(4 * NUMBER_OF_CHAN_RW_REGS + NUMBER_OF_COMMON_RW_REGS - 1 downto 0);
    signal stat : ipb_reg_v(4 * NUMBER_OF_CHAN_RO_REGS + NUMBER_OF_COMMON_RO_REGS - 1 downto 0);

    signal chan_ro_reg   : type_chan_ro_reg_array(3 downto 0);
    signal chan_rw_reg   : type_chan_rw_reg_array(3 downto 0);
    signal common_ro_reg : type_common_ro_reg;
    signal common_rw_reg : type_common_rw_reg;
    signal rxn_in          :   std_logic_vector(N_CHANNELS - 1 DOWNTO 0);
    signal rxp_in          :   std_logic_vector(N_CHANNELS - 1 DOWNTO 0);
    signal txn_out         :   std_logic_vector(N_CHANNELS - 1 DOWNTO 0);
    signal txp_out         :   std_logic_vector(N_CHANNELS - 1 DOWNTO 0);    
-- in    
    signal mgt_rst               : std_logic;
    signal mgt_loopback          : std_logic := '0';
    signal mgt_error_counter_rst : std_logic;
    signal loopback_mode         : std_logic_vector(3 * N_CHANNELS - 1 downto 0) :="010010010010";
-- out  
    signal link_status           : std_logic;
    signal link_down_latched     : std_logic;
    signal init_done             : std_logic;
    signal reset_tx_done         : std_logic;
    signal reset_rx_done         : std_logic;
    signal bufferbypass_tx_done  : std_logic;
    signal bufferbypass_rx_done  : std_logic;
    signal qpll0lock_out         : std_logic;
    signal crc_error_out         : std_logic_vector(N_CHANNELS - 1 downto 0);
begin



hermes_links_inst : entity work.async_mgt_wrapper
    generic map(
            INDEX               => INDEX,            
            LINK                => "ASYNC",
            PATTERN             => "USER",
            DATA_WIDTH          => 64,
            N_CHANNELS          => 4,
            STABLE_CLOCK_PERIOD => 31.25)    
    port map(
            ttc_clk_in              => ttc_clk_in,      -- 240 MHz or 360 MHz
            ttc_rst_in              => rst,
            stable_clk_in           => stable_clk_in,   -- 31.25 MHz
            top_mgtrefclk0          => top_mgtrefclk0,   -- 250 MHz            
            -- High Speed Serdes data ports 
            rxn_in                  => rxn_in,  
            rxp_in                  => rxp_in ,
            txn_out                 => txn_out,
             txp_out                => txp_out,
             -- Parallel interface data
            txdata_in               => txdata_in,
            rxdata_out              => rxdata_out,
            -- Control and debugg ports
            -- Channel Registers
            buf_rst_in         => buf_rst_in,
            buf_ptr_inc_in     => buf_ptr_inc_in,
            buf_ptr_dec_in     => buf_ptr_dec_in,
            rxclk_mon          => rxclk_mon,
            txclk_mon          => txclk_mon,
            chan_ro_regs_out   => chan_ro_reg,
            chan_rw_regs_in    => chan_rw_reg,
            -- Common Registers
            common_ro_regs_out => common_ro_reg,
            common_rw_regs_in  => common_rw_reg,
            align_marker_out   => align_marker_out,   
            qpll0lock_out      => qplllock         
             );      
      
 -- IPbus Slave    
 csr: entity work.ipbus_ctrlreg_v
     generic map(
            N_CTRL => 4 * NUMBER_OF_CHAN_RW_REGS + NUMBER_OF_COMMON_RW_REGS,
            N_STAT => 4 * NUMBER_OF_CHAN_RO_REGS + NUMBER_OF_COMMON_RO_REGS
            )
     port map(
            clk       => clk,
            reset     => rst,
            ipbus_in  => ipb_in,
            ipbus_out => ipb_out,
            d         => stat,
            q         => ctrl
            );
 
   chan_reg_gen : for i in 3 downto 0 generate
    chan_ro_gen : for j in NUMBER_OF_CHAN_RO_REGS - 1 downto 0 generate
      stat(NUMBER_OF_CHAN_RO_REGS * i + j) <= chan_ro_reg(i)(j);
    end generate;
    chan_rw_gen : for j in NUMBER_OF_CHAN_RW_REGS - 1 downto 0 generate
      chan_rw_reg(i)(j) <= ctrl(NUMBER_OF_CHAN_RW_REGS * i + j);
    end generate;
  end generate;

  common_ro_gen : for j in NUMBER_OF_COMMON_RO_REGS - 1 downto 0 generate
    stat(4 * NUMBER_OF_CHAN_RO_REGS + j) <= common_ro_reg(j);
  end generate;
  common_rw_gen : for j in NUMBER_OF_COMMON_RW_REGS - 1 downto 0 generate
    common_rw_reg(j) <= ctrl(4 * NUMBER_OF_CHAN_RW_REGS + j);
  end generate;




end behavioral;






