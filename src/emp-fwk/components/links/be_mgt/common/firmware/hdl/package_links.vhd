----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.07.2019 15:30:20
-- Design Name: 
-- Module Name: package_links - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library work;       
package package_links is  

    constant NUMBER_OF_CHAN_RW_REGS : integer := 1;
    constant NUMBER_OF_CHAN_RO_REGS : integer := 2;
    constant NUMBER_OF_COMMON_RW_REGS : integer := 1;
    constant NUMBER_OF_COMMON_RO_REGS : integer := 1;
    
    type type_vector_of_stdlogicvec_x3 is array(natural range <>) of std_logic_vector(2 downto 0);
    type type_vector_of_stdlogicvec_x8 is array(natural range <>) of std_logic_vector(7 downto 0);
    type type_vector_of_stdlogicvec_x32 is array(natural range <>) of std_logic_vector(31 downto 0);
    
    -- CHANNEL register space
    subtype type_chan_ro_reg is type_vector_of_stdlogicvec_x32(NUMBER_OF_CHAN_RO_REGS-1 downto 0);
    subtype type_chan_rw_reg is type_vector_of_stdlogicvec_x32(NUMBER_OF_CHAN_RW_REGS-1 downto 0);
    
    type type_chan_ro_reg_array is array(natural range <>) of type_chan_ro_reg;
    type type_chan_rw_reg_array is array(natural range <>) of type_chan_rw_reg;    
  -- COMMON register space
    subtype type_common_ro_reg is type_vector_of_stdlogicvec_x32(NUMBER_OF_COMMON_RO_REGS-1 downto 0);
    subtype type_common_rw_reg is type_vector_of_stdlogicvec_x32(NUMBER_OF_COMMON_RW_REGS-1 downto 0);  
    
end package;
