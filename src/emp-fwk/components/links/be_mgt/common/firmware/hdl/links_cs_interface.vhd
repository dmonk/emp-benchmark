----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/15/2019 11:06:28 AM
-- Design Name: 
-- Module Name: links_cs_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.package_links.all;
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity links_cs_interface is
  generic(N_CHANNELS : integer := 4;
          KIND       : integer
          );
  port (
    ttc_clk_in                      : in  std_logic;
    txusrclk2_i                     : in  std_logic;
    rxusrclk2_i                     : in  std_logic;
    stable_clk_in                   : in  std_logic;
    -- Channel Registers
    chan_ro_regs_out                : out type_chan_ro_reg_array(3 downto 0);
    chan_rw_regs_in                 : in  type_chan_rw_reg_array(3 downto 0);
    -- Common Registers
    common_ro_regs_out              : out type_common_ro_reg;
    common_rw_regs_in               : in  type_common_rw_reg;
    -- Channel signals
    txresetdone_out                 : in  std_logic_vector(3 downto 0);     -- txusrclk domain
    rxresetdone_out                 : in  std_logic_vector(3 downto 0);     -- rxusrclk domain
    txpmaresetdone_i                : in  std_logic_vector(3 downto 0);     -- txusrclk domain
    rxpmaresetdone_i                : in  std_logic_vector(3 downto 0);     -- rxusrclk domain
    rxcdrlock                       : in  std_logic_vector(3 downto 0);     -- rxusrclk domain
    link_status_i                   : in  std_logic_vector(3 downto 0);     -- stable_clk domain
    link_down_latched_i             : in  std_logic_vector(3 downto 0);     -- stable_clk domain
    txpolarity_out_ttc              : out std_logic_vector(3 downto 0);     -- txusrclk domain
    rxpolarity_out_ttc              : out std_logic_vector(3 downto 0);     -- rxusrclk domain
    loopback_mode_in                : out std_logic_vector(11 downto 0);    -- async
    orbit_tag_enable, align_disable : out std_logic_vector(3 downto 0);     
    reset_crc_counters_ttc          : out std_logic_vector(3 downto 0);     
    rxlpmen_in                      : out std_logic_vector(3 downto 0);     -- async
    rx_crc_checked_cnt              : in  type_vector_of_stdlogicvec_x8(N_CHANNELS - 1 downto 0);   -- rxusrclk domain
    rx_crc_error_cnt                : in  type_vector_of_stdlogicvec_x8(N_CHANNELS - 1 downto 0);   -- rxusrclk domain
    -- Common signals
    buffbypass_tx_done_i            : in  std_logic;    
    buffbypass_rx_done_i            : in  std_logic;
    reset_tx_done_i                 : in  std_logic;
    reset_rx_done_i                 : in  std_logic;
    quad_link_status                : in  std_logic;    -- stable_clk domain
    qpll0lock_out_int               : in  std_logic;
    init_done_i                     : in  std_logic;    -- stable_clk domain
    quad_link_down_latched          : in  std_logic;    -- stable_clk domain
    soft_reset_s                    : out std_logic;
    reset_error_counter_out_q_s     : out std_logic;
    reset_tx_datapath_s             : out std_logic;    -- per quad
    reset_rx_datapath_s             : out std_logic;    -- per quad
    tx_usrrst_out_ttc               : out std_logic_vector(3 downto 0);    -- per channel
    rx_usrrst_out_ttc               : out std_logic_vector(3 downto 0)     -- per channel

    );


end links_cs_interface;

architecture Behavioral of links_cs_interface is

  signal reset_error_counter_in                   : std_logic_vector(N_CHANNELS - 1 downto 0); signal buffbypass_rx_done_ttc : std_logic;
  signal buffbypass_tx_done_ttc                   : std_logic;
  signal reset_tx_done_ttc                        : std_logic;
  signal reset_rx_done_ttc                        : std_logic;
  signal qpll0lock_out_ttc                        : std_logic;
  signal quad_link_status_ttc                     : std_logic;
  signal init_done_ttc                            : std_logic;
  signal quad_link_down_latched_ttc               : std_logic;
  signal soft_reset                               : std_logic;
  signal reset_error_counter_in_q                 : std_logic;
  signal reset_tx_datapath                        : std_logic;
  signal reset_rx_datapath                        : std_logic;
  signal txresetdone_out_ttc, rxresetdone_out_ttc : std_logic_vector(3 downto 0);
  signal link_status_ttc, txpmaresetdone_ttc      : std_logic_vector(3 downto 0);
  signal rxpmaresetdone_ttc, rxcdrlock_ttc        : std_logic_vector(3 downto 0);
  signal link_down_latched_ttc                    : std_logic_vector(3 downto 0);
  signal txpolarity_in                            : std_logic_vector(3 downto 0);
  signal rxpolarity_in                            : std_logic_vector(3 downto 0);
  signal tx_usrrst                                : std_logic_vector(3 downto 0);
  signal rx_usrrst                                : std_logic_vector(3 downto 0);
  signal reset_crc_counters                       : std_logic_vector(3 downto 0);
  signal loopback                                 : type_vector_of_stdlogicvec_x3(N_CHANNELS - 1 downto 0) := ("010","010","010","010");
  signal rx_crc_error_cnt_ttc                     : type_vector_of_stdlogicvec_x8(N_CHANNELS - 1 downto 0) := ("010","010","010","010");
  signal rx_crc_checked_cnt_ttc                   : type_vector_of_stdlogicvec_x8(N_CHANNELS - 1 downto 0) := ("010","010","010","010");

  attribute DONT_TOUCH                                     : string;
  attribute DONT_TOUCH of synchronizers_gen                : label is "true";
  attribute DONT_TOUCH of bit_synchronizer_txbuff_inst     : label is "true";
  attribute DONT_TOUCH of bit_synchronizer_rxbuff_inst     : label is "true";
  attribute DONT_TOUCH of reset_synchronizer_reset_inst    : label is "true";
  attribute DONT_TOUCH of reset_synchronizer_reseterr_inst : label is "true";
  attribute DONT_TOUCH of Behavioral                       : architecture is "yes";

begin


  synchronizers_gen : for i in 0 to N_CHANNELS - 1 generate

    bit_synchronizer_txdone_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => txresetdone_out(i),
        o_out  => txresetdone_out_ttc(i)
        );
    bit_synchronizer_rxdone_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => rxresetdone_out(i),
        o_out  => rxresetdone_out_ttc(i)
        );
    bit_synchronizer_txpmadone_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => txpmaresetdone_i(i),
        o_out  => txpmaresetdone_ttc(i)
        );
    bit_synchronizer_rxpmadone_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => rxpmaresetdone_i(i),
        o_out  => rxpmaresetdone_ttc(i)
        );
    bit_synchronizer_cdr_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => rxcdrlock(i),
        o_out  => rxcdrlock_ttc(i)
        );
    bit_synchronizer_status_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => link_status_i(i),
        o_out  => link_status_ttc(i)
        );
    bit_synchronizer_latched_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => link_down_latched_i(i),
        o_out  => link_down_latched_ttc(i)
        );
    reset_synchronizer_txpol_inst : entity work.reset_synchronizer
      port map(
        clk_in  => txusrclk2_i,
        rst_in  => txpolarity_in(i),
        rst_out => txpolarity_out_ttc(i)
        );
    reset_synchronizer_rxpol_inst : entity work.reset_synchronizer
      port map(
        clk_in  => rxusrclk2_i,
        rst_in  => rxpolarity_in(i),
        rst_out => rxpolarity_out_ttc(i)
        );

    reset_synchronizer_txrst_inst : entity work.reset_synchronizer
      port map(
        clk_in  => txusrclk2_i,
        rst_in  => tx_usrrst(i),
        rst_out => tx_usrrst_out_ttc(i)
        );
        
    reset_synchronizer_rxrst_inst : entity work.reset_synchronizer
      port map(
        clk_in  => rxusrclk2_i,
        rst_in  => rx_usrrst(i),
        rst_out => rx_usrrst_out_ttc(i)
        );
        
    reset_synchronizer_rstcrc_inst : entity work.reset_synchronizer
      port map(
        clk_in  => stable_clk_in,
        rst_in  => reset_crc_counters(i),
        rst_out => reset_crc_counters_ttc(i)
        );

  synchronizers_8b_gen : for j in 0 to 7 generate

    bit_synchronizer_crcerr_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => rx_crc_error_cnt(i)(j),
        o_out  => rx_crc_error_cnt_ttc(i)(j)
        );
    bit_synchronizer_crcchk_inst : entity work.bit_synchronizer
      port map(
        clk_in => stable_clk_in,
        i_in   => rx_crc_checked_cnt(i)(j),
        o_out  => rx_crc_checked_cnt_ttc(i)(j)
        );
        
  end generate;

  end generate;


        
  -- Loop over all channels
  reg_chan_gen : for i in 0 to N_CHANNELS - 1 generate
    ----------------------------------------------------------------------------- 
    -- ReadOnly Regs. All TTC domain
    -----------------------------------------------------------------------------
--   chan_ro_regs_out(i)(0) <= rx_trailer(i);  -- will come from rx_crc
    chan_ro_regs_out(i)(1) <= txresetdone_out_ttc(i) &  -- mask 0x80000000
                              rxresetdone_out_ttc(i) &  -- mask 0x40000000
                              txpmaresetdone_ttc(i) &   -- mask 0x20000000
                              rxpmaresetdone_ttc(i) &   -- mask 0x10000000
                              rxcdrlock_ttc(i) &        -- mask 0x08000000
                              link_status_i(i) & -- link_status_ttc(i) &      -- mask 0x04000000
                              link_down_latched_i(i) &-- mask 0x02000000
                              '0' &                     -- mask 0x01000000
                              x"00" &                   -- mask 0x00ff0000
                              rx_crc_error_cnt_ttc(i) &     -- mask 0x0000ff00
                              rx_crc_checked_cnt_ttc(i);    -- mask 0x800000ff

    -----------------------------------------------------------------------------
    -- ReadWrite Regs. All TTC domain except for polarity & loopback regs: 
    -----------------------------------------------------------------------------

    -- loopback is async input
    loopback(i)           <= chan_rw_regs_in(i)(0)(2 downto 0);  -- mask 0x7
    -- crc reset is in TTC domain
    reset_crc_counters(i) <= chan_rw_regs_in(i)(0)(3);           -- mask 0x8 
    -- txpolarity is in the txusrclk2 domain
    txpolarity_in(i)      <= chan_rw_regs_in(i)(0)(4);           -- mask 0x10
    -- rxpolarity is in the rxusrclk2 domain
    rxpolarity_in(i)      <= chan_rw_regs_in(i)(0)(5);           -- mask 0x20 
    tx_usrrst(i)          <= chan_rw_regs_in(i)(0)(6);           -- mask 0x40 
    rx_usrrst(i)          <= chan_rw_regs_in(i)(0)(7);           -- mask 0x80 
    orbit_tag_enable(i)   <= chan_rw_regs_in(i)(0)(8);           -- mask 0x100
    align_disable(i)      <= chan_rw_regs_in(i)(0)(9);           -- mask 0x200
    rxlpmen_in(i)         <= chan_rw_regs_in(i)(0)(10);          -- mask 0x400
  
  end generate;



  loopback_mode_in <= loopback(3) & loopback(2) & loopback(1) & loopback(0);


  bit_synchronizer_txbuff_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => buffbypass_tx_done_i,
      o_out  => buffbypass_tx_done_ttc
      );
  bit_synchronizer_rxbuff_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => buffbypass_rx_done_i,
      o_out  => buffbypass_rx_done_ttc
      );
  bit_synchronizer_rxdone_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => reset_tx_done_i,
      o_out  => reset_tx_done_ttc
      );
  bit_synchronizer_txdone_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => reset_rx_done_i,
      o_out  => reset_rx_done_ttc
      );
  bit_synchronizer_qpll_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => qpll0lock_out_int,
      o_out  => qpll0lock_out_ttc
      );
  bit_synchronizer_status_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => quad_link_status,
      o_out  => quad_link_status_ttc
      );
  bit_synchronizer_init_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => init_done_i,
      o_out  => init_done_ttc
      );

  bit_synchronizer_latched_inst : entity work.bit_synchronizer
    port map(
      clk_in => stable_clk_in,
      i_in   => quad_link_down_latched,
      o_out  => quad_link_down_latched_ttc
      );


  reset_synchronizer_reset_inst : entity work.reset_synchronizer
    port map(
      clk_in  => txusrclk2_i,
      rst_in  => soft_reset,
      rst_out => soft_reset_s
      );


  reset_synchronizer_reseterr_inst : entity work.reset_synchronizer
    port map(
      clk_in  => stable_clk_in,
      rst_in  => reset_error_counter_in_q,
      rst_out => reset_error_counter_out_q_s
      );
      
  reset_synchronizer_resetrxdat_inst : entity work.reset_synchronizer
        port map(
          clk_in  => stable_clk_in,
          rst_in  => reset_tx_datapath,
          rst_out => reset_tx_datapath_s
          );

  reset_synchronizer_resettxdat_inst : entity work.reset_synchronizer
        port map(
          clk_in  => stable_clk_in,
          rst_in  => reset_rx_datapath,
          rst_out => reset_rx_datapath_s
          );
  -----------------------------------------------------------------------------
  ---- COMMON Register Access:
  -----------------------------------------------------------------------------

  -- COMMON: ReadOnly Regs.
  -- WARNING: Clock domain may not be observed
  common_ro_regs_out(0)(0)            <= qpll0lock_out_ttc;      -- mask 0x1
  common_ro_regs_out(0)(4 downto 1)   <= std_logic_vector(to_unsigned(KIND, 4)); -- mask 0x1E
  common_ro_regs_out(0)(5)            <= quad_link_status;   -- mask 0x20
  common_ro_regs_out(0)(6)            <= init_done_i;          -- mask 0x40
  common_ro_regs_out(0)(7)            <= quad_link_down_latched;  -- mask 0x80
  common_ro_regs_out(0)(8)            <= reset_tx_done_ttc;      -- mask 0x100
  common_ro_regs_out(0)(9)            <= reset_rx_done_ttc;      -- mask 0x200
  common_ro_regs_out(0)(10)           <= buffbypass_tx_done_ttc; -- mask 0x400
  common_ro_regs_out(0)(11)           <= buffbypass_rx_done_ttc; -- mask 0x800
  common_ro_regs_out(0)(31 downto 12) <= (others => '0');        -- mask 0xFFFFF000

  -- COMMON: ReadWrite Regs.
  -- WARNING: Clock domain may not be observed
  soft_reset               <= common_rw_regs_in(0)(0);  -- mask 0x1
  reset_error_counter_in_q <= common_rw_regs_in(0)(1);  -- mask 0x2
  reset_tx_datapath        <= common_rw_regs_in(0)(2);  -- mask 0x4
  reset_rx_datapath        <= common_rw_regs_in(0)(3);  -- mask 0x8


end Behavioral;
