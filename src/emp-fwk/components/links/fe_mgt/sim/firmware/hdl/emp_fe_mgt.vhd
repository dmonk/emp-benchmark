
-- Dummy 'null' FE MGT interface for simulation

library ieee;
use ieee.std_logic_1164.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_data_types.all;

entity emp_fe_mgt is
  generic (
    INDEX          : integer;
    N_CHANNELS     : integer := 4
    );
  port (
    --usrclk         : in  std_logic;
    clk              : in  std_logic;
    rst              : in  std_logic;
    csel_in          : in  std_logic_vector(2 downto 0);
    fe_mgt_ipb_in    : in  ipb_wbus;
    fe_mgt_ipb_out   : out ipb_rbus;
    refclk           : in  std_logic;
    clk_p            : in  std_logic;
    clk40            : in  std_logic;
    rxclk_mon        : out std_logic;
    txclk_mon        : out std_logic;
    txdata_in        : in  ldata(N_CHANNELS - 1 DOWNTO 0);
    rxdata_out       : out ldata(N_CHANNELS - 1 DOWNTO 0);
    cfg_hdr10        : out std_logic;
    cfg_hdr11        : out std_logic
);
end emp_fe_mgt;

architecture behavioural of emp_fe_mgt is

  signal rxdata   : ldata(N_CHANNELS - 1 DOWNTO 0);

begin

  fe_mgt_ipb_out.ipb_rdata <= (others => '0');
  fe_mgt_ipb_out.ipb_ack <= '0';
  fe_mgt_ipb_out.ipb_err <= fe_mgt_ipb_in.ipb_strobe;

  process(clk_p)
  begin
      if rising_edge(clk_p) then
          rxdata <= txdata_in;
      end if;
  end process;

  rxdata_out <= txdata_in;

  rxclk_mon <= '0';
  txclk_mon <= '0';
  cfg_hdr10 <= '0';
  cfg_hdr11 <= '0';
end behavioural;
