-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Device specific transceiver
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

--! @brief MGT - Transceiver
--! @details 
--! The MGT module provides the interface to the transceivers to send the GBT-links via
--! high speed links (@4.8Gbps)
entity emp_lpgbt_xlx_ku_mgt_10g24 is                                     
 generic (N_CHANNELS : integer := 4);
   port (
       --=============--
       -- Clocks      --
       --=============--
       MGT_REFCLK_i                 : in  std_logic;
       MGT_FREEDRPCLK_i             : in  std_logic;
             
       MGT_RXUSRCLK_o               : out std_logic_vector(N_CHANNELS - 1 downto 0);
       MGT_TXUSRCLK_o               : out std_logic_vector(N_CHANNELS - 1 downto 0);
       
       --=============--
       -- Resets      --
       --=============--
       MGT_TXRESET_i                : in  std_logic;
       MGT_RXRESET_i                : in  std_logic;
       
       --=============--
       -- Control     --
       --=============--
     MGT_RXSlide_i                : in  std_logic_vector(N_CHANNELS-1 downto 0);
       
 --      MGT_ENTXCALIBIN_i            : in  std_logic;
 --      MGT_TXCALIB_i                : in  std_logic_vector(6 downto 0);
       
       --=============--
       -- Status      --
       --=============--
       MGT_TXREADY_o                : out std_logic;
       MGT_RXREADY_o                : out std_logic;
       
--       MGT_TX_ALIGNED_o             : out std_logic;
--       MGT_TX_PIPHASE_o             : out std_logic_vector(6 downto 0);         
       --==============--
       -- Data         --
       --==============--
       MGT_USRWORD_i                : in  std_logic_vector(32*N_CHANNELS - 1 downto 0);
       MGT_USRWORD_o                : out std_logic_vector(32*N_CHANNELS - 1 downto 0);
       
       --===============--
       -- Serial intf.  --
       --===============--
       RXn_i                        : in  std_logic_vector(N_CHANNELS - 1 downto 0);
       RXp_i                        : in  std_logic_vector(N_CHANNELS - 1 downto 0);
       
       TXn_o                        : out std_logic_vector(N_CHANNELS - 1 downto 0);
       TXp_o                        : out std_logic_vector(N_CHANNELS - 1 downto 0)   
   );
end emp_lpgbt_xlx_ku_mgt_10g24;

--! @brief MGT - Transceiver
--! @details The MGT module implements all the logic required to send the GBT frame on high speed
--! links: resets modules for the transceiver, Tx PLL and alignement logic to align the received word with the 
--! GBT frame header.
architecture structural of emp_lpgbt_xlx_ku_mgt_10g24 is
    --================================ Signal Declarations ================================--
   
   -- Reset signals
    signal tx_reset_done                    : std_logic;
    signal txfsm_reset_done                 : std_logic;
    signal rx_reset_done                    : std_logic;
    signal rxfsm_reset_done                 : std_logic;
    
    signal rxBuffBypassRst                  : std_logic;
    signal gtwiz_userclk_rx_active_int      : std_logic;
    signal gtwiz_buffbypass_rx_reset_in_s   : std_logic;
    signal gtwiz_userclk_tx_active_int      : std_logic;
    signal gtwiz_buffbypass_tx_reset_in_s   : std_logic;
    
    signal gtwiz_userclk_tx_reset_int       : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal gtwiz_userclk_rx_reset_int       : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal txpmaresetdone                   : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal rxpmaresetdone                   : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal rx_wordclk_sig_ce                : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal tx_wordclk_sig_ce                : std_logic_vector(N_CHANNELS - 1 downto 0);
        
    signal rx_reset_sig                     : std_logic;
    signal tx_reset_sig                     : std_logic;
    
    signal MGT_USRWORD_s                    : std_logic_vector(32*N_CHANNELS - 1 downto 0);
    
    -- Clock signals
    signal rx_wordclk_sig                   : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal tx_wordclk_sig                   : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal rxoutclk_sig                     : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal txoutclk_sig                     : std_logic_vector(N_CHANNELS - 1 downto 0);
    
    -- Tx phase aligner signals
    signal txbufstatus_s                    : std_logic_vector(2*N_CHANNELS-1 downto 0); 
          
    signal txpippmen_s                      : std_logic_vector(N_CHANNELS - 1 DOWNTO 0);
    signal txpippmovrden_s                  : std_logic_vector(N_CHANNELS - 1 DOWNTO 0);
    signal txpippmsel_s                     : std_logic_vector(N_CHANNELS - 1 DOWNTO 0);
    signal txpippmpd_s                      : std_logic_vector(N_CHANNELS - 1 DOWNTO 0);
    signal txpippmstepsize_in               : std_logic_vector(5*N_CHANNELS - 1 DOWNTO 0);
           
    signal drpaddr_s                        : std_logic_vector(10*N_CHANNELS - 1 downto 0);
    signal drpclk_s                         : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal drpdi_s                          : std_logic_vector(16*N_CHANNELS-1 downto 0);
    signal drpen_s                          : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal drpwe_s                          : std_logic_vector(N_CHANNELS - 1 downto 0); 
    signal drprdy_s                         : std_logic_vector(N_CHANNELS - 1 downto 0);
    signal drpdo_s                          : std_logic_vector(16*N_CHANNELS-1 downto 0);
    signal drpclk_o                         : std_logic_vector(N_CHANNELS - 1 downto 0);
    
    
    signal mgt_rst_phaligner_s              : std_logic;

    signal gtrefclk0_s                      : STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);
    signal rxslide_s                        : STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);
    signal rxusrclk_s                       : STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);
    signal rxusrclk2_s                      : STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);
    signal txusrclk_s                       : STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);
    signal txusrclk2_s                      : STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);

    
    -- for serenity
   signal txdiffctrl_o :  STD_LOGIC_VECTOR(5 * N_CHANNELS - 1 DOWNTO 0);
   signal txprecursor_o :  STD_LOGIC_VECTOR(5 * N_CHANNELS - 1 DOWNTO 0);
   signal txpostcursor_o :  STD_LOGIC_VECTOR(5 * N_CHANNELS - 1 DOWNTO 0);
   signal rxlpmen_o :  STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);
   signal rxrate_o :  STD_LOGIC_VECTOR(3*N_CHANNELS - 1 DOWNTO 0);
   signal eyescanreset_o :  STD_LOGIC_VECTOR(N_CHANNELS - 1 downto 0);
   
   signal rxrate_i        : STD_LOGIC_VECTOR(3*N_CHANNELS - 1 DOWNTO 0);
   signal txdiffctrl_i    : STD_LOGIC_VECTOR(5*N_CHANNELS - 1DOWNTO 0);
   signal txprecursor_i   : STD_LOGIC_VECTOR(5*N_CHANNELS - 1 DOWNTO 0);
   signal txpostcursor_i  : STD_LOGIC_VECTOR(5*N_CHANNELS - 1 DOWNTO 0);
   signal  rxlpmen_i      : STD_LOGIC_VECTOR(N_CHANNELS - 1 DOWNTO 0);

   signal rxpmaresetdone_all : std_logic;
   signal txpmaresetdone_all : std_logic;
   
   signal gtwiz_userclk_tx_reset_in          : std_logic;
   signal gtwiz_buffbypass_rx_start_user_in  : std_logic;
   signal gtwiz_reset_all_in                 : std_logic;
   signal gtwiz_reset_tx_datapath_in         : std_logic;
   signal gtwiz_reset_rx_pll_and_datapath_in : std_logic;      
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
    --==================================== User Logic =====================================--
   
    --=============--
    -- Assignments --
    --=============--              
    MGT_TXREADY_o          <= tx_reset_done;    
    MGT_RXREADY_o          <= rx_reset_done and rxfsm_reset_done;
       
    MGT_RXUSRCLK_o         <= rx_wordclk_sig;   
    MGT_TXUSRCLK_o         <= tx_wordclk_sig;
        
    rx_reset_sig           <= MGT_RXRESET_i; -- removed the reset on not tx ready
    tx_reset_sig           <= MGT_TXRESET_i;    

    rxBuffBypassRst        <= not(gtwiz_userclk_rx_active_int); -- removed the reset on not tx ready.
      
    gtwiz_userclk_tx_reset_int <= not(txpmaresetdone);
    gtwiz_userclk_rx_reset_int <= not(rxpmaresetdone);
    rx_wordclk_sig_ce <= not(gtwiz_userclk_rx_reset_int);
    tx_wordclk_sig_ce <= not(gtwiz_userclk_tx_reset_int);
    
    wordclkbuf_gen : for i in 0 to N_CHANNELS - 1 generate
        
        gtrefclk0_s(i) <= MGT_REFCLK_i;
        drpclk_s(i)    <= MGT_FREEDRPCLK_i;
        rxslide_s(i)   <= MGT_RXSlide_i(i);       
        
        rxWordClkBuf_inst: bufg_gt
        port map (
         O                                        => rx_wordclk_sig(i), 
         I                                        => rxoutclk_sig(i),
        --             CE                                       => rx_wordclk_sig_ce(i),
         CE                                       => '1',
         DIV                                      => "000",
         CLR                                      => '0',
         CLRMASK                                  => '0',
         CEMASK                                   => '0'
        ); 
                    
        txWordClkBuf_inst: bufg_gt
        port map (
         O                                        => tx_wordclk_sig(i), 
         I                                        => txoutclk_sig(i),
        --             CE                                       => tx_wordclk_sig_ce(i),
         CE                                       => '1',
         DIV                                      => "000",
         CLR                                      => '0',
         CLRMASK                                  => '0',
         CEMASK                                   => '0'
        ); 
    
      
       rxWordPipeline_proc: process(rx_reset_done, rx_wordclk_sig(i))
        begin
          if rx_reset_done = '0' then
              MGT_USRWORD_o(32*(i+1)-1 downto 32*i) <= (others => '0');
          elsif rising_edge(rx_wordclk_sig(i)) then
              MGT_USRWORD_o(32*(i+1)-1 downto 32*i) <= MGT_USRWORD_s(32*(i+1)-1 downto 32*i);
          end if;      
        end process;   
        
     
    
    end generate;   

    resetDoneSynch_rx: entity work.xlx_ku_mgt_ip_reset_synchronizer
    PORT MAP(
       clk_in                                   => rx_wordClk_sig(0),
       rst_in                                   => rxBuffBypassRst,
       rst_out                                  => gtwiz_buffbypass_rx_reset_in_s
    );
    
    activetxUsrClk_proc: process(gtwiz_userclk_tx_reset_int(0), tx_wordclk_sig(0))
    begin
        if gtwiz_userclk_tx_reset_int(0) = '1' then
            gtwiz_userclk_tx_active_int <= '0';
        elsif rising_edge(tx_wordclk_sig(0)) then
            gtwiz_userclk_tx_active_int <= '1';
        end if;
    
    end process;
    
    activerxUsrClk_proc: process(gtwiz_userclk_rx_reset_int, rx_wordclk_sig(0))
    begin
        if gtwiz_userclk_rx_reset_int(0) = '1' then
            gtwiz_userclk_rx_active_int <= '0';
        elsif rising_edge(rx_wordclk_sig(0)) then
            gtwiz_userclk_rx_active_int <= '1';
        end if;
    end process;
 
    gtwiz_userclk_tx_reset_in <= '0';    
    gtwiz_buffbypass_rx_start_user_in <= '0';
    gtwiz_reset_all_in <= '0';
    gtwiz_reset_tx_datapath_in <= '0';
    gtwiz_reset_rx_pll_and_datapath_in <= '0';               
 
      xlx_ku_mgt_std_i: entity work.emp_lpgbt_xlx_ku_mgt_ip_10g24
         PORT MAP (  
    gtwiz_userclk_tx_reset_in(0) => gtwiz_userclk_tx_reset_in,
    gtwiz_userclk_tx_active_in(0) => gtwiz_userclk_tx_active_int,
    gtwiz_userclk_rx_active_in(0) => gtwiz_userclk_rx_active_int,
--    gtwiz_buffbypass_tx_reset_in(0) => gtwiz_buffbypass_tx_reset_in_s,
--    gtwiz_buffbypass_tx_start_user_in  => "0",
--    gtwiz_buffbypass_tx_done_out(0) => txfsm_reset_done,
--    gtwiz_buffbypass_tx_error_out => open,
    gtwiz_buffbypass_rx_reset_in(0) => gtwiz_buffbypass_rx_reset_in_s,
    gtwiz_buffbypass_rx_start_user_in(0) => gtwiz_buffbypass_rx_start_user_in,
    gtwiz_buffbypass_rx_done_out(0) => rxfsm_reset_done,
    gtwiz_buffbypass_rx_error_out => open,
    gtwiz_reset_clk_freerun_in(0) => MGT_FREEDRPCLK_i,
    gtwiz_reset_all_in(0) => gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in(0) => tx_reset_sig,
    gtwiz_reset_tx_datapath_in(0) => gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in(0) => gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in(0) => rx_reset_sig,
    gtwiz_reset_rx_cdr_stable_out => open,
    gtwiz_reset_tx_done_out(0) => tx_reset_done,
    gtwiz_reset_rx_done_out(0) => rx_reset_done,
    gtwiz_userdata_tx_in => MGT_USRWORD_i,
    gtwiz_userdata_rx_out => MGT_USRWORD_s,
    drpaddr_in => drpaddr_s,
    drpclk_in => drpclk_s,
    drpdi_in => drpdi_s,
    drpen_in => drpen_s,
    drpwe_in => drpwe_s,
--    eyescanreset_in => eyescanreset_o,
    gthrxn_in => RXn_i,
    gthrxp_in => RXp_i,
    gtrefclk0_in => gtrefclk0_s,
--    rxlpmen_in => rxlpmen_o,
--    rxrate_in => rxrate_o,
    rxslide_in => rxslide_s,
    rxusrclk_in => rx_wordclk_sig,
    rxusrclk2_in => rx_wordclk_sig,
--    txdiffctrl_in => txdiffctrl_o,
--    txpostcursor_in => txpostcursor_o,
--    txprecursor_in => txprecursor_o,
    txpippmen_in => txpippmen_s,
    txpippmovrden_in => txpippmovrden_s,
    txpippmpd_in => txpippmpd_s,
    txpippmsel_in => txpippmsel_s,
    txpippmstepsize_in => txpippmstepsize_in,
    txusrclk_in => tx_wordclk_sig,
    txusrclk2_in => tx_wordclk_sig,
    drpdo_out => drpdo_s,
    drprdy_out => drprdy_s,
    gthtxn_out => TXn_o,
    gthtxp_out => TXp_o,
    gtpowergood_out => open,
    rxoutclk_out => rxoutclk_sig,
    rxpmaresetdone_out => rxpmaresetdone,
    txoutclk_out => txoutclk_sig,
    txpmaresetdone_out => txpmaresetdone
         );

    txpippmen_s <= (others => '0');
    txpippmovrden_s <= (others => '0');
    txpippmsel_s <= (others => '0');
    txpippmpd_s <= (others => '0');
    txpippmstepsize_in <= (others => '0');
    
    drpaddr_s <= (others => '0');
    drpen_s <= (others => '0');
    drpdi_s <= (others => '0');
    drpwe_s <= (others => '0');
   rxrate_i <= (others => '0');
    
 --   txdiffctrl_i <= "11000110001100011000";
    txprecursor_i <= (others => '0');
    txpostcursor_i <= (others => '0');
    rxlpmen_i <= (others => '0');

end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--