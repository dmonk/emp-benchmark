generate_target synthesis [get_files emp_lpgbt_xlx_ku_mgt_ip_5g12.xci]
set_property used_in_synthesis false [get_files -of_object [get_files emp_lpgbt_xlx_ku_mgt_ip_5g12.xci] -filter {FILE_TYPE == XDC}]
set_property used_in_implementation false [get_files -of_object [get_files emp_lpgbt_xlx_ku_mgt_ip_5g12.xci] -filter {FILE_TYPE == XDC}]