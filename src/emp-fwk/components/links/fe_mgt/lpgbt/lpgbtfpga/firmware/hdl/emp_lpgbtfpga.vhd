-------------------------------------------------------
--! @file
--! @author Kirika Uchida <kirika.uchida@cern.ch> 
--! @version 0.0
--! @brief this module contains lpgbtfpga modules and mgt
-------------------------------------------------------

--! Include the IEEE VHDL standard library
library ieee;
use ieee.std_logic_1164.all;

--! Include the LpGBT-FPGA specific package
use work.lpgbtfpga_package.all;

--! Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

entity emp_lpgbtfpga is 
   GENERIC (
        N_CHANNELS                       : integer := 4;
        DATARATE                         : integer range 0 to 2 := DATARATE_10G24;       --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                              : integer range 0 to 2 := FEC12;       --! FEC selection can be: FEC5 or FEC12
        ALIGNMENT_MODE                   : integer range 0 to 1 := PCS;
        MGT_WORD_WIDTH                   : integer :=32;
        DEBUG                            : boolean := false
   );
   PORT (
        clk_p_i                                      : in  std_logic;   --! payload clock
 
        mgt_tx_rst_i                                 : in  std_logic;                         
        mgt_rx_rst_i                                 : in  std_logic;                       
        
        downlink_bypass_interleaver_i                : in  std_logic;                       --! Bypass downlink interleaver (test purpose only)
        downlink_bypass_fec_encoder_i                : in  std_logic;                       --! Bypass downlink FEC (test purpose only)
        downlink_bypass_scrambler_i                  : in  std_logic;                       --! Bypass downlink scrambler (test purpose only)
        uplink_bypass_interleaver_i                  : in  std_logic;                       --! Bypass uplink interleaver (test purpose only)
        uplink_bypass_fec_encoder_i                  : in  std_logic;                       --! Bypass uplink FEC (test purpose only)
        uplink_bypass_scrambler_i                    : in  std_logic;                       --! Bypass uplink scrambler (test purpose only)

        -- Down link
        downlink_clken_i                             : in  std_logic_vector(N_CHANNELS - 1 downto 0);
        downlink_user_data_i                         : in  std_logic_vector(32 * N_CHANNELS - 1 downto 0);   --! Downlink data (user)
        downlink_ec_data_i                           : in  std_logic_vector(N_CHANNELS * 2 - 1 downto 0);    --! Downlink EC field
        downlink_ic_data_i                           : in  std_logic_vector(N_CHANNELS * 2 - 1 downto 0);    --! Downlink IC field                      
        lpgbtfpga_downlink_rdy_o                     : out std_logic_vector(N_CHANNELS - 1 downto 0);       --! Downlink ready status
        
        -- Up link
        --! Clock enable pulsed when new data is ready 
        --! '1' in every 8th clocks after rx gearbox reset. The timing is controlled by c_conterInitValue in lpgbtfpga_rxGearbox module and MULTICYCLE_DELAY in emp_lpgbtfpga_uplink_module.      
        lpgbtfpga_uplink_clken_o                     : out std_logic_vector(N_CHANNELS - 1 downto 0);                
        lpgbtfpga_uplink_user_data_o                 : out std_logic_vector(230 * N_CHANNELS - 1 downto 0);  --! Uplink data (user)
        lpgbtfpga_uplink_ic_data_o                   : out std_logic_vector(2 * N_CHANNELS - 1 downto 0);    --! Uplink EC field
        lpgbtfpga_uplink_ec_data_o                   : out std_logic_vector(2 * N_CHANNELS - 1 downto 0);    --! Uplink IC field 
        lpgbtfpga_uplink_user_data_corrected_o       : out std_logic_vector(230 *(N_CHANNELS)-1 downto 0);   --! Flag allowing to know which bit(s) were toggled by the FEC
        lpgbtfpga_uplink_ic_data_corrected_o         : out std_logic_vector(2*N_CHANNELS-1 downto 0);        --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        lpgbtfpga_uplink_ec_data_corrected_o         : out std_logic_vector(2*N_CHANNELS-1 downto 0);    
        lpgbtfpga_uplink_rdy_o                       : out std_logic_vector(N_CHANNELS - 1 downto 0);        --! Uplink ready status
                
        -- MGT
        clk_mgtrefclk_i                              : in  std_logic;                       --! Transceiver serial clock
        clk_mgtfreedrpclk_i                          : in  std_logic;
        
        mgt_tx_clk_o                                 : out std_logic_vector(N_CHANNELS - 1 downto 0);
        mgt_rx_clk_o                                 : out std_logic_vector(N_CHANNELS - 1 downto 0);                
        mgt_tx_rdy_o                                 : out std_logic;
                                      
        testsig_o                                    : out std_logic_vector(9 downto 0);
        lpgbtfpga_downlink_test_o                    : out std_logic_vector(8*N_CHANNELS - 1 downto 0)
   ); 
end emp_lpgbtfpga;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of emp_lpgbtfpga is
           
        -- MGT:
        signal mgt_tx_clk_s                     : std_logic_vector(N_CHANNELS-1 downto 0);
        signal mgt_rx_clk_s                     : std_logic_vector(N_CHANNELS-1 downto 0);
        signal mgt_tx_rdy_s                     : std_logic;
        signal mgt_rx_rdy_s                     : std_logic;        
        signal uplink_frame_from_mgt_s           : std_logic_vector(MGT_WORD_WIDTH*N_CHANNELS-1 downto 0);
        signal downlink_frame_to_mgt_s           : std_logic_vector(MGT_WORD_WIDTH*N_CHANNELS-1 downto 0);        
        signal mgt_rx_slide_s                   : std_logic_vector(N_CHANNELS-1 downto 0);

        signal mgt_rxn_s                         : std_logic_vector(N_CHANNELS-1 downto 0);
        signal mgt_rxp_s                         : std_logic_vector(N_CHANNELS-1 downto 0);
begin                 --========####   Architecture Body   ####========--

    testsig_o(0) <= mgt_tx_rdy_s; -- from MGT
    testsig_o(1) <= mgt_rx_rdy_s; -- from MGT
     
    mgt_tx_clk_o  <= mgt_tx_clk_s;
    mgt_rx_clk_o  <= mgt_rx_clk_s;
    mgt_tx_rdy_o  <= mgt_tx_rdy_s;
   
    downlink_gen : for i in 0 to N_CHANNELS-1 GENERATE    
    begin

        lpgbtfptga_downlink_inst : ENTITY work.emp_lpgbtfpga_downlink
           GENERIC map(
                -- Expert parameters
                c_multicyleDelay             => 3,                           --! Multicycle delay: USEd to relax the timing constraints
                c_clockRatio                 => 8,                           --! Clock ratio is clock_out / 40 (shall be an integer - E.g.: 320/40 = 8)
                c_outputWidth                => MGT_WORD_WIDTH                           --! Transceiver's word size
           )
           PORT map(
                clk_p_i                      => clk_p_i,
                -- Clocks
                clk_i                        => mgt_tx_clk_s(i),            --! Downlink datapath clock (either 320 or 40MHz)
                clkEn_i                      => downlink_clken_i(i),         --! Clock enable (1 over 8 WHEN encoding runs @ 320Mhz, '1' @ 40MHz)
                rst_n_i                      => mgt_tx_rdy_s,               --! Downlink reset SIGNAL (Tx ready from the transceiver)
        
                -- Down link
                USErData_i                   => downlink_user_data_i(32 * (i+1) - 1 downto 32 * i),
                ECData_i                     => downlink_ec_data_i(2 * (i+1) - 1 downto 2 * i),
                ICData_i                     => downlink_ic_data_i(2 * (i+1) - 1 downto 2 * i),
          
                -- Output
                mgt_word_o                   => downlink_frame_to_mgt_s(MGT_WORD_WIDTH * (i+1) - 1 downto MGT_WORD_WIDTH * i),    --! Downlink encoded frame (IC + EC + User Data + FEC)
        
                -- Configuration
                interleaverBypass_i          => downlink_bypass_interleaver_i,                             --! Bypass downlink interleaver (test purpose only)
                encoderBypass_i              => downlink_bypass_fec_encoder_i,                              --! Bypass downlink FEC (test purpose only)
                scramblerBypass_i            => downlink_bypass_scrambler_i,                               --! Bypass downlink scrambler (test purpose only)
        
                -- Status
                rdy_o                        => lpgbtfpga_downlink_rdy_o(i),                                       --! Downlink ready status
                test_o                       => lpgbtfpga_downlink_test_o(8*(i+1)-1 downto 8*i)
           );
              
    end generate;        
 
 
     
    -- Uplink reset scheme:
    --      Step 1: Reset the MGT Rx (using signal)
    --      Step 2: Pattern search is reseted until the sta_mgtRxRdy_s signal is asserted
    --      Step 3: Rx gearbox reseted until the header is locked (sta_headeLocked_s)
    --      Step 4: Rx uplink datapath until the Rx gearbox is ready (sta_rxGbRdy_s)
 
    --upLinkFrame_from_mgt_s <= uplinkFrame_from_mgt_allch_s(31 downto 0);   
    uplink_gen : for i in 0 to N_CHANNELS-1 GENERATE   
    
        lpgbtfpga_uplink_inst : entity work.emp_lpgbtfpga_uplink
           generic map(
                -- General configuration
                DATARATE                        => DATARATE,                                   --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
                FEC                             => FEC,                                        --! FEC selection can be: FEC5 or FEC12
                -- ALIGNMENT_MODE               => ALIGNMENT_MODE,
                -- Expert parameters
                c_multicyleDelay                => 3,                                          --! Multicycle delay: USEd to relax the timing constraints
                c_clockRatio                    => 8,                                          --! Clock ratio is mgt_USErclk / 40 (shall be an integer)
                c_mgtWordWidth                  => MGT_WORD_WIDTH,                             --! Bus size of the input word
                c_allowedFalseHeader            => 5,                                         --! Number of false header allowed to avoid unlock on frame error
                c_allowedFalseHeaderOverN       => 64,                                         --! Number of header checked to know wether the lock is lost or not
                c_requiredTrueHeader            => 30,                                         --! Number of true header required to go in locked state
                c_bitslip_mindly                => 1,                                          --! Number of clock cycle required WHEN asserting the bitslip SIGNAL
                c_bitslip_waitdly               => 40                                          --! Number of clock cycle required before being back in a stable state
           )
           port map(
                clk_p_i                         => clk_p_i,
                -- Clock and reset
                clk_freeRunningClk_i            => '0',         --! currently not used. Used when c_resetOnEven = '1' at lpgbtfpga_framealigner
                uplinkClk_i                     => mgt_rx_clk_s(i),                            --! Input clock (Rx USEr clock from transceiver)
                uplinkClkOutEn_o                => lpgbtfpga_uplink_clken_o(i),                           --! Clock enable to be USEd in the USEr's logic
                uplinkRst_n_i                   => mgt_rx_rdy_s,                               --! Uplink reset SIGNAL (Rx ready from the transceiver)
        
                -- Input
                mgt_word_o                      => uplink_frame_from_mgt_s(MGT_WORD_WIDTH * (i+1) - 1 downto MGT_WORD_WIDTH * i),--! Input frame coming from the MGT
        
                -- Data
                USErData_o                      => lpgbtfpga_uplink_user_data_o(230*(i+1)-1 downto 230*i),  --! User output (decoded data). The payload size varies depENDing on the
                                                                                                --! datarate/FEC configuration:
                                                                                                --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                                --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                                --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                                --!     * *FEC12 / 10.24 Gbps*: 202bit
                EcData_o                        =>  lpgbtfpga_uplink_ec_data_o(2*(i+1)-1 downto 2*i),       --! EC field value received from the LpGBT
                IcData_o                        =>  lpgbtfpga_uplink_ic_data_o(2*(i+1)-1 downto 2*i),       --! IC field value received from the LpGBT
        
                -- Control
                bypassInterleaver_i             => uplink_bypass_interleaver_i,                   --! Bypass uplink interleaver (test purpose only)
                bypassFECEncoder_i              => uplink_bypass_fec_encoder_i,                    --! Bypass uplink FEC (test purpose only)
                bypassScrambler_i               => uplink_bypass_scrambler_i,                     --! Bypass uplink scrambler (test purpose only)
             
                -- Transceiver control
                mgt_bitslipCtrl_o               => mgt_rx_slide_s(i),                            --! Control the Bitslib/RxSlide PORT of the Mgt
        
                -- Status
                dataCorrected_o                 => lpgbtfpga_uplink_user_data_corrected_o(230*(i+1)-1 downto 230*i), --! Flag allowing to know which bit(s) were toggled by the FEC
                IcCorrected_o                   => lpgbtfpga_uplink_ic_data_corrected_o(2*(i+1)-1 downto 2*i),       --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
                EcCorrected_o                   => lpgbtfpga_uplink_ec_data_corrected_o(2*(i+1)-1 downto 2*i),       --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
                rdy_o                           => lpgbtfpga_uplink_rdy_o(i)                                   --! Ready SIGNAL from the uplink decoder
           );
        
    end generate;
    
    -- MGT
    mgt_10g_gen : if DATARATE = DATARATE_10G24 generate 
    
        mgt_inst: entity work.emp_lpgbt_xlx_ku_mgt_10g24
        generic map ( N_CHANNELS => N_CHANNELS )
           port map(
                -- Clocks
                MGT_REFCLK_i                 => clk_mgtrefclk_i,
                MGT_FREEDRPCLK_i             => clk_mgtfreedrpclk_i,
                        
                MGT_RXUSRCLK_o               => mgt_rx_clk_s,
                MGT_TXUSRCLK_o               => mgt_tx_clk_s,
                  
                -- Resets
                MGT_TXRESET_i                => mgt_tx_rst_i,
                MGT_RXRESET_i                => mgt_rx_rst_i,
                  
                -- Control & Status
                MGT_RXSlide_i                => mgt_rx_slide_s,    
                MGT_TXREADY_o                => mgt_tx_rdy_s,
                MGT_RXREADY_o                => mgt_rx_rdy_s,
                
                -- Data
                MGT_USRWORD_i                => downlink_frame_to_mgt_s,
                MGT_USRWORD_o                => uplink_frame_from_mgt_s,
                  
                RXn_i                        => mgt_rxn_s,
                RXp_i                        => mgt_rxp_s       
        --        TXn_o                        => mgt_txn_s,
        --        TXp_o                        => mgt_txp_s
           );
            
    end generate;
    
    mgt_5g_gen : if DATARATE = DATARATE_5G12 generate 
            
            signal mgt_frame_d  : std_logic_vector(15 downto 0); 
            signal mgt_rx_rst_d : std_logic;
            signal mgt_slide_d  : std_logic;
            signal mgt_rx_rdy_d : std_logic;
    
    begin
        mgt_inst: entity work.emp_lpgbt_xlx_ku_mgt_5g12
        generic map ( N_CHANNELS => N_CHANNELS )
           port map(
                -- Clocks
                MGT_REFCLK_i                 => clk_mgtrefclk_i,
                MGT_FREEDRPCLK_i             => clk_mgtfreedrpclk_i,
                        
                MGT_RXUSRCLK_o               => mgt_rx_clk_s,
                MGT_TXUSRCLK_o               => mgt_tx_clk_s,
                  
                -- Resets
                MGT_TXRESET_i                => mgt_tx_rst_i,
                MGT_RXRESET_i                => mgt_rx_rst_i,
                  
                -- Control & Status
                MGT_RXSlide_i                => mgt_rx_slide_s,    
                MGT_TXREADY_o                => mgt_tx_rdy_s,
                MGT_RXREADY_o                => mgt_rx_rdy_s,
                
                -- Data
                MGT_USRWORD_i                => downlink_frame_to_mgt_s,
                MGT_USRWORD_o                => uplink_frame_from_mgt_s,
                  
                RXn_i                        => mgt_rxn_s,
                RXp_i                        => mgt_rxp_s       
        --        TXn_o                        => mgt_txn_s,
        --        TXp_o                        => mgt_txp_s
           );
  
    
    delay_p : process (mgt_rx_clk_s(3))
    begin
        if rising_edge(mgt_rx_clk_s(3)) then
            mgt_frame_d <= uplink_frame_from_mgt_s(16*4-1 downto 16 * 3);
            mgt_rx_rst_d <= mgt_rx_rst_i;
            mgt_slide_d <= mgt_rx_slide_s(3);
            mgt_rx_rdy_d <= mgt_rx_rdy_s;
        end if;
    
    end process;
    
    end generate;
    
   debug_gen : if DEBUG = true generate 
    
    ila_ic_tx_inst : entity work.ila_sc
        PORT MAP (
        clk => clk_p_i,
        probe0 => downlink_ic_data_i(2*(3+1) - 1 downto 2 * 3),
        probe1 => downlink_ec_data_i(2*(3+1) - 1 downto 2 * 3),
        probe2 => downlink_user_data_i(32 * 3 + 1 downto 32 * 3),
        probe3 => "00"
        );     
    
    ila_mgt_rx_5g_inst : entity work.ila_mgt_rx_5g
        PORT MAP (
            clk => mgt_rx_clk_s(3),
            probe0(0) => mgt_rx_rst_i, 
            probe1(0) => mgt_rx_slide_s(3),
            probe2(0) => mgt_rx_rdy_s,
            probe3 => uplink_frame_from_mgt_s(16*4-1 downto 16 * 3),
            probe4 => downlink_frame_to_mgt_s(16*4-1 downto 16 * 3)
        );     
 
   end generate;
    
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
