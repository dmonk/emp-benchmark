-------------------------------------------------------
--! @file
--! @author Kirika Uchida <kirika.uchida@cern.ch> 
--! @version 0.0
--! @brief this module contains data framer and lpgbtfpga interface.
-------------------------------------------------------

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

use work.lpgbtfpga_package.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_data_types.all;
use work.emp_lpgbt_decl.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;

entity emp_lpgbt is
  generic (
     INDEX          : integer;
     N_CHANNELS     : integer := 4;
     DEBUG          : boolean := false     
     ); 
port 
(  

  --usrclk         : in  std_logic;
  clk              : in  std_logic; 
  rst              : in  std_logic; 
  csel_in          : in  std_logic_vector(2 downto 0);
  lpgbt_ipb_in     : in  ipb_wbus;
  lpgbt_ipb_out    : out ipb_rbus;
  df_ipb_in        : in  ipb_wbus_array(3 downto 0);
  df_ipb_out       : out ipb_rbus_array(3 downto 0); 
  refclk           : in  std_logic;
  clk_p            : in  std_logic;    
  rxclk_mon        : out std_logic;
  txclk_mon        : out std_logic;
  txdata_in        : in  ldata(N_CHANNELS - 1 DOWNTO 0); -- array of 64 bit vectors 
  rxdata_out       : out ldata(N_CHANNELS - 1 DOWNTO 0);
  cfg_hdr10        : out std_logic;
  cfg_hdr11        : out std_logic
);  
end emp_lpgbt;

architecture rtl of emp_lpgbt is
    
        -- constants
        constant      FEC       : integer := REGION_LPGBT_CONF(INDEX).rx_fec;
        constant DATARATE       : integer := REGION_LPGBT_CONF(INDEX).rx_datarate;
        constant ALIGNMENT_MODE : integer := REGION_LPGBT_CONF(INDEX).rx_alignment_mode;
    
    -- Signals:
 
        signal mgt_tx_rst_s                       : std_logic := '1';
        signal mgt_rx_rst_s                       : std_logic;
        signal mgt_tx_rdy_s                       : std_logic;

        -- Output clocks from mgt. These clocks loop back to mgt
        signal mgt_tx_clk_s                       : std_logic_vector(N_CHANNELS - 1 downto 0);
        signal mgt_rx_clk_s                       : std_logic_vector(N_CHANNELS - 1 downto 0);    
 
        -- lpgbtfpga ready signals
        signal lpgbtfpga_downlink_rdy_s           : std_logic_vector(N_CHANNELS - 1 downto 0);
        signal lpgbtfpga_uplink_rdy_s             : std_logic_vector(N_CHANNELS - 1 downto 0);

        -- data signals
        --- downlink       
        signal data_framer_downlink_clken_s           : std_logic_vector(N_CHANNELS - 1 downto 0);
        signal data_framer_downlink_user_data_s       : std_logic_vector(N_CHANNELS * 32 - 1 downto 0);
        signal data_framer_downlink_ec_data_s         : std_logic_vector(N_CHANNELS * 2 - 1 downto 0);
        signal data_framer_downlink_ic_data_s         : std_logic_vector(N_CHANNELS * 2 - 1 downto 0);
 
        --- uplink
        signal lpgbtfpga_uplink_clken_s               : std_logic_vector(N_CHANNELS - 1 downto 0); --! The data comes one clock later than clken from descrambler in lpgbtfpga      
        -- signal uplink_clken_p                         : std_logic_vector(N_CHANNELS - 1 downto 0); 
        signal lpgbtfpga_uplink_user_data_s           : std_logic_vector(230 * N_CHANNELS - 1 downto 0);
        signal lpgbtfpga_uplink_ec_data_s             : std_logic_vector(2 * N_CHANNELS - 1 downto 0);
        signal lpgbtfpga_uplink_ic_data_s             : std_logic_vector(2 * N_CHANNELS - 1 downto 0);
        signal lpgbtfpga_uplink_user_data_corrected_s : std_logic_vector(230 * N_CHANNELS - 1 downto 0);  --! Flag allowing to know which bit(s) were toggled by the FEC
        signal lpgbtfpga_uplink_ec_data_corrected_s   : std_logic_vector(2 * N_CHANNELS - 1 downto 0);    --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        signal lpgbtfpga_uplink_ic_data_corrected_s   : std_logic_vector(2 * N_CHANNELS - 1 downto 0);    
        signal data_framer_uplink_ec_spare_data_s     : std_logic_vector(2 * N_MAX_EC_SPARE * N_CHANNELS - 1 downto 0);
        signal data_framer_uplink_error_s             : std_logic_vector(N_CHANNELS - 1 downto 0);
                       
        -- configuration and control signals
        signal uplink_bypass_interleaver_s            : std_logic := '0';
        signal uplink_bypass_fec_encoder_s            : std_logic := '0';
        signal uplink_bypass_scrambler_s              : std_logic := '0';
                    
        signal downlink_bypass_interleaver_s          : std_logic := '0';
        signal downlink_bypass_fec_encoder_s          : std_logic := '0';          
        signal downlink_bypass_scrambler_s            : std_logic := '0';

        -- ipbus ctrl
        signal ipb_mgt_tx_rst                         : std_logic := '0'; 
        signal ipb_mgt_rx_rst                         : std_logic := '0';
        signal ipb_rx_lock_err_rst                    : std_logic_vector(3 downto 0);

        constant N_CTRL                                : integer := 3;
        constant N_STAT_CH                            : integer := 2;
        constant N_STAT                                : integer := N_STAT_CH * 4;
        signal ctrl                                   : ipb_reg_v(N_CTRL-1 downto 0);
        signal status                                 : ipb_reg_v(N_STAT-1 downto 0);
        signal stb_ctrl                               : std_logic_vector(N_CTRL-1 downto 0);

        -- vio configuration and control signals      
        signal vio_be_mgt_tx_rst                  : std_logic := '0';
        signal vio_be_mgt_rx_rst                  : std_logic := '0';

        -- vio status signals
        signal vio_be_uplink_ready                : std_logic_vector(3 downto 0);
        signal vio_be_downlink_ready              : std_logic_vector(3 downto 0);
        signal vio_be_uplink_error                : std_logic_vector(3 downto 0);
                       
        signal testsig                            : std_logic_vector(9 downto 0);
        signal lpgbtfpga_downlink_test_s          : std_logic_vector(8*N_CHANNELS - 1 downto 0);

        signal mgt_tx_clk_s_o                     : std_logic;      
        signal mgt_rx_clk_s_o                     : std_logic;
 
 
        signal lpgbtfpga_uplink_rdy_err_s         : std_logic_vector(N_CHANNELS - 1 downto 0);
        signal fec_err_counter_reset_s            : std_logic_vector(N_CHANNELS - 1 downto 0);
        signal fec_err_counter_s                  : std_logic_vector(32 * N_CHANNELS - 1 downto 0);       
--        attribute keep                            : boolean;
--        attribute keep of vio_be_uplink_ready     : signal is true;
--        attribute keep of vio_be_downlink_ready   : signal is true;    
--        attribute keep of vio_be_uplink_error     : signal is true;
--        attribute keep of vio_be_mgt_rx_rst       : signal is true;
--        attribute keep of vio_be_mgt_tx_rst       : signal is true;
                   
begin            

    cfg_hdr10 <= mgt_tx_clk_s_o;
    cfg_hdr11 <= mgt_rx_clk_s_o;

bufg_rx_clk : BUFG
    port map(
      i => mgt_rx_clk_s(3),
      o => mgt_rx_clk_s_o
      );
bufg_tx_clk : BUFG
    port map(
      i => mgt_tx_clk_s(3),
      o => mgt_tx_clk_s_o
      );

 
   clk_div: entity work.freq_ctr_div
    generic map(
      N_CLK => 2
    )
    port map(
      clk(0) => mgt_rx_clk_s(0),
      clk(1) => mgt_tx_clk_s(0),
      clkdiv(0) => rxclk_mon,
      clkdiv(1) => txclk_mon
    );
    
    mgt_tx_rst_s   <= vio_be_mgt_tx_rst or ipb_mgt_tx_rst;
    mgt_rx_rst_s   <= vio_be_mgt_rx_rst or ipb_mgt_rx_rst;

 
 
 
    data_framers_gen : for i in 0 to N_CHANNELS - 1 generate
 
        signal downlink_ic_data       : std_logic_vector(1 downto 0);
        signal downlink_ec_data       : std_logic_vector(1 downto 0);
        signal downlink_user_data     : std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        signal downlink_rdy           : std_logic;
        signal downlink_clken         : std_logic;

        signal uplink_clken_p         : std_logic_vector(1 downto 0);
        signal uplink_user_data       : std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        signal uplink_ic_data         : std_logic_vector(1 downto 0);
        signal uplink_ec_data         : std_logic_vector(1 downto 0);     
        
    begin
 
        uplink_data_pipe : process ( clk_p ) begin 
            
            if rising_edge(clk_p) then
                uplink_clken_p(0)             <= lpgbtfpga_uplink_clken_s(i);
                uplink_clken_p(1)             <= uplink_clken_p(0);
                uplink_user_data(229 downto 0)<= lpgbtfpga_uplink_user_data_s(230*(i+1)-1 downto 230*i);
                uplink_ic_data                <= lpgbtfpga_uplink_ic_data_s(2*(i+1)-1 downto 2*i);
                uplink_ec_data                <= lpgbtfpga_uplink_ec_data_s(2*(i+1)-1 downto 2*i);
            end if;
            
        end process;
 
        downlink_rdy <= lpgbtfpga_downlink_rdy_s(i);
        txClkEn_proc: process(downlink_rdy, clk_p)
            variable cnter : integer range 0 to 8;
        begin
            if downlink_rdy = '0' then
                cnter := 0;
                downlink_clken  <= '0';
            
            elsif rising_edge(clk_p) then
            
                cnter := cnter + 1;
                
                if cnter = 8 then
                    cnter := 0;                              
                end if;
                            
                downlink_clken  <= '0';
                if cnter = 0 then
                    downlink_clken   <= '1';  
                end if;
            
            end if;
            
        end process;
 
        data_framer_downlink_ic_data_s(2*(i+1)-1 downto 2*i)     <= downlink_ic_data;
        data_framer_downlink_ec_data_s(2*(i+1)-1 downto 2*i)     <= downlink_ec_data;
        data_framer_downlink_user_data_s(32*(i+1)-1 downto 32*i) <= downlink_user_data(31 downto 0);        
       
        data_framer_inst: entity work.emp_data_framer
        generic map(LPGBT => 1, 
                    INDEX => INDEX, 
                    CHANNEL_INDEX => i,
                    IC_SIMPLE     => REGION_DATA_FRAMER_CONF(INDEX)(i).IC_SIMPLE,
                    NO_EC => REGION_DATA_FRAMER_CONF(INDEX)(i).NO_EC,
                    N_EC_SPARE => REGION_DATA_FRAMER_CONF(INDEX)(i).N_EC_SPARE, 
                    EC_BROADCAST => REGION_DATA_FRAMER_CONF(INDEX)(i).EC_BROADCAST )
        port map( 
            clk_i                    => clk,
            rst_i                    => rst,
            ipb_in                   => df_ipb_in(i),
            ipb_out                  => df_ipb_out(i),  
            clk_p_i                  => clk_p,
            txdata_i                 => txdata_in(i),
            rxdata_o                 => rxdata_out(i),
            mgt_tx_rdy_i             => mgt_tx_rdy_s,
            uplink_rdy_i             => lpgbtfpga_uplink_rdy_s(i),
            downlink_rdy_i           => lpgbtfpga_downlink_rdy_s(i),    
            uplink_clk_i             => clk_p,
            uplink_clken_i           => uplink_clken_p(1),
            uplink_user_data_i       => uplink_user_data,
            uplink_ic_data_i         => uplink_ic_data,
            uplink_ec_data_i         => uplink_ec_data,
            downlink_clk_i           => clk_p,
            downlink_clken_i         => downlink_clken,
            downlink_clken_o         => data_framer_downlink_clken_s(i),
            downlink_user_data_o     => downlink_user_data,
            downlink_ic_data_o       => downlink_ic_data,
            downlink_ec_data_o       => downlink_ec_data
           );

    end generate;
    
    -- LpGBT FPGA
    lpgbtfpga_10g_gen : if DATARATE = DATARATE_10G24 generate
    
        constant THE_DATARATE               : integer := DATARATE_10G24;
        constant MGT_WORD_WIDTH             : integer := 32;
        
    begin
    
        lpgbtfpga_inst: entity work.emp_lpgbtfpga
        generic map( N_CHANNELS     => N_CHANNELS,
                     DATARATE       => THE_DATARATE,      --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
                     FEC            => FEC,           --! FEC sel
                     ALIGNMENT_MODE => ALIGNMENT_MODE,
                     MGT_WORD_WIDTH => MGT_WORD_WIDTH)
        port map (    
            clk_p_i                          => clk_p,
            mgt_tx_rst_i                     => mgt_tx_rst_s,
            mgt_rx_rst_i                     => mgt_rx_rst_s,
            
            downlink_bypass_interleaver_i    => downlink_bypass_interleaver_s,
            downlink_bypass_fec_encoder_i    => downlink_bypass_fec_encoder_s,
            downlink_bypass_scrambler_i      => downlink_bypass_scrambler_s,
            uplink_bypass_interleaver_i      => uplink_bypass_interleaver_s,
            uplink_bypass_fec_encoder_i      => uplink_bypass_fec_encoder_s,
            uplink_bypass_scrambler_i        => uplink_bypass_scrambler_s,
            
            -- Down link
            downlink_clken_i                 => data_framer_downlink_clken_s,
            downlink_user_data_i             => data_framer_downlink_user_data_s,
            downlink_ec_data_i               => data_framer_downlink_ec_data_s,
            downlink_ic_data_i               => data_framer_downlink_ic_data_s,   
            lpgbtfpga_downlink_rdy_o         => lpgbtfpga_downlink_rdy_s,
        
            -- Up link
            lpgbtfpga_uplink_clken_o                => lpgbtfpga_uplink_clken_s,
            lpgbtfpga_uplink_user_data_o            => lpgbtfpga_uplink_user_data_s,
            lpgbtfpga_uplink_ec_data_o              => lpgbtfpga_uplink_ec_data_s,
            lpgbtfpga_uplink_ic_data_o              => lpgbtfpga_uplink_ic_data_s,
    
            lpgbtfpga_uplink_user_data_corrected_o  => lpgbtfpga_uplink_user_data_corrected_s,                 --! Flag allowing to know which bit(s) were toggled by the FEC
            lpgbtfpga_uplink_ic_data_corrected_o    => lpgbtfpga_uplink_ic_data_corrected_s,                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
            lpgbtfpga_uplink_ec_data_corrected_o    => lpgbtfpga_uplink_ec_data_corrected_s,            
            lpgbtfpga_uplink_rdy_o                  => lpgbtfpga_uplink_rdy_s,
        
            -- MGT
            clk_mgtrefclk_i                         => refclk,
            clk_mgtfreedrpclk_i                     => clk,
            
            mgt_tx_clk_o                            => mgt_tx_clk_s,
            mgt_rx_clk_o                            => mgt_rx_clk_s,          
            mgt_tx_rdy_o                            => mgt_tx_rdy_s,
                    
            testsig_o                               => testsig,
            lpgbtfpga_downlink_test_o               => lpgbtfpga_downlink_test_s
        );
    end generate;

    lpgbtfpga_5g_gen : if DATARATE = DATARATE_5G12 generate
    
        constant THE_DATARATE               : integer := DATARATE_5G12;
        constant MGT_WORD_WIDTH             : integer := 16;
        
    begin
    
        lpgbtfpga_inst: entity work.emp_lpgbtfpga
        generic map( N_CHANNELS     => N_CHANNELS,
                     DATARATE       => THE_DATARATE,  --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
                     FEC            => FEC,           --! FEC sel
                     ALIGNMENT_MODE => ALIGNMENT_MODE,
                     MGT_WORD_WIDTH => MGT_WORD_WIDTH)
        port map (    
            clk_p_i                          => clk_p,
            mgt_tx_rst_i                     => mgt_tx_rst_s,
            mgt_rx_rst_i                     => mgt_rx_rst_s,
            
            downlink_bypass_interleaver_i    => downlink_bypass_interleaver_s,
            downlink_bypass_fec_encoder_i    => downlink_bypass_fec_encoder_s,
            downlink_bypass_scrambler_i      => downlink_bypass_scrambler_s,
            uplink_bypass_interleaver_i      => uplink_bypass_interleaver_s,
            uplink_bypass_fec_encoder_i      => uplink_bypass_fec_encoder_s,
            uplink_bypass_scrambler_i        => uplink_bypass_scrambler_s,
            
            -- Down link
            downlink_clken_i                 => data_framer_downlink_clken_s,
            downlink_user_data_i             => data_framer_downlink_user_data_s,
            downlink_ec_data_i               => data_framer_downlink_ec_data_s,
            downlink_ic_data_i               => data_framer_downlink_ic_data_s,   
            lpgbtfpga_downlink_rdy_o         => lpgbtfpga_downlink_rdy_s,
        
            -- Up link
            lpgbtfpga_uplink_clken_o                => lpgbtfpga_uplink_clken_s,
            lpgbtfpga_uplink_user_data_o            => lpgbtfpga_uplink_user_data_s,
            lpgbtfpga_uplink_ec_data_o              => lpgbtfpga_uplink_ec_data_s,
            lpgbtfpga_uplink_ic_data_o              => lpgbtfpga_uplink_ic_data_s,
    
            lpgbtfpga_uplink_user_data_corrected_o  => lpgbtfpga_uplink_user_data_corrected_s,                 --! Flag allowing to know which bit(s) were toggled by the FEC
            lpgbtfpga_uplink_ic_data_corrected_o    => lpgbtfpga_uplink_ic_data_corrected_s,                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
            lpgbtfpga_uplink_ec_data_corrected_o    => lpgbtfpga_uplink_ec_data_corrected_s,            
            lpgbtfpga_uplink_rdy_o                  => lpgbtfpga_uplink_rdy_s,
        
            -- MGT
            clk_mgtrefclk_i                         => refclk,
            clk_mgtfreedrpclk_i                     => clk,
            
            mgt_tx_clk_o                            => mgt_tx_clk_s,
            mgt_rx_clk_o                            => mgt_rx_clk_s,          
            mgt_tx_rdy_o                            => mgt_tx_rdy_s,
                    
            testsig_o                               => testsig,
            lpgbtfpga_downlink_test_o               => lpgbtfpga_downlink_test_s
        );
    end generate;
 
 
     fec_err_counter_inst : entity work.emp_lpgbt_fec_correction_counter 
        generic map(
            N_CHANNELS => N_CHANNELS
            )
        port map(
            clk_i                       => clk_p,
            clken_i                     => lpgbtfpga_uplink_clken_s,
            user_data_corrected_i       => lpgbtfpga_uplink_user_data_corrected_s,
            ic_data_corrected_i         => lpgbtfpga_uplink_ic_data_corrected_s,
            ec_data_corrected_i         => lpgbtfpga_uplink_ec_data_corrected_s,
            reset_i                     => fec_err_counter_reset_s,
            frame_err_counter_o         => fec_err_counter_s
    );
    
    debug_gen : if DEBUG generate
    vio_inst : entity work.emp_lpgbt_vio_0
      PORT MAP (
        clk           => clk_p,
        probe_in0     => vio_be_uplink_ready,
        probe_in1     => vio_be_downlink_ready,
        probe_in2     => vio_be_uplink_error,
        probe_in3     => testsig,
        probe_in4     => ctrl(0)(7 downto 0),
        probe_in5     => status(0)(12 downto 0),
        probe_in6     => lpgbtfpga_downlink_test_s(7 downto 0),
        probe_in7     => data_framer_downlink_user_data_s(31 downto 0),
        probe_in8     => lpgbtfpga_uplink_user_data_s(31 downto 0),
        probe_out0(0) => vio_be_mgt_tx_rst,
        probe_out0(1) => vio_be_mgt_rx_rst
      ); 
    end generate;
    
    vio_be_uplink_ready   <= lpgbtfpga_uplink_rdy_s;                    
    vio_be_downlink_ready <= lpgbtfpga_downlink_rdy_s;                  
    vio_be_uplink_error   <= data_framer_uplink_error_s;     

    

    -- IPbus Slave    
    csr: entity work.ipbus_ctrlreg_v
    generic map(
        N_CTRL => N_CTRL,
        N_STAT => N_STAT
        )
    port map(
        clk       => clk,
        reset     => rst,
        ipbus_in  => lpgbt_ipb_in,
        ipbus_out => lpgbt_ipb_out,
        d         => status,
        q         => ctrl,
        stb => stb_ctrl
        );
    
    status_reg_gen : for i in 0 to N_CHANNELS - 1 generate
        
        signal fec_err_counter_ipb                  : std_logic_vector(32 * N_CHANNELS - 1 downto 0);             
        signal err_counter_d : std_logic_vector(32 * N_CHANNELS - 1 downto 0);       

    begin
 
        rx_lock_err_p : process (clk_p, ipb_rx_lock_err_rst(i))
 
        begin
            if ipb_rx_lock_err_rst(i) = '1' then
                lpgbtfpga_uplink_rdy_err_s(i) <= '0';
            elsif rising_edge(clk_p) then
                if lpgbtfpga_uplink_rdy_s(i) = '0' then
                    lpgbtfpga_uplink_rdy_err_s(i) <= '1';
                end if;
            end if;
        end process;
 
        
        fec_err_counter_ipb_p : process (clk_p, ipb_mgt_rx_rst)
 
        begin
            if rising_edge(clk_p) then
                err_counter_d(i) <= fec_err_counter_s(i);
                if err_counter_d(i) = fec_err_counter_s(i) then
                    fec_err_counter_ipb(i) <= fec_err_counter_s(i);
                end if;
            end if;
        end process;
               
        status(N_STAT_CH*i)(0)                  <= lpgbtfpga_uplink_rdy_s(i);
        status(N_STAT_CH*i)(1)                  <= lpgbtfpga_downlink_rdy_s(i);
        status(N_STAT_CH*i)(2)                  <= data_framer_uplink_error_s(i);
        status(N_STAT_CH*i)(3)                  <= lpgbtfpga_uplink_rdy_err_s(i);
        status(N_STAT_CH*i)(11) <= mgt_rx_clk_s(i);
        status(N_STAT_CH*i)(12) <= mgt_tx_clk_s(i);  
        status(N_STAT_CH*i+1) <= fec_err_counter_ipb(32*(i+1)-1 downto 32*i);
    end generate;

    ipb_mgt_rx_rst                <= ctrl(0)(0) and stb_ctrl(0);
    ipb_mgt_tx_rst                <= ctrl(0)(1) and stb_ctrl(0);      
    uplink_bypass_interleaver_s   <= ctrl(0)(2);
    uplink_bypass_fec_encoder_s   <= ctrl(0)(3);
    uplink_bypass_scrambler_s     <= ctrl(0)(4);      
    downlink_bypass_scrambler_s   <= ctrl(0)(5);
    downlink_bypass_fec_encoder_s <= ctrl(0)(6);   
    downlink_bypass_interleaver_s <= ctrl(0)(7);

    fec_err_counter_reset_s       <= ctrl(1)(N_CHANNELS - 1 downto 0) when stb_ctrl(1) = '1' else (others => '0');
    ipb_rx_lock_err_rst           <= ctrl(2)(3 downto 0) when stb_ctrl(2) = '1' else (others => '0');
end rtl;
