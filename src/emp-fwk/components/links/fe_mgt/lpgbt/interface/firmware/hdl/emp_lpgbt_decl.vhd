-------------------------------------------------------
--! @file
--! @author Kirika Uchida <kirika.uchida@cern.ch> 
--! @version 0.0
--! @brief 

-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.lpgbtfpga_package.all;
use work.emp_device_decl.all;
package emp_lpgbt_decl is

 

    type region_lpgbt_conf_t is
    record
        rx_fec                 : integer; -- FEC5 or FEC12
        rx_datarate            : integer; -- DATARATE_5G12 or DATARATE_10G24
        rx_alignment_mode      : integer; -- PCS or PMA
    end record;

    type region_lpgbt_conf_array_t is array(0 to N_REGION - 1) of region_lpgbt_conf_t;
    constant kDummyRegionLpgbt : region_lpgbt_conf_t := (FEC12, DATARATE_10G24, PCS);

end emp_lpgbt_decl;
