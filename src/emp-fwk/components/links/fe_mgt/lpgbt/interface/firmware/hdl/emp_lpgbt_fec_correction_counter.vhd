----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/27/2020 02:28:04 PM
-- Design Name: 
-- Module Name: emp_lpgbt_fec_correction_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.lpgbtfpga_package.all;

entity emp_lpgbt_fec_correction_counter is
    generic(
        N_CHANNELS                       : integer := 4
        );      
    port (
        clk_i                       : in  std_logic;   --! payload clock
        clken_i                     : in  std_logic_vector(N_CHANNELS - 1 downto 0);                
        user_data_corrected_i       : in  std_logic_vector(230 *(N_CHANNELS)-1 downto 0);   --! Flag allowing to know which bit(s) were toggled by the FEC
        ic_data_corrected_i         : in  std_logic_vector(2*N_CHANNELS-1 downto 0);        --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        ec_data_corrected_i         : in  std_logic_vector(2*N_CHANNELS-1 downto 0);    
        reset_i                     : in  std_logic_vector(N_CHANNELS - 1 downto 0);        --! Uplink ready status
        frame_err_counter_o         : out std_logic_vector(32*N_CHANNELS - 1 downto 0)
);
end emp_lpgbt_fec_correction_counter;

architecture Behavioral of emp_lpgbt_fec_correction_counter is

begin


    counter_gen : for i in 0 to N_CHANNELS-1 GENERATE   
    
      signal user_data_s       : std_logic_vector(230-1 downto 0);   --! Flag allowing to know which bit(s) were toggled by the FEC
      signal ic_data_s         : std_logic_vector(1 downto 0);        --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
      signal ec_data_s         : std_logic_vector(1 downto 0);    
      signal err_counter_s     : std_logic_vector(31 downto 0);
      constant user_data_zero  : std_logic_vector(230-1 downto 0) := (others => '0');
    begin
 
        user_data_s <= user_data_corrected_i(230 *(i+1)-1 downto 230 * i); 
        ic_data_s   <= ic_data_corrected_i(2*(i+1)-1 downto 2*i);
        ec_data_s   <= ec_data_corrected_i(2*(i+1)-1 downto 2*i);
        frame_err_counter_o(32 * (i+1) - 1 downto 32 * i) <= err_counter_s;
 
        counter_p : process ( clk_i, reset_i(i) )

        variable counter    : integer range 0 to 2**32 -1;
 
        begin
        
            if reset_i(i) = '1' then
                counter := 0;
            
            elsif rising_edge(clk_i) then
                
                err_counter_s <= std_logic_vector( to_unsigned (counter, 32));
                
                if clken_i(i) = '1' then
                    if user_data_s /= user_data_zero or ic_data_s /= "00" or ec_data_s /= "00" then
                        counter := counter + 1;
                    end if;
                end if;

            end if;
            
            
        end process;
        
    end generate;

end Behavioral;
