-- Address decode logic for ipbus fabric

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package ipbus_decode_emp_scc_ec_ctrl is
  
  constant IPBUS_SEL_WIDTH: positive := 2; 
  
  subtype ipbus_sel_t is std_logic_vector(IPBUS_SEL_WIDTH - 1 downto 0);

  function ipbus_sel_emp_scc_ec_ctrl(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t;


  constant N_SLV_TxRAM                   : integer := 0;
  constant N_SLV_RxRAM                   : integer := 1;    
  constant N_SLV_CTRL                    : integer := 2;
  constant N_SLAVES                      : integer := 3;  
  
end ipbus_decode_emp_scc_ec_ctrl;

package body ipbus_decode_emp_scc_ec_ctrl is

  function ipbus_sel_emp_scc_ec_ctrl(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t is
    variable sel: ipbus_sel_t;
  begin

    if    std_match(addr, "----------------------------00--") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_TxRAM, IPBUS_SEL_WIDTH)); -- EC frame TxRAM from lpGBT: 0- CH+TrID+Controll+Address; 1- Data1+Data0+CMD+Len; 2- Data3+Dtat2
    elsif std_match(addr, "----------------------------01--") then         -- / base 0x00000002  
      sel := ipbus_sel_t(to_unsigned(N_SLV_RxRAM, IPBUS_SEL_WIDTH)); -- EC frame RxRAM to lpGBT:0- CH+TrID+Controll+Address; 1- Data1+Data0+Err+Len; 2- Data3+Dtat2 
    elsif std_match(addr, "----------------------------10--") then         -- / base 0x00000002  
      sel := ipbus_sel_t(to_unsigned(N_SLV_CTRL, IPBUS_SEL_WIDTH));  -- control and status registers
    else
        sel := ipbus_sel_t(to_unsigned(N_SLAVES, IPBUS_SEL_WIDTH));
    end if;

    return sel;

  end function ipbus_sel_emp_scc_ec_ctrl;

end ipbus_decode_emp_scc_ec_ctrl;

