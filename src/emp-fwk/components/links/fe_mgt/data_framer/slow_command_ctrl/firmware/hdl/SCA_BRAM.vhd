-- SCA DATA BRAM module
-- address 0x0 to reset the address of the bram to write data.
-- address 0x1 to write data.
-- 21.04.2020 Kirika Uchida

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use IEEE.NUMERIC_STD.ALL;
use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.ipbus_reg_types.all;
--use work.ipbus_decode_emp_slow_command_ctrl.all;
use work.ipbus_fabric_sel;

-- BRAM without output register (1 clock latancy)
entity SCA_BRAM is
    generic ( BRAM_TYPE : integer := 0 );  -- 0 for tx, 1 for rx
	port(
	   ipb_clk  : IN STD_LOGIC;
       ipbus_in : in ipb_wbus;
       ipbus_out: out ipb_rbus;
       frame_count_o : out std_logic_vector(8 downto 0);       
       clk      : IN STD_LOGIC;
       addr     : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
       d        : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
       q        : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
       we       : IN STD_LOGIC
       
    );
	
end SCA_BRAM;

architecture rtl of SCA_BRAM is

       constant ADDR_WIDTH : integer := 2; 
       
       constant BRAM_SEL       : integer := 0;
       constant BADDR_SEL      : integer := 1;
       constant FRAME_ADDR_SEL : integer := 2;
       
       signal sel : integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
       
       signal ack_s, ack_p    : std_logic;
       signal ipb_we_s : std_logic;
       signal trig_s   : std_logic;
      
       signal ipb_uaddr   : unsigned(9 downto 0);
       signal ipb_addr    : std_logic_vector(9 downto 0);
       signal ipb_dcount : unsigned(10 downto 0);

       signal rx_bram_doutb : std_logic_vector(31 downto 0);

begin
    
    tx_bram_gen : if BRAM_TYPE = 0 generate
        tx_bram : entity work.emp_slow_command_ctrl_csa_tx_bram
          PORT MAP (
            clka  => ipb_clk,
            wea(0)=> ipb_we_s,
            addra => ipb_addr(8 downto 0),
            dina  => ipbus_in.ipb_wdata,
            clkb  => clk,
            addrb => addr(6 downto 0),
            doutb => q
          );
        frame_count_o <= std_logic_vector(ipb_dcount(10 downto 2));
      end generate;
 
     rx_bram_gen : if BRAM_TYPE = 1 generate
        rx_bram : entity work.emp_slow_command_ctrl_csa_rx_bram
          PORT MAP (
            clka  => clk,
            wea(0)=> we,
            addra => addr(6 downto 0),
            dina  => d,
            clkb  => ipb_clk,
            addrb => ipb_addr(8 downto 0),
            doutb => rx_bram_doutb
          );
        frame_count_o <= std_logic_vector( unsigned( '0' & addr) + 1 );
      end generate;

 	ipb_addr <= std_logic_vector(ipb_uaddr);      
    ipb_dcount <= ( '0' & ipb_uaddr) + 1;
    
    sel <= to_integer(unsigned(ipbus_in.ipb_addr(ADDR_WIDTH-1 downto 0)));   
    
    ipb_we_s <= ipbus_in.ipb_write and ack_s when sel = BRAM_SEL;

 	process(ipb_clk)
	begin
        if rising_edge(ipb_clk) then
		      
            ack_s <= ipbus_in.ipb_strobe and not ack_s;	

            if ipbus_in.ipb_strobe = '1' then

                if sel = BRAM_SEL then

                    if ipbus_in.ipb_write = '0' then	      		
                        ipbus_out.ipb_rdata <= rx_bram_doutb;
                    end if;
                           
                    if ack_s = '1' then
                      ipb_uaddr <= ipb_uaddr + 1;
                    end if;                   
                    
                elsif sel = BADDR_SEL then

                    if ipbus_in.ipb_write = '1' then	      		      
                        ipb_uaddr <= unsigned(ipbus_in.ipb_wdata(9 downto 0));
                    else
                        ipbus_out.ipb_rdata <= (others => '0');
                        ipbus_out.ipb_rdata(9 downto 0) <= std_logic_vector(ipb_uaddr);
                    end if;  

                elsif sel = FRAME_ADDR_SEL then
                    
                    if ipbus_in.ipb_write = '0' then	    
                        ipbus_out.ipb_rdata <= (others => '0');                    
                        ipbus_out.ipb_rdata(7 downto 0) <= addr;
                    end if;
                    
                end if;
    
            end if;
		end if;

	end process;
 	
 	ipbus_out.ipb_ack <= ack_s;
	ipbus_out.ipb_err <= '0';
 	   
end rtl;

