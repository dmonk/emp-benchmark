-- Address decode logic for ipbus fabric

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package ipbus_decode_emp_scc_ic_ctrl is
  
  constant IPBUS_SEL_WIDTH: positive := 4; 
  
  subtype ipbus_sel_t is std_logic_vector(IPBUS_SEL_WIDTH - 1 downto 0);

  function ipbus_sel_emp_scc_ic_ctrl(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t;

   -- IC control signals  
  constant N_SLV_IC_TxDATA_REG         : integer := 0;  -- IC Tx FIFO data
  constant N_SLV_IC_TxConf_REG         : integer := 1;  -- IC Tx Configuration: lpGBT address 0..7bits, Internal address 23..8bits = 24bits
  constant N_SLV_IC_TxNWRead_REG       : integer := 2;  -- Number of words to be read (only for read transactions)
  constant N_SLV_IC_RxDATA_REG         : integer := 3;  -- IC Rx FIFO data
  
  constant N_SLV_IC_Status_REG         : integer := 4; -- IC Status register
  constant N_SLV_IC_Tx_start_write     : integer := 5; -- IC start write operation
  constant N_SLV_IC_Tx_start_read      : integer := 6; -- IC start read operation
  constant N_SLAVES                    : integer := 7;  
  
end ipbus_decode_emp_scc_ic_ctrl;

package body ipbus_decode_emp_scc_ic_ctrl is

  function ipbus_sel_emp_scc_ic_ctrl(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t is
    variable sel: ipbus_sel_t;
  begin
    
 -- IC control signals      
    if    std_match(addr, "----------------------------1000") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC_TxDATA_REG, IPBUS_SEL_WIDTH));   -- IC Tx FIFO data/ base 0x00001001 
    elsif std_match(addr, "----------------------------1001") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC_TxConf_REG, IPBUS_SEL_WIDTH));   -- IC Tx Configuration: lpGBT address 0..7bits, Internal address 23..8bits = 24bits / base 0x00001000 
    elsif std_match(addr, "----------------------------1010") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC_TxNWRead_REG, IPBUS_SEL_WIDTH)); -- IC Number of words to be read (only for read transactions) / base 0x00001002 
    elsif std_match(addr, "----------------------------1100") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC_RxDATA_REG, IPBUS_SEL_WIDTH));   -- IC Rx FIFO data / base 0x00001005     
    elsif std_match(addr, "----------------------------1101") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC_Status_REG, IPBUS_SEL_WIDTH));     -- IC Status register / base 0x00001004 
    elsif std_match(addr, "----------------------------1110") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC_Tx_start_write, IPBUS_SEL_WIDTH)); -- IC start write operation / base 0x00001009 
    elsif std_match(addr, "----------------------------1111") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC_Tx_start_read, IPBUS_SEL_WIDTH));  -- IC start read operation / base 0x00001008 
    else
        sel := ipbus_sel_t(to_unsigned(N_SLAVES, IPBUS_SEL_WIDTH));
    end if;
    
  return sel;

  end function ipbus_sel_emp_scc_ic_ctrl;

end ipbus_decode_emp_scc_ic_ctrl;

