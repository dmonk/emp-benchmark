----------------------------------------------------------------------------------
-- sca control ipbus module
-- Kirika Uchida  09.14.2020
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.sca_pkg.all;
use work.ipbus_decode_emp_scc_ec_ctrl.all; 

entity emp_scc_sca_ipbus is
    generic(
           g_NO_TX_BRAM            : boolean := false;
           g_NO_RX_BRAM            : boolean := false
           );
    Port ( 
		ipb_clk: in std_logic;
		ipb_rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
	
	    Tx_clk_i      : in std_logic; 
	    Tx_clk_en_i   : in std_logic := '1';		
		Rx_clk_i      : in std_logic := '0';
		Rx_clk_en_i   : in std_logic := '0';
 
       -- SCA commands
       start_input_cmd_o                : out std_logic;
       start_reset_cmd_o                : out std_logic;
       start_connect_cmd_o              : out std_logic;
       start_test_cmd_o                 : out std_logic;   	
       -- SCA frame fields
       tx_address_o                  : out std_logic_vector(7 downto 0);   --! Command: address field (According to the SCA manual)
       tx_transID_o                  : out std_logic_vector(7 downto 0);   --! Command: transaction ID field (According to the SCA manual)
       tx_channel_o                  : out std_logic_vector(7 downto 0);   --! Command: channel field (According to the SCA manual)
       tx_len_o                      : out std_logic_vector(7 downto 0);   --! Command: leght field (According to the SCA manual)
       tx_command_o                  : out std_logic_vector(7 downto 0);   --! Command: command field (According to the SCA manual)
       tx_data_o                     : out std_logic_vector(31 downto 0);  --! Command: data field (According to the SCA manual)
       rx_received_i                 : in std_logic;   --! Reply received flag (pulse)
       rx_address_i                  : in std_logic_vector(7 downto 0);           --! Reply: address field (According to the SCA manual)
       rx_control_i                  : in std_logic_vector(7 downto 0);           --! Reply: control field (According to the SCA manual)
       rx_transID_i                  : in std_logic_vector(7 downto 0);           --! Reply: transaction ID field (According to the SCA manual)
       rx_channel_i                  : in std_logic_vector(7 downto 0);           --! Reply: channel field (According to the SCA manual)
       rx_len_i                      : in std_logic_vector(7 downto 0);           --! Reply: len field (According to the SCA manual)
       rx_error_i                    : in std_logic_vector(7 downto 0);           --! Reply: error field (According to the SCA manual)
       rx_data_i                     : in std_logic_vector(31 downto 0);          --! Reply: data field (According to the SCA manual)  
       -- sca_rx module signals 
       rx_ongoing_i                     : in STD_LOGIC;
       rx_delimiter_found_i             : in STD_LOGIC;
       done_i                           : in std_logic
    );
end emp_scc_sca_ipbus;

architecture Behavioral of emp_scc_sca_ipbus is
       
       signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
	   signal ipbr: ipb_rbus_array(N_SLAVES - 1 downto 0);

       signal tx_frame_data_s           : std_logic_vector(127 downto 0);
       signal rx_we_s                   : STD_LOGIC := '0';
       signal rx_frame_data_s           : std_logic_vector(127 downto 0);
       
       signal EC_Frame_addr_s           : std_logic_vector(7 downto 0) := (others => '0');
       
       signal tx_bram_frame_count_s     : std_logic_vector(8 downto 0);
       signal rx_bram_frame_count_s     : std_logic_vector(8 downto 0);       

begin

	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_emp_scc_ec_ctrl(ipb_in.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );
 

------------------ Registers, FIFO, RAM for EC and IC module control signals -----------------
------------------------------ control signals for EC moduls  --------------------------------
-- SCA_Tx_RAM 0x00001fc2
-- ipBUS addr       <            0                 > <         1          > <  2  >
--            <SOF> <Address> <Control> <Tr.ID> <CH> <Lenght> <CMD> < Data> < Data> <FEC> <EOF>
-- nbit         8       8         8        8      8     8       8      16     16     16    8
---------------------------------------------------------------------------------------------
    SCA_TX_RAM_gen : if g_NO_TX_BRAM = false generate
       SCA_Tx_RAM: entity work.SCA_BRAM
       generic map( BRAM_TYPE => 0 )
       PORT MAP (
           ipb_clk       => ipb_clk,
           ipbus_in      => ipbw(N_SLV_TxRAM),
           ipbus_out     => ipbr(N_SLV_TxRAM),
           frame_count_o => tx_bram_frame_count_s,
           clk           => Tx_clk_i,
           addr          => EC_Frame_addr_s(7 downto 0),
           d             => (others => '0'),
           we            => '0',
           q             => tx_frame_data_s --tx_elink_H_so
        );
        tx_address_o <= tx_frame_data_s(7 downto 0);   -- Address
        --the control field is not set externally. It is generated inside the module.
        tx_transID_o <= tx_frame_data_s(23 downto 16); -- Tr.ID
        tx_channel_o <= tx_frame_data_s(31 downto 24); -- CH
        tx_len_o     <= tx_frame_data_s(39 downto 32); -- Lenght
        tx_command_o <= tx_frame_data_s(47 downto 40); -- CMD
        tx_data_o    <= tx_frame_data_s(79 downto 48); -- Data
    
    end generate;
---------------------------------------------------------------------------------------------
-- SCA_Rx_RAM 0x6400
-- ipBUS addr       <            0                 > <         1         > <  2 >
--            <SOF> <Address> <Control> <Tr.ID> <CH> <Lenght> <CMD> <Data> <Data> <FEC> <EOF>
-- nbit         8       8         8        8     8      8       8    16     16     16     8
-------------------------------------- SLAVE 9 ----------------------------------------------
    SCA_RX_RAM_gen : if g_NO_RX_BRAM = false generate
       SCA_Rx_RAM: entity work.SCA_BRAM
       generic MAP ( BRAM_TYPE => 1 )
       port map(
           ipb_clk       => ipb_clk,
           ipbus_in      => ipbw(N_SLV_RxRAM),
           ipbus_out     => ipbr(N_SLV_RxRAM),
           frame_count_o => rx_bram_frame_count_s,
           clk           => Rx_clk_i,
           addr          => EC_Frame_addr_s(7 downto 0),
           d             => rx_frame_data_s,
           we            => rx_we_s,
           q             => open
        );
        -- rx_we_s <= (rx_received_i and Rx_clk_en_i);
        rx_frame_data_s(7 downto 0)   <= rx_address_i;
        rx_frame_data_s(15 downto 8)  <= rx_control_i;
        rx_frame_data_s(23 downto 16) <= rx_transID_i;
        rx_frame_data_s(31 downto 24) <= rx_channel_i;
        rx_frame_data_s(39 downto 32) <= rx_len_i;
        rx_frame_data_s(47 downto 40) <= rx_error_i;
        rx_frame_data_s(79 downto 48) <= rx_data_i;
    end generate;
    
------------------------------------ SLAVE 11 ------------------------------------------
        ctrl_inst : entity work.emp_scc_sca_ctrl_ipbus
        port map(
		ipb_clk     => ipb_clk,
		ipb_rst     => ipb_rst,
		ipb_in      => ipbw(N_SLV_CTRL),
		ipb_out     => ipbr(N_SLV_CTRL),
	
	    Tx_clk_i    => Tx_clk_i,
	    Tx_clk_en_i => Tx_clk_en_i,
		
		Rx_clk_i      => Rx_clk_i,
		Rx_clk_en_i   => Rx_clk_en_i,
       -- SCA commands
       start_input_cmd_o                  => start_input_cmd_o,
       start_reset_cmd_o                  => start_reset_cmd_o,
       start_connect_cmd_o                => start_connect_cmd_o,
       start_test_cmd_o                   => start_test_cmd_o,
       -- sca_rx module signals 
       rx_ongoing_i                       => rx_ongoing_i,
       rx_delimiter_found_i               => rx_delimiter_found_i,
       -- to make signal rx_des_started which is a pulse made when sca deserializer find the delimiter on data.
       rx_data_received_i                 => rx_received_i,
       done_i                             => done_i,
       rx_bram_we_o                       => rx_we_s,
       -- bram address at the controller side
       EC_Frame_addr_o                    => EC_Frame_addr_s,
       tx_bram_frame_count_i              => tx_bram_frame_count_s,
       rx_bram_frame_count_i              => rx_bram_frame_count_s     
	);


end Behavioral;
