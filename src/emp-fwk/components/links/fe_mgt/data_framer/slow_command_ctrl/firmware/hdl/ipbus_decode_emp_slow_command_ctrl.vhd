-- Address decode logic for ipbus fabric

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package ipbus_decode_emp_slow_command_ctrl is
 -- 6 bits are available
 constant IPBUS_SEL_WIDTH: positive := 3; 
  subtype ipbus_sel_t is std_logic_vector(IPBUS_SEL_WIDTH - 1 downto 0);
  function ipbus_sel_emp_slow_command_ctrl(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t;


  constant N_SLV_CSR                   : integer := 0;
  constant N_SLV_IC                    : integer := 1;  
  constant N_SLV_EC                    : integer := 2; 
  constant N_SLV_EC_SPARE              : integer := 3;  
  
  constant N_SLAVES                    : integer := 4;  
end ipbus_decode_emp_slow_command_ctrl;

package body ipbus_decode_emp_slow_command_ctrl is

  function ipbus_sel_emp_slow_command_ctrl(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t is
    variable sel: ipbus_sel_t;
  begin

    if    std_match(addr, "--------------------------00----") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_CSR, IPBUS_SEL_WIDTH));         -- csr / base 0x00000000 
    elsif std_match(addr, "--------------------------01----") then    
      sel := ipbus_sel_t(to_unsigned(N_SLV_IC, IPBUS_SEL_WIDTH));          -- IC control
    elsif std_match(addr, "--------------------------10----") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_EC, IPBUS_SEL_WIDTH));          --  EC control   
  -- EC control signals   
    elsif std_match(addr, "--------------------------11----") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_EC_SPARE, IPBUS_SEL_WIDTH));    --  EC spare control 
    else
      sel := ipbus_sel_t(to_unsigned(N_SLAVES, IPBUS_SEL_WIDTH));
    end if;

    return sel;

  end function ipbus_sel_emp_slow_command_ctrl;

end ipbus_decode_emp_slow_command_ctrl;

