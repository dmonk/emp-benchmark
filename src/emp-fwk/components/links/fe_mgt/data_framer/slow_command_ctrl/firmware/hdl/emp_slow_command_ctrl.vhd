library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_data_framer_decl.all;

use work.sca_pkg.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity emp_slow_command_ctrl is
   generic (
        CHANNEL_INDEX       : integer := 0;
        -- IC configuration
        IC_SIMPLE           : boolean := false;
        g_ToLpGBT           : integer range 0 to 1 := 0;  --! 1 to use LpGBT. Otherwise, it should be 0
        -- EC configuration
        g_NO_EC                   : boolean := false;
        g_EC_BROADCAST            : boolean := false;
        g_SPARE_SCA_COUNT         : integer := 0;         --! Defines the maximum number of SCA SPARE that can be connected to this module
        g_Debug                   : boolean := false
    );

   Port ( 
        clk_i                    : in std_logic;  --- for ipbus (31MHz)
        rst_i                    : in std_logic;  --- for ipbus
        ipb_in                   : in ipb_wbus;   --- ipbus data in (from PC)
        ipb_out                  : out ipb_rbus;  --- ipbus data out (to PC)
 
        clk_p_i                  : in  std_logic;  --- 320MHz payload clock for channel buffer
        uplink_clk_i             : in  std_logic;
        uplink_rdy_i             : in  std_logic;
        downlink_rdy_i           : in  std_logic;  --- rdy_o (synchronized with downlinkClk_i, '1' when the logic is not reset) in lpgbtfpga_downlink    
        uplink_clken_i           : in  std_logic;  --- '1' when the data is stable.  Should be in 40 MHz.
        uplink_ic_data_i         : in  std_logic_vector(1 downto 0);
        uplink_ec_data_i         : in  std_logic_vector(1 downto 0);
        uplink_ec_spare_data_i   : in  std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);  
        downlink_clk_i           : in  std_logic;
        downlink_clken_i         : in  std_logic;
        downlink_clken_o         : out std_logic;
        downlink_ic_data_o       : out std_logic_vector(1 downto 0);
        downlink_ec_data_o       : out std_logic_vector(1 downto 0);
        downlink_ec_spare_data_o : out std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0)  
   );
end emp_slow_command_ctrl;

architecture Behavioral of emp_slow_command_ctrl is
	   	   
       -- clocks
       signal tx_clk_i  : std_logic;
       signal tx_clk_en : std_logic;     
       signal rx_clk_i  : std_logic;
       signal rx_clk_en : std_logic;
       
       -- initialize IC and EC moduls
       signal reset_to_gbtsc_s                        : STD_LOGIC := '0';
       signal ic_reset_to_gbtsc_s                     : std_logic := '0';
  
       -- sca commands
       signal sca_start_input_cmd_to_gbtsc_s          : std_logic_vector(g_SPARE_SCA_COUNT downto 0) := (others => '0');  ---! Send the command set in input
       signal sca_start_reset_cmd_to_gbtsc_s          : std_logic_vector(g_SPARE_SCA_COUNT downto 0) := (others => '0'); --! Send a reset command
       signal sca_start_connect_cmd_to_gbtsc_s        : std_logic_vector(g_SPARE_SCA_COUNT downto 0) := (others => '0'); --! Send a connect command
       signal sca_start_test_cmd_to_gbtsc_s           : std_logic_vector(g_SPARE_SCA_COUNT downto 0) := (others => '0'); --! Send a test command
       -- sca frame fields
       signal sca_tx_address_to_gbtsc_s               : reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: address field (According to the SCA manual)
       signal sca_tx_transID_to_gbtsc_s               : reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: transaction ID field (According to the SCA manual)
       signal sca_tx_channel_to_gbtsc_s               : reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: channel field (According to the SCA manual)
       signal sca_tx_len_to_gbtsc_s                   : reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: leght field (According to the SCA manual)
       signal sca_tx_command_to_gbtsc_s               : reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: command field (According to the SCA manual)
       signal sca_tx_data_to_gbtsc_s                  : reg32_arr(g_SPARE_SCA_COUNT downto 0);  --! Command: data field (According to the SCA manual)
       signal sca_rx_received_from_gbtsc_s            : std_logic_vector(g_SPARE_SCA_COUNT downto 0);   --! Reply received flag (pulse)
       signal sca_rx_address_from_gbtsc_s             : reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: address field (According to the SCA manual)
       signal sca_rx_control_from_gbtsc_s             : reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: control field (According to the SCA manual)
       signal sca_rx_transID_from_gbtsc_s             : reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: transaction ID field (According to the SCA manual)
       signal sca_rx_channel_from_gbtsc_s             : reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: channel field (According to the SCA manual)
       signal sca_rx_len_from_gbtsc_s                 : reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: len field (According to the SCA manual)
       signal sca_rx_error_from_gbtsc_s               : reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: error field (According to the SCA manual)
       signal sca_rx_data_from_gbtsc_s                : reg32_arr(g_SPARE_SCA_COUNT downto 0);          --! Reply: data field (According to the SCA manual)
       -- signals from sca_rx module
       signal sca_rx_delimiter_found_from_gbtsc_s     : std_logic_vector(g_SPARE_SCA_COUNT downto 0) := (others => '0');
       signal sca_rx_ongoing_from_gbtsc_s             : std_logic_vector(g_SPARE_SCA_COUNT downto 0) := (others => '0');
        -- EC lines
       signal ec_data_from_gbtsc_s                    : reg2_arr(g_SPARE_SCA_COUNT downto 0);           --! (TX) Array of bits to be mapped to the TX GBT-Frame
       signal ec_data_to_gbtsc_s                      : reg2_arr(g_SPARE_SCA_COUNT downto 0);           --! (RX) Array of bits to be mapped to the RX GBT-Frame
       
       -- IC configuration
       signal ic_tx_GBTx_address_to_gbtsc_s           : std_logic_vector(7 downto 0) :=(others => '0');
       signal ic_tx_register_addr_to_gbtsc_s          : std_logic_vector(15 downto 0) :=(others => '0');
       signal ic_tx_nb_to_be_read_to_gbtsc_s          : std_logic_vector(15 downto 0) :=(others => '0');
       -- IC Status
       signal ic_tx_ready_from_gbtsc_s                : std_logic;
       signal ic_rx_empty_from_gbtsc_s                : std_logic;
       -- IC FIFO control    
       signal ic_tx_wr_to_gbtsc_s                     : std_logic := '0';
       signal ic_tx_data_to_gbtsc_s                   : std_logic_vector(7 downto 0) :=(others => '0');
       signal ic_rx_rd_to_gbtsc_s                     : std_logic := '0';
       signal ic_rx_data_from_gbtsc_s                 : std_logic_vector(7 downto 0) :=(others => '0');     
       -- IC control
       signal ic_tx_start_write_to_gbtsc_s            : std_logic := '0';
       signal ic_tx_start_read_to_gbtsc_s             : std_logic := '0';  
       -- IC lines
       signal ic_data_from_gbtsc_s                    : std_logic_vector(1 downto 0);  --! (TX) Array of bits to be mapped to the TX GBT-Frame (bits 83/84)
       signal ic_data_to_gbtsc_s                      : std_logic_vector(1 downto 0) :=(others => '0');  --! (RX) Array of bits to be mapped to the RX GBT-Frame (bits 83/84)           

       signal test_s : std_logic_vector(7 downto 0);
begin

    debug_gen : if g_Debug = true generate

        ila_gen : if ( g_ToLpGBT = 1 and CHANNEL_INDEX = 0 ) or ( g_ToLpGBT = 0 and CHANNEL_INDEX = 0 ) generate
    
            ila_sc_inst : entity work.ila_sc
            PORT MAP (
            clk => clk_p_i,
            probe0 => ic_data_from_gbtsc_s,
            probe1 => ic_data_to_gbtsc_s,
            probe2 => ec_data_from_gbtsc_s(0),
            probe3 => ec_data_to_gbtsc_s(0)
            );
    
            ila_ic_inst : entity work.ila_ic
            port map(
            clk => clk_p_i,
            probe0=>ic_tx_GBTx_address_to_gbtsc_s,
            probe1=>ic_tx_register_addr_to_gbtsc_s,
            probe2=>ic_tx_nb_to_be_read_to_gbtsc_s,
            probe3(0)=>ic_tx_start_write_to_gbtsc_s,
            probe4(0)=>ic_tx_start_read_to_gbtsc_s,
            probe5(0)=>ic_tx_wr_to_gbtsc_s,
            probe6=>ic_tx_data_to_gbtsc_s,
            probe7(0)=>ic_rx_rd_to_gbtsc_s,
            probe8=>ic_rx_data_from_gbtsc_s,
            probe9=> test_s
            );
            ec_ila_gen : if g_NO_EC = false generate
            ila_ec_inst : entity work.ila_ec
            port map(
            clk => clk_p_i,
            probe0(0)       => sca_start_input_cmd_to_gbtsc_s(0),
            probe1(0)       => sca_start_reset_cmd_to_gbtsc_s(0),
            probe2(0)       => sca_start_connect_cmd_to_gbtsc_s(0),
            probe3(0)       => sca_start_test_cmd_to_gbtsc_s(0),
            probe4          => sca_tx_address_to_gbtsc_s(0),
            probe5          => sca_tx_transID_to_gbtsc_s(0),
            probe6          => sca_tx_channel_to_gbtsc_s(0),
            probe7          => sca_tx_len_to_gbtsc_s(0),
            probe8          => sca_tx_command_to_gbtsc_s(0),
            probe9          => sca_tx_data_to_gbtsc_s(0),
            probe10(0)      => sca_rx_received_from_gbtsc_s(0),
            probe11         => sca_rx_address_from_gbtsc_s(0),
            probe12         => sca_rx_control_from_gbtsc_s(0),
            probe13         => sca_rx_transID_from_gbtsc_s(0),
            probe14         => sca_rx_channel_from_gbtsc_s(0),
            probe15         => sca_rx_len_from_gbtsc_s(0),
            probe16         => sca_rx_error_from_gbtsc_s(0),
            probe17         => sca_rx_data_from_gbtsc_s(0),
            probe18(0)      => sca_rx_delimiter_found_from_gbtsc_s(0),
            probe19(0)      => sca_rx_ongoing_from_gbtsc_s(0),
            probe20         => ec_data_from_gbtsc_s(0),
            probe21         => ec_data_to_gbtsc_s(0)        
            );
           end generate;
           
           spare_gen : if g_SPARE_SCA_COUNT >= 1 generate
          
           ila_ec_spare_inst : entity work.ila_ec
            port map(
            clk => clk_p_i,
            probe0(0)       => sca_start_input_cmd_to_gbtsc_s(1),
            probe1(0)       => sca_start_reset_cmd_to_gbtsc_s(1),
            probe2(0)       => sca_start_connect_cmd_to_gbtsc_s(1),
            probe3(0)       => sca_start_test_cmd_to_gbtsc_s(1),
            probe4          => sca_tx_address_to_gbtsc_s(1),
            probe5          => sca_tx_transID_to_gbtsc_s(1),
            probe6          => sca_tx_channel_to_gbtsc_s(1),
            probe7          => sca_tx_len_to_gbtsc_s(1),
            probe8          => sca_tx_command_to_gbtsc_s(1),
            probe9          => sca_tx_data_to_gbtsc_s(1),
            probe10(0)      => sca_rx_received_from_gbtsc_s(1),
            probe11         => sca_rx_address_from_gbtsc_s(1),
            probe12         => sca_rx_control_from_gbtsc_s(1),
            probe13         => sca_rx_transID_from_gbtsc_s(1),
            probe14         => sca_rx_channel_from_gbtsc_s(1),
            probe15         => sca_rx_len_from_gbtsc_s(1),
            probe16         => sca_rx_error_from_gbtsc_s(1),
            probe17         => sca_rx_data_from_gbtsc_s(1),
            probe18(0)      => sca_rx_delimiter_found_from_gbtsc_s(1),
            probe19(0)      => sca_rx_ongoing_from_gbtsc_s(1),
            probe20         => ec_data_from_gbtsc_s(1),
            probe21         => ec_data_to_gbtsc_s(1)
            );
           end generate;
       
       end generate;
   
    end generate;
    
    ic_data_to_gbtsc_s <= uplink_ic_data_i;
    downlink_ic_data_o <= ic_data_from_gbtsc_s;
 
    ec_data_i_gen : if g_NO_EC = false generate   
        ec_data_to_gbtsc_s(0) <= uplink_ec_data_i;

    end generate;
    ec_data_o_gen : if g_NO_EC = false or g_EC_BROADCAST = true generate
        downlink_ec_data_o <= ec_data_from_gbtsc_s(0);
    end generate;
    
    ec_spare_data_gen : if g_SPARE_SCA_COUNT /= 0 generate    
        ec_spare_data_i_gen : for i in 1 to g_SPARE_SCA_COUNT generate
            ec_data_to_gbtsc_s(i) <= uplink_ec_spare_data_i((i-1)*2+1 downto (i-1)*2);
            downlink_ec_spare_data_o((i-1)*2+1 downto (i-1)*2) <= ec_data_from_gbtsc_s(i);
        end generate;
    end generate;
    
    downlink_clken_o <= downlink_clken_i;
    tx_clk_i      <= downlink_clk_i;
    rx_clk_i      <= uplink_clk_i;

    tx_clk_en <= downlink_clken_i;
    rx_clk_en <= uplink_clken_i;	
		
 ipbus_gen : entity work.emp_slow_command_ctrl_ipbus
    generic map( 
        LpGBT          => g_ToLpGBT,
        IC_SIMPLE      => IC_SIMPLE,
        g_NO_EC        => g_NO_EC,
        g_EC_BROADCAST => g_EC_BROADCAST,
        g_SPARE_SCA_COUNT => g_SPARE_SCA_COUNT
        )
	port map(
		ipb_clk => clk_i,
		ipb_rst => rst_i,
		ipb_in  => ipb_in,
		ipb_out => ipb_out,

	    Tx_clk_i                             => tx_clk_i,
	    Tx_clk_en_i                          => tx_clk_en,
		Rx_clk_i                             => rx_clk_i,
		Rx_clk_en_i                          => rx_clk_en,    
  		reset_to_gbtsc_o                     => reset_to_gbtsc_s,
  		ic_reset_to_gbtsc_o                  => ic_reset_to_gbtsc_s,
        ic_tx_GBTx_address_to_gbtsc_o        => ic_tx_GBTx_address_to_gbtsc_s,
        ic_tx_register_addr_to_gbtsc_o       => ic_tx_register_addr_to_gbtsc_s,
        ic_tx_nb_to_be_read_to_gbtsc_o       => ic_tx_nb_to_be_read_to_gbtsc_s,
	    ic_tx_ready_from_gbtsc_i             => ic_tx_ready_from_gbtsc_s,
        ic_rx_empty_from_gbtsc_i             => ic_rx_empty_from_gbtsc_s,        
        ic_tx_wr_to_gbtsc_o                  => ic_tx_wr_to_gbtsc_s,  
        ic_tx_data_to_gbtsc_o                => ic_tx_data_to_gbtsc_s,
        ic_rx_rd_to_gbtsc_o                  => ic_rx_rd_to_gbtsc_s, 
		ic_rx_data_from_gbtsc_i              => ic_rx_data_from_gbtsc_s,
		ic_tx_start_write_to_gbtsc_o         => ic_tx_start_write_to_gbtsc_s,  
        ic_tx_start_read_to_gbtsc_o          => ic_tx_start_read_to_gbtsc_s,
        sca_start_input_cmd_to_gbtsc_o       => sca_start_input_cmd_to_gbtsc_s,
        sca_start_reset_cmd_to_gbtsc_o       => sca_start_reset_cmd_to_gbtsc_s,
        sca_start_connect_cmd_to_gbtsc_o     => sca_start_connect_cmd_to_gbtsc_s,
        sca_start_test_cmd_to_gbtsc_o        => sca_start_test_cmd_to_gbtsc_s,
        sca_tx_address_to_gbtsc_o            => sca_tx_address_to_gbtsc_s,
        sca_tx_transID_to_gbtsc_o            => sca_tx_transID_to_gbtsc_s,
        sca_tx_channel_to_gbtsc_o            => sca_tx_channel_to_gbtsc_s,
        sca_tx_len_to_gbtsc_o                => sca_tx_len_to_gbtsc_s,
        sca_tx_command_to_gbtsc_o            => sca_tx_command_to_gbtsc_s,
        sca_tx_data_to_gbtsc_o               => sca_tx_data_to_gbtsc_s,
        sca_rx_received_from_gbtsc_i         => sca_rx_received_from_gbtsc_s,
        sca_rx_address_from_gbtsc_i          => sca_rx_address_from_gbtsc_s,
        sca_rx_control_from_gbtsc_i          => sca_rx_control_from_gbtsc_s,
        sca_rx_transID_from_gbtsc_i          => sca_rx_transID_from_gbtsc_s,
        sca_rx_channel_from_gbtsc_i          => sca_rx_channel_from_gbtsc_s,
        sca_rx_len_from_gbtsc_i              => sca_rx_len_from_gbtsc_s,
        sca_rx_error_from_gbtsc_i            => sca_rx_error_from_gbtsc_s,
        sca_rx_data_from_gbtsc_i             => sca_rx_data_from_gbtsc_s,
        sca_rx_delimiter_found_from_gbtsc_i  => sca_rx_delimiter_found_from_gbtsc_s,
        sca_rx_ongoing_from_gbtsc_i          => sca_rx_ongoing_from_gbtsc_s,
        test_o                               => test_s
	);
 

   sc_inst: entity work.gbtsc_top
        generic map(
            CHANNEL_INDEX => CHANNEL_INDEX,
        -- IC configuration
            g_IC_FIFO_DEPTH     => 16,  -- Defines the depth of the FIFO used to handle the Internal control (Max. number of words/bytes can be read/write from/to a GBTx)
            g_ToLpGBT           => g_ToLpGBT,
        -- EC configuration
            g_SCA_COUNT         => 1 + g_SPARE_SCA_COUNT
        )
        port map(
       -- Clock & reset
            tx_clk_i                => tx_clk_i,
            tx_clk_en               => tx_clk_en,

            rx_clk_i                => rx_clk_i,
            rx_clk_en               => rx_clk_en,

            rx_reset_i              => reset_to_gbtsc_s, -- asynchronous
            tx_reset_i              => reset_to_gbtsc_s, -- asynchronous
            ic_reset_i              => ic_reset_to_gbtsc_s, -- asynchronous
       -- IC configuration
            tx_GBTx_address_i       => ic_tx_GBTx_address_to_gbtsc_s,
            tx_register_addr_i      => ic_tx_register_addr_to_gbtsc_s,
            tx_nb_to_be_read_i      => ic_tx_nb_to_be_read_to_gbtsc_s,
            parity_err_mask_i       => X"00",
       -- IC Status
            tx_ready_o              => ic_tx_ready_from_gbtsc_s,
            rx_empty_o              => ic_rx_empty_from_gbtsc_s,
       -- IC FIFO control
            --wr_clk_i                => Tx_clk_i, 
            --rd_clk_i                => Rx_clk_i,
            wr_clk_i                => clk_i, 
            rd_clk_i                => clk_i,
      -- IC FIFO control      
            tx_wr_i                 => ic_tx_wr_to_gbtsc_s,
            tx_data_to_gbtx_i       => ic_tx_data_to_gbtsc_s,
            rx_rd_i                 => ic_rx_rd_to_gbtsc_s,
            rx_data_from_gbtx_o     => ic_rx_data_from_gbtsc_s,
       -- IC control
            tx_start_write_i        => ic_tx_start_write_to_gbtsc_s,
            tx_start_read_i         => ic_tx_start_read_to_gbtsc_s,
       -- SCA control
            start_input_cmd_i       => sca_start_input_cmd_to_gbtsc_s,
            start_reset_cmd_i       => sca_start_reset_cmd_to_gbtsc_s,
            start_connect_cmd_i     => sca_start_connect_cmd_to_gbtsc_s,
            start_test_cmd_i        => sca_start_test_cmd_to_gbtsc_s,
            inject_crc_error        => (others => '0'),
       -- SCA command
            tx_address_i            => sca_tx_address_to_gbtsc_s,
            tx_transID_i            => sca_tx_transID_to_gbtsc_s,
            tx_channel_i            => sca_tx_channel_to_gbtsc_s,
            tx_len_i                => sca_tx_len_to_gbtsc_s,
            tx_command_i            => sca_tx_command_to_gbtsc_s,
            tx_data_i               => sca_tx_data_to_gbtsc_s,
            rx_received_o           => sca_rx_received_from_gbtsc_s,
            rx_address_o            => sca_rx_address_from_gbtsc_s,
            rx_control_o            => sca_rx_control_from_gbtsc_s,
            rx_transID_o            => sca_rx_transID_from_gbtsc_s,
            rx_channel_o            => sca_rx_channel_from_gbtsc_s,
            rx_len_o                => sca_rx_len_from_gbtsc_s,
            rx_error_o              => sca_rx_error_from_gbtsc_s,
            rx_data_o               => sca_rx_data_from_gbtsc_s,
       -- sca_rx module signals
            delimiter_found_o       => sca_rx_delimiter_found_from_gbtsc_s,
            ongoing_o               => sca_rx_ongoing_from_gbtsc_s,
       -- EC line
            ec_data_o               => ec_data_from_gbtsc_s,
            ec_data_i               => ec_data_to_gbtsc_s,
       -- IC lines
            ic_data_o               => ic_data_from_gbtsc_s,
            ic_data_i               => ic_data_to_gbtsc_s
        );
 
end Behavioral;
