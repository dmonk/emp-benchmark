----------------------------------------------------------------------------------
-- ic control ipbus module
-- Kirika Uchida  09.14.2020
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_emp_scc_ic_ctrl.all;


entity emp_scc_ic_ctrl_ipbus_auto is
    generic (  LpGBT : integer := 0 );
      port (
        ipb_clk: in std_logic;
		ipb_rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		
        tx_GBTx_address_o       : out std_logic_vector(7 downto 0);
        tx_register_addr_o      : out std_logic_vector(15 downto 0);
        tx_nb_to_be_read_o      : out std_logic_vector(15 downto 0);		
	    tx_ready_i              : in  std_logic;
        rx_empty_i              : in  std_logic;
        tx_wr_o                 : out std_logic; 
        tx_data_o               : out std_logic_vector(7 downto 0);
        rx_rd_o                 : out std_logic;               
		rx_data_i               : in  std_logic_vector(7 downto 0);
        tx_start_read_o         : out std_logic;
		tx_start_write_o        : out std_logic;
		ic_reset_o              : out std_logic
		);        
end emp_scc_ic_ctrl_ipbus_auto;

architecture Behavioral of emp_scc_ic_ctrl_ipbus_auto is

    type fsm_type is (idle, txdata_fifo_proc, txdata_dfifo_read, txdata_ready, rxdata_read);
    signal fsm : fsm_type;

    signal reset_s                 : std_logic := '0';
    signal txdata_fifo_srst_s      : std_logic := '0';
    signal txdata_dfifo_srst_s     : std_logic := '0';
    signal rxdata_fifo_srst_s      : std_logic := '0';
        
    signal tx_rw                 : std_logic;
    signal tx_GBTx_address_s     : std_logic_vector(6 downto 0);
    signal tx_other_data_s       : std_logic_vector(11 downto 0);
    signal tx_nb_to_be_read_s    : std_logic_vector(11 downto 0);
    signal tx_nb_to_be_sent_s    : std_logic_vector(11 downto 0);

    signal rx_data_nread_s       : std_logic_vector(15 downto 0);
    signal err_s                 : std_logic;
    signal rx_rd_p               : std_logic_vector(2 downto 0);
    signal rxdata_ready_s        : std_logic;

    signal txdata_fifo_srst        : std_logic := '0';
    signal txdata_fifo_din         : std_logic_vector(31 downto 0);
    signal txdata_fifo_wr_en       : std_logic;
    signal txdata_fifo_rd_en       : std_logic;
    signal txdata_fifo_dout        : std_logic_vector(31 downto 0);
    signal txdata_fifo_full        : std_logic;
    signal txdata_fifo_empty       : std_logic;
    signal txdata_fifo_wr_rst_busy : std_logic;
    signal txdata_fifo_rd_rst_busy : std_logic;
 
    signal txdata_dfifo_srst        : std_logic := '0';
    signal txdata_dfifo_din         : std_logic_vector(7 downto 0);
    signal txdata_dfifo_wr_en       : std_logic;
    signal txdata_dfifo_rd_en       : std_logic;
    signal txdata_dfifo_dout        : std_logic_vector(7 downto 0);
    signal txdata_dfifo_full        : std_logic;
    signal txdata_dfifo_empty       : std_logic;
    signal txdata_dfifo_wr_rst_busy : std_logic;
    signal txdata_dfifo_rd_rst_busy : std_logic;
    
    signal rxdata_fifo_srst        : std_logic := '0';
    signal rxdata_fifo_din         : std_logic_vector(63 downto 0);
    signal rxdata_fifo_wr_en       : std_logic;
    signal rxdata_fifo_rd_en       : std_logic;
    signal rxdata_fifo_dout        : std_logic_vector(31 downto 0);
    signal rxdata_fifo_full        : std_logic;
    signal rxdata_fifo_empty       : std_logic;
    signal rxdata_fifo_wr_rst_busy : std_logic;
    signal rxdata_fifo_rd_rst_busy : std_logic;
  
    signal reply_data_count        : unsigned(31 downto 0);
  

    constant ADDR_WIDTH : integer := 4; 
    
    constant RESET_IC_CTRL_SEL              : integer := 0;
    constant RESET_IC_CTRL_GLOBAL_BIT       : integer := 0;
    constant RESET_IC_CTRL_TXDATA_FIFO_BIT  : integer := 1;
    constant RESET_IC_CTRL_TXDATA_DFIFO_BIT : integer := 2;
    constant RESET_IC_CTRL_RXDATA_FIFO_BIT  : integer := 3;
        
    constant STATUS_SEL                     : integer := 1;
    constant STATUS_TX_READY_BIT            : integer := 0;
    constant STATUS_RX_EMPTY_BIT            : integer := 1;
    constant STATUS_RXDATA_FIFO_EMPTY_BIT   : integer := 2;
    constant STATUS_FSM_BIT_OFFSET          : integer := 3;
    
    constant REPLY_DATA_COUNT_SEL           : integer := 2;
    
    constant TXDATA_FIFO_SEL                : integer := 3;
    constant TXDATA_DFIFO_SEL               : integer := 4;
    constant RXDATA_FIFO_SEL                : integer := 5;

    signal sel : integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
       
    signal ack_s    : std_logic;
    signal strobe_d : std_logic;
    signal reset_ipb                    : std_logic := '0';
    signal txdata_fifo_srst_ipb         : std_logic := '0';
    signal txdata_dfifo_srst_ipb        : std_logic := '0';
    signal rxdata_fifo_srst_ipb         : std_logic := '0';    
    signal rxdata_fifo_data_rdy         : std_logic := '0';
    signal fsm_ipb                      : std_logic_vector(2 downto 0);

    signal CHIPADDR   : integer range 0 to 6;
    signal NREAD_L    : integer range 0 to 6;
    signal NREAD_H    : integer range 0 to 6;
    signal MEMADDR_L  : integer range 0 to 6;
    signal MEMADDR_H  : integer range 0 to 6;    
begin

    rx_rd_o <= rx_rd_p(0);
    tx_GBTx_address_o <= '0' & tx_GBTx_address_s;
    tx_nb_to_be_read_o <= "0000" & tx_nb_to_be_read_s;
    reset_s <= reset_ipb;
    txdata_fifo_srst  <= txdata_fifo_srst_s  or txdata_fifo_srst_ipb;
    txdata_dfifo_srst <= txdata_dfifo_srst_s or txdata_dfifo_srst_ipb; 
    rxdata_fifo_srst  <= rxdata_fifo_srst_s  or rxdata_fifo_srst_ipb;
    
    gbt_const_gen : if LpGBT = 0 generate
        CHIPADDR <= 1;
        NREAD_L <= 3;
        NREAD_H <= 4;
        MEMADDR_L  <= 5;
        MEMADDR_H  <= 6;       
    end generate;
 
    lpgbt_const_gen : if LpGBT = 1 generate
        CHIPADDR <= 0;
        NREAD_L <= 3;
        NREAD_H <= 4;
        MEMADDR_L  <= 5;
        MEMADDR_H  <= 6;       
    end generate;
           
    -- gbt_ic interface
    ic_ctrl_p : process ( ipb_clk, reset_s )
    variable reset_on : boolean;
    variable timer    : integer range 0 to 2**12;
    variable rx_nread : integer range 0 to 2**12; 
    variable rx_iread : integer range 0 to 2**12; 
    begin
    
        if reset_s = '1' then

            reset_on := true;
            tx_start_read_o <= '0';
            tx_start_write_o <= '0';
            tx_wr_o <= '0';
            rx_rd_p(0) <= '0';
            rxdata_ready_s <= '0';
                
            txdata_fifo_rd_en  <= '0';
            txdata_dfifo_rd_en <= '0';
            rxdata_fifo_wr_en  <= '0';  
            rx_iread := 0;
        
            reply_data_count <= (others => '0');
        
        elsif rising_edge(ipb_clk) then

            -- reset
            txdata_fifo_srst_s <= '0';
            txdata_dfifo_srst_s <= '0';
            rxdata_fifo_srst_s <= '0';
            ic_reset_o <= '0';
            if reset_on then
                txdata_fifo_srst_s <= '1';
                txdata_dfifo_srst_s <= '1';
                rxdata_fifo_srst_s <= '1';
                ic_reset_o <= '1';
                reset_on := false;
                fsm <= idle;
            end if;
            
            -- IC tx -> rx process
            tx_start_read_o <= '0';
            tx_start_write_o <= '0';
            tx_wr_o <= '0';
            rx_rd_p(0) <= '0';            
            txdata_fifo_rd_en  <= '0';
            txdata_dfifo_rd_en <= '0';
            rxdata_ready_s  <= '0';
            rxdata_fifo_wr_en <= rxdata_ready_s;
            rx_rd_p(2 downto 1) <= rx_rd_p(1 downto 0);
            case fsm is
            when idle =>  
                if txdata_fifo_empty = '0' and tx_ready_i = '1' then
                    txdata_fifo_rd_en     <= '1';
                    fsm <= txdata_fifo_proc;
                    timer := 0;
                end if;
                
            when txdata_fifo_proc =>
                if timer = 1 then
                    tx_rw              <= txdata_fifo_dout(0);
                    tx_GBTx_address_s  <= txdata_fifo_dout(7 downto 1);
                    tx_register_addr_o <= ( others => '0');
                    tx_register_addr_o(11 downto 0) <= txdata_fifo_dout(19 downto 8);
                    tx_other_data_s    <= txdata_fifo_dout(31 downto 20);		 
                elsif timer = 2 then
                    if tx_rw = '0' then
                        if txdata_dfifo_empty = '0' then
                            tx_nb_to_be_sent_s <= tx_other_data_s;
                            txdata_dfifo_rd_en <= '1';
                            timer := 0;
                            fsm <= txdata_dfifo_read;
                        else
                            tx_nb_to_be_sent_s <= x"001";
                            tx_data_o <= tx_other_data_s(7 downto 0);
                            tx_wr_o <= '1';
                            fsm <= txdata_ready;
                        end if;                  
                    else
                        tx_nb_to_be_read_s <= ( others => '0');
                        tx_nb_to_be_read_s(11 downto 0) <= tx_other_data_s;
                        fsm <= txdata_ready;
                    end if;
                end if;
                timer := timer + 1;
           
           when txdata_dfifo_read =>                
                
                if timer /= 0 then
                    tx_data_o <= txdata_dfifo_dout;
                    tx_wr_o <= '1';  
                    
                    if timer =  to_integer(unsigned(tx_nb_to_be_sent_s)) then
                     fsm <= txdata_ready; 
                    end if;
                end if;
                timer := timer + 1;                
                if timer /= to_integer(unsigned(tx_nb_to_be_sent_s)) then
                    txdata_dfifo_rd_en <= '1';
                end if;
 
           when txdata_ready =>
                if tx_rw = '0' then
                    tx_start_write_o <= '1';
                    rx_nread := 8 + to_integer(unsigned(tx_nb_to_be_sent_s));
                else
                    tx_start_read_o <= '1';
                    rx_nread := 8 + to_integer(unsigned(tx_nb_to_be_read_s));
                end if;
                rx_iread := 0;
                fsm <= rxdata_read;
            when rxdata_read =>
                if rx_empty_i = '0'and rx_rd_p(1 downto 0) = "00" then
                    rx_rd_p(0) <= '1';
                    if rx_iread = CHIPADDR then
                        rxdata_fifo_din(63 downto 56) <= rx_data_i;
                    elsif rx_iread = NREAD_L then -- nread
                        rxdata_fifo_din(55 downto 48) <= rx_data_i;
                        rx_data_nread_s(15 downto 8) <= rx_data_i;
                    elsif rx_iread = NREAD_H then -- nread
                        rxdata_fifo_din(47 downto 40) <= rx_data_i;
                        rx_data_nread_s( 7 downto 0) <= rx_data_i;
                    elsif rx_iread = MEMADDR_L then -- addr
                        rxdata_fifo_din(39 downto 32) <= rx_data_i;
                        if rx_nread /= to_integer(unsigned(rx_data_nread_s)) then
                            err_s <= '1';
                        end if;
                    elsif rx_iread = MEMADDR_H then -- addr
                        rxdata_fifo_din(31 downto 24) <= rx_data_i;                
                    elsif rx_iread > MEMADDR_H and rx_iread < rx_nread - 1 then
                        rxdata_ready_s <= '1';
                        rxdata_fifo_din(23 downto 8) <= std_logic_vector(to_unsigned(rx_iread - 7, 16));
                        rxdata_fifo_din(7 downto 0) <= rx_data_i;
                        reply_data_count <= reply_data_count + 1;
                    end if;
                    rx_iread := rx_iread + 1;
                    if rx_iread = rx_nread then
                        fsm <= idle;                   
                    end if;
                end if;

            end case;
        end if;
    end process;    

    sel <= to_integer(unsigned(ipb_in.ipb_addr(ADDR_WIDTH-1 downto 0)));   
    ipb_out.ipb_ack <= ack_s;
	ipb_out.ipb_err <= '0';
    ipb_ctrl_p : process ( ipb_clk, ipb_rst )
    begin   
        if ipb_rst = '1' then
            reset_ipb <= '1';
            ack_s <= '0';
            strobe_d <= '0';
            rxdata_fifo_rd_en <= '0';
        elsif rising_edge(ipb_clk) then
		   
		   reset_ipb             <= '0';   
		   txdata_fifo_srst_ipb  <= '0';
		   txdata_dfifo_srst_ipb <= '0';
		   rxdata_fifo_srst_ipb  <= '0';
           txdata_fifo_wr_en     <= '0';
           txdata_dfifo_wr_en    <= '0';
           strobe_d <= ipb_in.ipb_strobe;
           rxdata_fifo_data_rdy <=  rxdata_fifo_rd_en;
           
           if sel = RXDATA_FIFO_SEL then
              ack_s <= '0';
              if rxdata_fifo_data_rdy = '1' then
                    ipb_out.ipb_rdata <= rxdata_fifo_dout;
                    ack_s <= '1';
               end if; 
           else
             ack_s <= ipb_in.ipb_strobe and not ack_s;	
           end if;
         
           if ipb_in.ipb_write = '1' then
               
               if ack_s = '1' then   
    
                    if sel = RESET_IC_CTRL_SEL then                               
                        reset_ipb             <= ipb_in.ipb_wdata(RESET_IC_CTRL_GLOBAL_BIT);
                        txdata_fifo_srst_ipb  <= ipb_in.ipb_wdata(RESET_IC_CTRL_TXDATA_FIFO_BIT);
                        txdata_dfifo_srst_ipb <= ipb_in.ipb_wdata(RESET_IC_CTRL_TXDATA_DFIFO_BIT);
                        rxdata_fifo_srst_ipb  <= ipb_in.ipb_wdata(RESET_IC_CTRL_RXDATA_FIFO_BIT);
                    elsif sel = TXDATA_FIFO_SEL then
                        txdata_fifo_wr_en <= '1';
                        txdata_fifo_din   <= ipb_in.ipb_wdata;
                    elsif sel = TXDATA_DFIFO_SEL then
                        txdata_dfifo_wr_en <= '1';
                        txdata_dfifo_din   <= ipb_in.ipb_wdata(7 downto 0);                    
                    end if;
                end if;
            else 

                if sel = STATUS_SEL then
                    ipb_out.ipb_rdata <= (others => '0');
                    ipb_out.ipb_rdata(STATUS_TX_READY_BIT) <= tx_ready_i;
                    ipb_out.ipb_rdata(STATUS_RX_EMPTY_BIT) <= rx_empty_i;
                    ipb_out.ipb_rdata(STATUS_RXDATA_FIFO_EMPTY_BIT) <= rxdata_fifo_empty;
                    ipb_out.ipb_rdata(2 + STATUS_FSM_BIT_OFFSET downto STATUS_FSM_BIT_OFFSET) <= fsm_ipb;
                elsif sel = REPLY_DATA_COUNT_SEL then
                    ipb_out.ipb_rdata <= std_logic_vector(reply_data_count);
                elsif sel = RXDATA_FIFO_SEL then            		
                    rxdata_fifo_rd_en <= ipb_in.ipb_strobe and not strobe_d;                        
  
                end if; 
            end if;
        end if;
    end process;

    fsm_ipb
    <= "000" when fsm = idle else
       "001" when fsm = txdata_fifo_proc else 
       "010" when fsm = txdata_dfifo_read else
       "011" when fsm = txdata_ready else
       "100" when fsm = rxdata_read;
                    
    txdata_fifo : entity work.ic_txdata_fifo
      PORT MAP (
        clk => ipb_clk,
        srst => txdata_fifo_srst,
        din => txdata_fifo_din,
        wr_en => txdata_fifo_wr_en,
        rd_en => txdata_fifo_rd_en,
        dout => txdata_fifo_dout,
        full => txdata_fifo_full,
        empty => txdata_fifo_empty,
        wr_rst_busy => txdata_fifo_wr_rst_busy,
        rd_rst_busy => txdata_fifo_rd_rst_busy
      );

    txdata_dfifo : entity work.ic_txdata_dfifo
      PORT MAP (
        clk => ipb_clk,
        srst => txdata_dfifo_srst,
        din => txdata_dfifo_din,
        wr_en => txdata_dfifo_wr_en,
        rd_en => txdata_dfifo_rd_en,
        dout => txdata_dfifo_dout,
        full => txdata_dfifo_full,
        empty => txdata_dfifo_empty,
        wr_rst_busy => txdata_dfifo_wr_rst_busy,
        rd_rst_busy => txdata_dfifo_rd_rst_busy
      );    
    rxdata_fifo : entity work.ic_rxdata_fifo
      PORT MAP (
        clk => ipb_clk,
        srst => rxdata_fifo_srst,
        din => rxdata_fifo_din,
        wr_en => rxdata_fifo_wr_en,
        rd_en => rxdata_fifo_rd_en,
        dout => rxdata_fifo_dout,
        full => rxdata_fifo_full,
        empty => rxdata_fifo_empty,
        wr_rst_busy => rxdata_fifo_wr_rst_busy,
        rd_rst_busy => rxdata_fifo_rd_rst_busy
      );
    
end Behavioral;
