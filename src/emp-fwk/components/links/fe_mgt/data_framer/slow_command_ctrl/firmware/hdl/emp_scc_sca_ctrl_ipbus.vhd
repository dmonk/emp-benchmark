----------------------------------------------------------------------------------
-- SCA control ipbus module
-- Kirika Uchida 09.14.2020
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.sca_pkg.all;

entity emp_scc_sca_ctrl_ipbus is
generic(ADDR_WIDTH : natural := 2;
        COMMAND_PULSE_LENGTH : natural := 8 );
port(
        ipb_clk: in std_logic;
        ipb_rst: in std_logic;
        ipb_in: in ipb_wbus;
        ipb_out: out ipb_rbus;
        
        Tx_clk_i      : in std_logic; 
        Tx_clk_en_i   : in std_logic := '1';
        
        Rx_clk_i      : in std_logic := '0';
        Rx_clk_en_i   : in std_logic := '0';
        -- SCA commands
        start_input_cmd_o                 : out std_logic;
        start_reset_cmd_o                 : out std_logic;
        start_connect_cmd_o               : out std_logic;
        start_test_cmd_o                  : out std_logic;
        -- SCA module signals
        rx_ongoing_i                      : in STD_LOGIC;
        rx_delimiter_found_i              : in STD_LOGIC;
        -- to make signal rx_des_started which is a pulse made when sca deserializer find the delimiter on data.
        rx_data_received_i                : in std_logic;
        done_i                            : in std_logic;
        rx_bram_we_o                      : out std_logic;
        -- bram address at the controller side
        EC_Frame_addr_o                   : out std_logic_vector(7 downto 0);
        -- txbram address counter at ipbus side
        tx_bram_frame_count_i             : in std_logic_vector(8 downto 0);
        rx_bram_frame_count_i             : in std_logic_vector(8 downto 0)
	);
end emp_scc_sca_ctrl_ipbus;

architecture Behavioral of emp_scc_sca_ctrl_ipbus is
 
    signal reg                             : ipb_reg_v(3 downto 0);
    signal sel                             : integer range 0 to 3;
    signal ack                             : std_logic;
    signal ack_s                           : std_logic;
    
    constant CTRL_SIG_SEL                  : integer := 0;
    constant N_RX_FRAME_READY_SEL          : integer := 1;

    constant CTRL_SIG_START_INPUT_BIT               : integer := 0;    
    constant CTRL_SIG_START_RESET_BIT               : integer := 1;
    constant CTRL_SIG_START_CONNECT_BIT             : integer := 2;
    constant CTRL_SIG_START_TEST_BIT                : integer := 3;
    
    signal ipb_ctrl_p0                     : std_logic_vector(31 downto 0);
    signal ipb_ctrl_p1                     : std_logic_vector(31 downto 0);
    signal ipb_ctrl_pp                     : std_logic_vector(31 downto 0);
    signal ipb_ctrl_d                      : std_logic_vector(31 downto 0);
    signal ipb_ctrl_s                      : std_logic_vector(31 downto 0);
	 
    signal ipb_start_input_cmd_s           : std_logic;
    signal ipb_start_reset_cmd_s           : std_logic;
    signal ipb_start_connect_cmd_s         : std_logic;
    signal ipb_start_test_cmd_s            : std_logic;

    signal next_start_input_cmd_s          : std_logic;
	signal EC_Frame_addr_s                 : std_logic_vector(7 downto 0) := (others => '0');
	signal EC_Frame_cnt_s                  : integer range 0 to 1023 := 0;
	signal current_ec_frame_count          : integer range 0 to 1024 := 0;
	signal send_next_start_input_cmd_s     : std_logic := '0';
    signal n_rx_frame_ready_s              : integer range 0 to 1024 := 0;
    signal all_rx_frame_ready_s            : std_logic;
	   
--    signal rx_data_arrived_d_s            : std_logic := '0';
--	signal rx_data_arrived_2_s            : STD_LOGIC := '0';
--	signal tx_clk_rx_data_arrived_2_s     : STD_LOGIC := '0';
--	signal rx_data_arrived_s              : std_logic := '0';
--	signal SCA_frame_TxRx_s        : STD_LOGIC := '0';

    signal start_input_cmd_s                     : std_logic;
    signal done_p                                : std_logic;

begin

    com_sync_proc: process(Tx_clk_i)
    begin
        if rising_edge(Tx_clk_i) then
            
            if Tx_clk_en_i = '1' then    
        
                start_input_cmd_o   <= start_input_cmd_s;
                start_reset_cmd_o   <= ipb_start_reset_cmd_s;
                start_connect_cmd_o <= ipb_start_connect_cmd_s;
                start_test_cmd_o    <= ipb_start_test_cmd_s;
        
            end if;
        end if;
    end process;

    ipb_out.ipb_ack <= ack_s;
    sel <= to_integer(unsigned(ipb_in.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;

    process(ipb_rst, ipb_clk)
    variable wsel : integer range 0 to 3;
    variable wcom : boolean := false;
    begin
        if ipb_rst = '1' then
        
            reg <= (others => (others => '0'));
            ipb_ctrl_p0 <= (others => '0');
            ipb_ctrl_p1 <= (others => '0');
            ack_s <= '0';
        elsif rising_edge(ipb_clk) then

            ack_s <= ipb_in.ipb_strobe and not ack_s;	

            ipb_ctrl_p0 <= (others => '0');
            ipb_ctrl_p1 <= ipb_ctrl_p0;    
            if ipb_in.ipb_strobe='1' then
                
                if sel = CTRL_SIG_SEL then                         
                    if ipb_in.ipb_write='1' then 
                            ipb_ctrl_p0 <= ipb_in.ipb_wdata;
                    end if;
                elsif sel = N_RX_FRAME_READY_SEL then
                    if ipb_in.ipb_write='0' then
                        ipb_out.ipb_rdata <= (others => '0');
                        ipb_out.ipb_rdata(0) <= all_rx_frame_ready_s;
                    end if;
                end if;
                
            end if;            
               
        end if;    
    end process;
    ipb_ctrl_pp <= ipb_ctrl_p0 or ipb_ctrl_p1;            
    ipb_ctrl_proc: process(Tx_clk_i)
    begin
    
        if rising_edge(Tx_clk_i) then
            

            if Tx_clk_en_i = '1' then
            
                ipb_ctrl_s <= (others => '0');
                ipb_ctrl_d <= ipb_ctrl_pp;
            
                for i in 0 to 31 loop
                    if ipb_ctrl_d(i) = '0' and ipb_ctrl_pp(i) = '1' then
                        ipb_ctrl_s(i) <= '1';
                    end if;
                end loop;
            
            end if;
        end if;
    end process;
    
    ipb_start_input_cmd_s <= ipb_ctrl_s(CTRL_SIG_START_INPUT_BIT);
    ipb_start_reset_cmd_s <= ipb_ctrl_s(CTRL_SIG_START_RESET_BIT);
    ipb_start_connect_cmd_s <= ipb_ctrl_s(CTRL_SIG_START_CONNECT_BIT);   
    ipb_start_test_cmd_s <= ipb_ctrl_s(CTRL_SIG_START_TEST_BIT);

    start_input_cmd_s <= ipb_start_input_cmd_s or next_start_input_cmd_s;
 
    rx_bram_we_o <= rx_data_received_i and Rx_clk_en_i; -- data from gbtsc need to be read within 8 x 40 MHz clock from rx_received_o from gbtsc_top
    EC_Frame_addr_o <= EC_Frame_addr_s;
    EC_Frame_addr_s <= std_logic_vector(to_unsigned(EC_Frame_cnt_s, 8));
 
    done_p_proc: process(Rx_clk_i)
    begin
        if rising_edge(Rx_clk_i) then
            
            if Rx_clk_en_i = '1' then
                done_p <= done_i;
            end if;
        end if;
    end process;

    next_frame_proc: process(Tx_clk_i)
    begin
        if rising_edge(Tx_clk_i) then
            
            if Tx_clk_en_i = '1' then
                
--                tx_clk_rx_data_arrived_2_s <= rx_data_arrived_2_s ;
--                rx_data_arrived_s  <= rx_data_arrived_2_s and not tx_clk_rx_data_arrived_2_s;
                
                if ipb_start_input_cmd_s = '1' then
                    EC_Frame_cnt_s <= 0;
                    current_ec_frame_count <= 1;
                    n_rx_frame_ready_s <= 0;
                    all_rx_frame_ready_s <= '0';
                end if;
                if next_start_input_cmd_s = '1' then
                    current_ec_frame_count <= current_ec_frame_count + 1;
                end if;
   
                send_next_start_input_cmd_s <= '0';             
                if current_ec_frame_count < to_integer(unsigned(tx_bram_frame_count_i)) then                
                    send_next_start_input_cmd_s <= '1';
                end if;
                
                next_start_input_cmd_s <= '0';
                if done_p = '0' and done_i = '1' then
                    n_rx_frame_ready_s <= n_rx_frame_ready_s + 1;
                    if send_next_start_input_cmd_s = '1' then                   
                        EC_Frame_cnt_s <= EC_Frame_cnt_s + 1;
                        next_start_input_cmd_s <= '1';
                    else    
                        EC_Frame_cnt_s <= 0;
                        all_rx_frame_ready_s <= '1';
                    end if;
                end if;                    
            end if;
            
--            if start_input_cmd_s = '1'   then SCA_frame_TxRx_s <= '1'; end if;
--            if rx_data_arrived_s = '1'       then SCA_frame_TxRx_s <= '0'; end if;
 
        end if;
    end process;


end Behavioral;
