-------------------------------------------------------
-- The control top module 
-- Kirika Uchida 09.14.2020
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

LIBRARY work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_emp_slow_command_ctrl.all;

library UNISIM;
use UNISIM.VComponents.all;

--! Use SCA Package to define specific types (vector arrays)
use work.SCA_PKG.all;
--use IEEE.std_logic_unsigned.ALL;



entity emp_slow_command_ctrl_ipbus is
    generic(
        LpGBT                     : integer := 0;
        IC_SIMPLE                 : boolean := false;
        -- EC configuration
        g_NO_EC                   : boolean := false;
        g_EC_BROADCAST            : boolean := false;
        g_SPARE_SCA_COUNT         : integer := 0                                      --! Defines the maximum number of SCA SPARE that can be connected to this module
	);
	port(
		ipb_clk: in std_logic;
		ipb_rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		
	    Tx_clk_i      : in std_logic; 
	    Tx_clk_en_i   : in std_logic := '0';
		Rx_clk_i      : in std_logic := '0';
		Rx_clk_en_i   : in std_logic := '0';

        reset_to_gbtsc_o                    : out std_logic;      
        ic_tx_GBTx_address_to_gbtsc_o       : out std_logic_vector(7 downto 0);
        ic_tx_register_addr_to_gbtsc_o      : out std_logic_vector(15 downto 0);
        ic_tx_nb_to_be_read_to_gbtsc_o      : out std_logic_vector(15 downto 0);		
	    ic_tx_ready_from_gbtsc_i            : in  std_logic;
        ic_rx_empty_from_gbtsc_i            : in  std_logic;
        ic_tx_wr_to_gbtsc_o                 : out std_logic; 
        ic_tx_data_to_gbtsc_o               : out std_logic_vector(7 downto 0);
        ic_rx_rd_to_gbtsc_o                 : out std_logic;               
		ic_rx_data_from_gbtsc_i             : in  std_logic_vector(7 downto 0);
        ic_tx_start_read_to_gbtsc_o         : out std_logic;
		ic_tx_start_write_to_gbtsc_o        : out std_logic; 
		ic_reset_to_gbtsc_o                 : out std_logic;      
        sca_start_input_cmd_to_gbtsc_o      : out std_logic_vector(g_SPARE_SCA_COUNT downto 0);
        sca_start_reset_cmd_to_gbtsc_o      : out std_logic_vector(g_SPARE_SCA_COUNT downto 0);
        sca_start_connect_cmd_to_gbtsc_o    : out std_logic_vector(g_SPARE_SCA_COUNT downto 0);
        sca_start_test_cmd_to_gbtsc_o       : out std_logic_vector(g_SPARE_SCA_COUNT downto 0); 
        sca_tx_address_to_gbtsc_o           : out reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: address field (According to the SCA manual)
        sca_tx_transID_to_gbtsc_o           : out reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: transaction ID field (According to the SCA manual)
        sca_tx_channel_to_gbtsc_o           : out reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: channel field (According to the SCA manual)
        sca_tx_len_to_gbtsc_o               : out reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: leght field (According to the SCA manual)
        sca_tx_command_to_gbtsc_o           : out reg8_arr(g_SPARE_SCA_COUNT downto 0);   --! Command: command field (According to the SCA manual)
        sca_tx_data_to_gbtsc_o              : out reg32_arr(g_SPARE_SCA_COUNT downto 0);  --! Command: data field (According to the SCA manual)
        sca_rx_received_from_gbtsc_i        : in std_logic_vector(g_SPARE_SCA_COUNT downto 0);   --! Reply received flag (pulse)
        sca_rx_address_from_gbtsc_i         : in reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: address field (According to the SCA manual)
        sca_rx_control_from_gbtsc_i         : in reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: control field (According to the SCA manual)
        sca_rx_transID_from_gbtsc_i         : in reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: transaction ID field (According to the SCA manual)
        sca_rx_channel_from_gbtsc_i         : in reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: channel field (According to the SCA manual)
        sca_rx_len_from_gbtsc_i             : in reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: len field (According to the SCA manual)
        sca_rx_error_from_gbtsc_i           : in reg8_arr(g_SPARE_SCA_COUNT downto 0);           --! Reply: error field (According to the SCA manual)
        sca_rx_data_from_gbtsc_i            : in reg32_arr(g_SPARE_SCA_COUNT downto 0);          --! Reply: data field (According to the SCA manual)
        sca_rx_ongoing_from_gbtsc_i         : in std_logic_vector(g_SPARE_SCA_COUNT downto 0);
        sca_rx_delimiter_found_from_gbtsc_i : in std_logic_vector(g_SPARE_SCA_COUNT downto 0);
        test_o  : out std_logic_vector(7 downto 0)
	);

end emp_slow_command_ctrl_ipbus;

architecture rtl of  emp_slow_command_ctrl_ipbus is


 
       signal ipb_rdata: std_logic_vector(31 downto 0) ;
       signal ipb_wdata: std_logic_vector(31 downto 0);
       signal addr : STD_LOGIC_VECTOR(11 DOWNTO 0);
       signal ipb_write: STD_LOGIC;
    
       signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
	   signal ipbr: ipb_rbus_array(N_SLAVES - 1 downto 0);
       constant N_CTRL : integer := 2;
	   signal ctrl: ipb_reg_v(N_CTRL-1 downto 0);
       signal stat: ipb_reg_v(0 downto 0);
       signal stb_ctrl   : std_logic_vector(N_CTRL-1 downto 0);
       signal stb_ctrl_d : std_logic_vector(N_CTRL-1 downto 0);
       signal wr_ctrl    : std_logic_vector(N_CTRL-1 downto 0);  
            
        -- initialize IC and EC moduls
       signal reset_to_gbtsc_s                : STD_LOGIC := '0';
  
      --------------------------------- EC signals ----------------------------------- 

       signal ec_tx_address_to_gbtsc_s                  : std_logic_vector(7 downto 0);   --! Command: address field (According to the SCA manual)
       signal ec_tx_transID_to_gbtsc_s                  : std_logic_vector(7 downto 0);   --! Command: transaction ID field (According to the SCA manual)
       signal ec_tx_channel_to_gbtsc_s                  : std_logic_vector(7 downto 0);   --! Command: channel field (According to the SCA manual)
       signal ec_tx_len_to_gbtsc_s                      : std_logic_vector(7 downto 0);   --! Command: leght field (According to the SCA manual)
       signal ec_tx_command_to_gbtsc_s                  : std_logic_vector(7 downto 0);   --! Command: command field (According to the SCA manual)
       signal ec_tx_data_to_gbtsc_s                     : std_logic_vector(31 downto 0);  --! Command: data field (According to the SCA manual)
       signal ec_start_input_cmd_to_gbtsc_s             : std_logic;
       signal ec_start_reset_cmd_to_gbtsc_s             : std_logic;
       signal ec_start_connect_cmd_to_gbtsc_s           : std_logic;
       signal ec_start_test_cmd_to_gbtsc_s              : std_logic;
       
       signal sca_done_s                                : std_logic_vector(g_SPARE_SCA_COUNT downto 0);
               
       -- fifo
 
--        signal txdata_fifo_srst        : std_logic := '0';
--        signal txdata_fifo_din         : std_logic_vector(31 downto 0);
--        signal txdata_fifo_wr_en       : std_logic;
--        signal txdata_fifo_rd_en       : std_logic;
--        signal txdata_fifo_dout        : std_logic_vector(31 downto 0);
--        signal txdata_fifo_full        : std_logic;
--        signal txdata_fifo_empty       : std_logic;
--        signal txdata_fifo_wr_rst_busy : std_logic;
--        signal txdata_fifo_rd_rst_busy : std_logic;
        
--        signal txdata_dfifo_srst        : std_logic := '0';
--        signal txdata_dfifo_din         : std_logic_vector(7 downto 0);
--        signal txdata_dfifo_wr_en       : std_logic;
--        signal txdata_dfifo_rd_en       : std_logic;
--        signal txdata_dfifo_dout        : std_logic_vector(7 downto 0);
--        signal txdata_dfifo_full        : std_logic;
--        signal txdata_dfifo_empty       : std_logic;
--        signal txdata_dfifo_wr_rst_busy : std_logic;
--        signal txdata_dfifo_rd_rst_busy : std_logic;
        
----------------------------- test output ------------------------------      

 
  
  	   signal soft_rst                    : std_logic;
  	   signal nuke                        : std_logic;
       signal ec_spare_sel                : std_logic_vector(3 downto 0);
       signal active_ec_s                 : std_logic_vector(16 downto 0);
begin
   
   test_o(0) <= stb_ctrl(0);
   test_o(1) <= stb_ctrl_d(0);
   test_o(2) <= wr_ctrl(0);
   test_o(3) <= reset_to_gbtsc_s;
   reset_to_gbtsc_o <= reset_to_gbtsc_s;
 ---------------------------------------------------------------------------------------
----------------------------- ipbus address decode -------------------------------------
		
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_emp_slow_command_ctrl(ipb_in.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );
 
---------------------------------------------------------------------------------------

	slave0: entity work.ipbus_ctrlreg_v
    generic map(
		N_CTRL => N_CTRL,
		N_STAT => 1
		)
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_CSR),
			ipbus_out => ipbr(N_SLV_CSR),
			d => stat,
			q => ctrl,
			stb => stb_ctrl
		);
		
		stat(0) <= X"abcdfe19";
		soft_rst <= ctrl(0)(0);
		nuke <= ctrl(0)(1);
		stb_p : process (ipb_clk)
		begin
		  if rising_edge(ipb_clk) then
		      stb_ctrl_d <= stb_ctrl;

		      for i in 0 to N_CTRL - 1 loop
 		         wr_ctrl(i) <= '0';
                  if stb_ctrl(i) = '1' and stb_ctrl_d(i) = '0' then
                      wr_ctrl(i) <= '1';
                  end if;
              end loop;
		  end if;
		end process;
		reset_to_gbtsc_s <= ctrl(0)(2) and wr_ctrl(0);
		
    active_ec_s  <= ctrl(1)(16 downto 0); 
    ec_spare_sel <= ctrl(1)(23 downto 20);
 
    ic_ctrl_ipbus_ka_gen : if IC_SIMPLE = true generate
 
        ic_ctrl_ipbus_ka_inst : entity work.emp_scc_ic_ctrl_ipbus_simple
            port map( 
                ipb_clk => ipb_clk,
                ipb_rst => ipb_rst,
                ipb_in => ipbw(N_SLV_IC),
                ipb_out => ipbr(N_SLV_IC),
            
                Tx_clk_i  => Tx_clk_i,
                Tx_clk_en_i => Tx_clk_en_i,
                
                Rx_clk_i     => Rx_clk_i,
                Rx_clk_en_i  => Rx_clk_en_i,    
    
                tx_GBTx_address_o  => ic_tx_GBTx_address_to_gbtsc_o,
                tx_register_addr_o => ic_tx_register_addr_to_gbtsc_o,
                tx_nb_to_be_read_o => ic_tx_nb_to_be_read_to_gbtsc_o,	
                tx_ready_i         => ic_tx_ready_from_gbtsc_i,
                rx_empty_i         => ic_rx_empty_from_gbtsc_i,
                tx_wr_o            => ic_tx_wr_to_gbtsc_o,
                tx_data_o          => ic_tx_data_to_gbtsc_o,
                rx_rd_o            => ic_rx_rd_to_gbtsc_o,              
                rx_data_i          => ic_rx_data_from_gbtsc_i,
                tx_start_read_o    => ic_tx_start_read_to_gbtsc_o,
                tx_start_write_o   => ic_tx_start_write_to_gbtsc_o
                );
    end generate;
    
    ic_ctrl_ipbus_ku_gen : if IC_SIMPLE = false generate
        ic_ctrl_ipbus_ku_inst : entity work.emp_scc_ic_ctrl_ipbus_auto
            generic map( LpGBT => LpGBT )
            port map( 
                ipb_clk            => ipb_clk,
                ipb_rst            => ipb_rst,
                ipb_in             => ipbw(N_SLV_IC),
                ipb_out            => ipbr(N_SLV_IC),
                tx_GBTx_address_o  => ic_tx_GBTx_address_to_gbtsc_o,
                tx_register_addr_o => ic_tx_register_addr_to_gbtsc_o,
                tx_nb_to_be_read_o => ic_tx_nb_to_be_read_to_gbtsc_o,	
                tx_ready_i         => ic_tx_ready_from_gbtsc_i,
                rx_empty_i         => ic_rx_empty_from_gbtsc_i,
                tx_wr_o            => ic_tx_wr_to_gbtsc_o,
                tx_data_o          => ic_tx_data_to_gbtsc_o,
                rx_rd_o            => ic_rx_rd_to_gbtsc_o,              
                rx_data_i          => ic_rx_data_from_gbtsc_i,
                tx_start_read_o    => ic_tx_start_read_to_gbtsc_o,
                tx_start_write_o   => ic_tx_start_write_to_gbtsc_o,
                ic_reset_o         => ic_reset_to_gbtsc_o
            );
    end generate;
    
    sca_tx_address_to_gbtsc_o(0)         <= ec_tx_address_to_gbtsc_s;
    sca_tx_transID_to_gbtsc_o(0)         <= ec_tx_transID_to_gbtsc_s;
    sca_tx_channel_to_gbtsc_o(0)         <= ec_tx_channel_to_gbtsc_s;
    sca_tx_len_to_gbtsc_o(0)             <= ec_tx_len_to_gbtsc_s;
    sca_tx_command_to_gbtsc_o(0)         <= ec_tx_command_to_gbtsc_s;
    sca_tx_data_to_gbtsc_o(0)            <= ec_tx_data_to_gbtsc_s;    
    sca_start_input_cmd_to_gbtsc_o(0)    <= ec_start_input_cmd_to_gbtsc_s;
    sca_start_reset_cmd_to_gbtsc_o(0)    <= ec_start_reset_cmd_to_gbtsc_s;
    sca_start_connect_cmd_to_gbtsc_o(0)  <= ec_start_connect_cmd_to_gbtsc_s;
    sca_start_test_cmd_to_gbtsc_o(0)     <= ec_start_test_cmd_to_gbtsc_s; 
    

    sca_done_s <= sca_rx_received_from_gbtsc_i;
        
    tx_signal_and_rx_received_bc_gen : if g_EC_BROADCAST = true generate
               
        signal rx_received_latched_s         : std_logic_vector(g_SPARE_SCA_COUNT downto 0); 
        signal all_rx_received_latched_p0    : std_logic_vector(1 downto 0);
        signal all_rx_received_latched_p1    : std_logic_vector(1 downto 0);

    begin    
  
        ec_spare_tx_signal_gen : for i in 1 to g_SPARE_SCA_COUNT generate
   
            sca_tx_address_to_gbtsc_o(i)         <= ec_tx_address_to_gbtsc_s;
            sca_tx_transID_to_gbtsc_o(i)         <= ec_tx_transID_to_gbtsc_s;
            sca_tx_channel_to_gbtsc_o(i)         <= ec_tx_channel_to_gbtsc_s;
            sca_tx_len_to_gbtsc_o(i)             <= ec_tx_len_to_gbtsc_s;
            sca_tx_command_to_gbtsc_o(i)         <= ec_tx_command_to_gbtsc_s;
            sca_tx_data_to_gbtsc_o(i)            <= ec_tx_data_to_gbtsc_s;    
            sca_start_input_cmd_to_gbtsc_o(i)    <= ec_start_input_cmd_to_gbtsc_s;
            sca_start_reset_cmd_to_gbtsc_o(i)    <= ec_start_reset_cmd_to_gbtsc_s;
            sca_start_connect_cmd_to_gbtsc_o(i)  <= ec_start_connect_cmd_to_gbtsc_s;
            sca_start_test_cmd_to_gbtsc_o(i)     <= ec_start_test_cmd_to_gbtsc_s; 
            
        end generate;
      
        EC_rx_data_received_proc: process(Rx_clk_i,Rx_clk_en_i)            
          begin
            if rising_edge(Rx_clk_i) then
 
                if Rx_clk_en_i = '1' then

                    sca_done_s(0) <= '0';

                    for i in 0 to g_SPARE_SCA_COUNT loop
                        if sca_rx_received_from_gbtsc_i(i) = '1' then
                            rx_received_latched_s(i) <= '1';
                        end if;
                    end loop;
                    
                    if ec_start_input_cmd_to_gbtsc_s = '1' then
                        all_rx_received_latched_p0 <= (others => '0');
                        rx_received_latched_s <= (others => '0');
                    elsif rx_received_latched_s = active_ec_s(g_SPARE_SCA_COUNT downto 0) then
                        all_rx_received_latched_p0(0) <= '1';
                    end if;
                    all_rx_received_latched_p0(1) <= all_rx_received_latched_p0(0);                  
                    sca_done_s(0) <= not all_rx_received_latched_p0(1) and all_rx_received_latched_p0(0);
                   
                end if;
            end if;
        end process;           
    end generate;  
     
     ec_ctrl_ipbus_gen : if g_NO_EC = false or g_EC_BROADCAST = true generate
         ec_ctrl_ipbus_inst : entity work.emp_scc_sca_ipbus
                 generic map( g_NO_TX_BRAM  =>  false, g_NO_RX_BRAM => g_NO_EC )
         port map(
            ipb_clk => ipb_clk,
            ipb_rst => ipb_rst,
            ipb_in  => ipbw(N_SLV_EC),
            ipb_out => ipbr(N_SLV_EC),
            
            Tx_clk_i  => Tx_clk_i,
            Tx_clk_en_i => Tx_clk_en_i,
            
            Rx_clk_i     => Rx_clk_i,
            Rx_clk_en_i  => Rx_clk_en_i,    

           -- SCA commands
            start_input_cmd_o            => ec_start_input_cmd_to_gbtsc_s,
            start_reset_cmd_o            => ec_start_reset_cmd_to_gbtsc_s,
            start_connect_cmd_o          => ec_start_connect_cmd_to_gbtsc_s,
            start_test_cmd_o             => ec_start_test_cmd_to_gbtsc_s,            
            -- SCA frame fields
            tx_address_o              => ec_tx_address_to_gbtsc_s,
            tx_transID_o              => ec_tx_transID_to_gbtsc_s,
            tx_channel_o              => ec_tx_channel_to_gbtsc_s,
            tx_len_o                  => ec_tx_len_to_gbtsc_s,
            tx_command_o              => ec_tx_command_to_gbtsc_s,
            tx_data_o                 => ec_tx_data_to_gbtsc_s,
            rx_received_i             => sca_rx_received_from_gbtsc_i(0),
            rx_address_i              => sca_rx_address_from_gbtsc_i(0),
            rx_control_i              => sca_rx_control_from_gbtsc_i(0),
            rx_transID_i              => sca_rx_transID_from_gbtsc_i(0),
            rx_channel_i              => sca_rx_channel_from_gbtsc_i(0),
            rx_len_i                  => sca_rx_len_from_gbtsc_i(0),
            rx_error_i                => sca_rx_error_from_gbtsc_i(0),
            rx_data_i                 => sca_rx_data_from_gbtsc_i(0),
            -- sca_rx module signals 
            rx_ongoing_i              => sca_rx_ongoing_from_gbtsc_i(0),
            rx_delimiter_found_i      => sca_rx_delimiter_found_from_gbtsc_i(0),
            done_i                    => sca_done_s(0)
         );
     end generate;
       
     ec_spare_ipbus_gen : if g_SPARE_SCA_COUNT /= 0 generate
         signal ipbw_ec_spare : ipb_wbus_array(g_SPARE_SCA_COUNT-1 downto 0);
         signal ipbr_ec_spare : ipb_rbus_array(g_SPARE_SCA_COUNT-1 downto 0);
     begin
         ec_spare_ipbus_sel : entity work.ipbus_fabric_sel
          generic map(
            NSLV      => g_SPARE_SCA_COUNT,
            SEL_WIDTH => 4
            )
          port map(
            sel             => ec_spare_sel,
            ipb_in          => ipbw(N_SLV_EC_SPARE),
            ipb_out         => ipbr(N_SLV_EC_SPARE),
            ipb_to_slaves   => ipbw_ec_spare,
            ipb_from_slaves => ipbr_ec_spare
            );
         
         ec_spare_ipbus_gen : for i in 0 to g_SPARE_SCA_COUNT - 1 generate
 
           signal tx_address_s                  : std_logic_vector(7 downto 0);   --! Command: address field (According to the SCA manual)
           signal tx_transID_s                  : std_logic_vector(7 downto 0);   --! Command: transaction ID field (According to the SCA manual)
           signal tx_channel_s                  : std_logic_vector(7 downto 0);   --! Command: channel field (According to the SCA manual)
           signal tx_len_s                      : std_logic_vector(7 downto 0);   --! Command: leght field (According to the SCA manual)
           signal tx_command_s                  : std_logic_vector(7 downto 0);   --! Command: command field (According to the SCA manual)
           signal tx_data_s                     : std_logic_vector(31 downto 0);  --! Command: data field (According to the SCA manual)
           signal rx_received_s                 : std_logic;   --! Reply received flag (pulse)
           signal rx_address_s                  : std_logic_vector(7 downto 0);           --! Reply: address field (According to the SCA manual)
           signal rx_control_s                  : std_logic_vector(7 downto 0);           --! Reply: control field (According to the SCA manual)
           signal rx_transID_s                  : std_logic_vector(7 downto 0);           --! Reply: transaction ID field (According to the SCA manual)
           signal rx_channel_s                  : std_logic_vector(7 downto 0);           --! Reply: channel field (According to the SCA manual)
           signal rx_len_s                      : std_logic_vector(7 downto 0);           --! Reply: len field (According to the SCA manual)
           signal rx_error_s                    : std_logic_vector(7 downto 0);           --! Reply: error field (According to the SCA manual)
           signal rx_data_s                     : std_logic_vector(31 downto 0);          --! Reply: data field (According to the SCA manual)
           signal start_input_cmd_s             : std_logic;
           signal start_reset_cmd_s             : std_logic;
           signal start_connect_cmd_s           : std_logic;
           signal start_test_cmd_s              : std_logic;
           signal ongoing_s                     : std_logic;
           signal delimiter_found_s             : std_logic;

         begin
         
            tx_signal_gen : if g_EC_BROADCAST = false generate
            
                sca_tx_address_to_gbtsc_o(i+1)         <= tx_address_s;
                sca_tx_transID_to_gbtsc_o(i+1)         <= tx_transID_s;
                sca_tx_channel_to_gbtsc_o(i+1)         <= tx_channel_s;
                sca_tx_len_to_gbtsc_o(i+1)             <= tx_len_s;
                sca_tx_command_to_gbtsc_o(i+1)         <= tx_command_s;
                sca_tx_data_to_gbtsc_o(i+1)            <= tx_data_s;    
                sca_start_input_cmd_to_gbtsc_o(i+1)    <= start_input_cmd_s;
                sca_start_reset_cmd_to_gbtsc_o(i+1)    <= start_reset_cmd_s;
                sca_start_connect_cmd_to_gbtsc_o(i+1)  <= start_connect_cmd_s;
                sca_start_test_cmd_to_gbtsc_o(i+1)     <= start_test_cmd_s; 
 
            end generate;
           
            rx_received_s             <= sca_rx_received_from_gbtsc_i(i+1);
            rx_address_s              <= sca_rx_address_from_gbtsc_i(i+1);
            rx_control_s              <= sca_rx_control_from_gbtsc_i(i+1);
            rx_transID_s              <= sca_rx_transID_from_gbtsc_i(i+1);
            rx_channel_s              <= sca_rx_channel_from_gbtsc_i(i+1);
            rx_len_s                  <= sca_rx_len_from_gbtsc_i(i+1);
            rx_error_s                <= sca_rx_error_from_gbtsc_i(i+1);
            rx_data_s                 <= sca_rx_data_from_gbtsc_i(i+1);
            ongoing_s                 <= sca_rx_ongoing_from_gbtsc_i(i+1);
            delimiter_found_s         <= sca_rx_delimiter_found_from_gbtsc_i(i+1);                           

             sca_ctrl_ipbus_inst : entity work.emp_scc_sca_ipbus
             generic map( g_NO_TX_BRAM  =>  g_EC_BROADCAST, g_NO_RX_BRAM => false )
             port map(
                ipb_clk => ipb_clk,
                ipb_rst => ipb_rst,
                ipb_in  => ipbw_ec_spare(i),
                ipb_out => ipbr_ec_spare(i),
                
                Tx_clk_i  => Tx_clk_i,
                Tx_clk_en_i => Tx_clk_en_i,
                
                Rx_clk_i     => Rx_clk_i,
                Rx_clk_en_i  => Rx_clk_en_i,    
                -- SCA commands
                start_input_cmd_o         => start_input_cmd_s,
                start_reset_cmd_o         => start_reset_cmd_s,
                start_connect_cmd_o       => start_connect_cmd_s,
                start_test_cmd_o          => start_test_cmd_s,                
                -- SCA frame fields
                tx_address_o              => tx_address_s,
                tx_transID_o              => tx_transID_s,
                tx_channel_o              => tx_channel_s,
                tx_len_o                  => tx_len_s,
                tx_command_o              => tx_command_s,
                tx_data_o                 => tx_data_s,
                rx_received_i             => rx_received_s,
                rx_address_i              => rx_address_s,
                rx_control_i              => rx_control_s,
                rx_transID_i              => rx_transID_s,
                rx_channel_i              => rx_channel_s,
                rx_len_i                  => rx_len_s,
                rx_error_i                => rx_error_s,
                rx_data_i                 => rx_data_s,
                -- sca_rx module signals                
                rx_ongoing_i              => ongoing_s,
                rx_delimiter_found_i      => delimiter_found_s,
                done_i                    => sca_done_s(i+1)
             );
         
         
         end generate;   
	 end generate;
 
 
 
end rtl;
