----------------------------------------------------------------------------------
-- ic control ipbus code from Alexey Kozyrev
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_emp_scc_ic_ctrl.all;

entity emp_scc_ic_ctrl_ipbus_simple is
      port ( 
        ipb_clk: in std_logic;
		ipb_rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		
	    Tx_clk_i                : in std_logic; 
	    Tx_clk_en_i             : in std_logic := '0';
		Rx_clk_i                : in std_logic := '0';
		Rx_clk_en_i             : in std_logic := '0';
        tx_GBTx_address_o       : out std_logic_vector(7 downto 0);
        tx_register_addr_o      : out std_logic_vector(15 downto 0);
        tx_nb_to_be_read_o      : out std_logic_vector(15 downto 0);		
	    tx_ready_i              : in  std_logic;
        rx_empty_i              : in  std_logic;
        tx_wr_o                 : out std_logic; 
        tx_data_o               : out std_logic_vector(7 downto 0);
        rx_rd_o                 : out std_logic;               
		rx_data_i               : in  std_logic_vector(7 downto 0);
        tx_start_read_o         : out std_logic;
		tx_start_write_o        : out std_logic
		);        
end emp_scc_ic_ctrl_ipbus_simple;

architecture Behavioral of emp_scc_ic_ctrl_ipbus_simple is

       signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
	   signal ipbr: ipb_rbus_array(N_SLAVES - 1 downto 0);

      -- IC configuration
       signal GBTx_address_to_gbtic_si       : std_logic_vector(7 downto 0) :=(others => '0');
       signal Register_addr_to_gbtic_si      : std_logic_vector(15 downto 0) :=(others => '0');
       signal nb_to_be_read_to_gbtic_si      : std_logic_vector(15 downto 0) :=(others => '0');
       -- IC control
       signal start_write_to_gbtic_si        : std_logic := '0';
       signal start_read_to_gbtic_si         : std_logic := '0';
       
 --      signal ipb_IC_read_cmd_strb_s         : std_logic := '0';
       signal lpGBT_IC_read_cmd_strb0_s      : std_logic := '0';
       signal lpGBT_IC_read_cmd_s            : std_logic := '0';
       
--       signal ipb_IC_write_cmd_strb_s         : std_logic := '0';
       signal lpGBT_IC_write_cmd_strb0_s      : std_logic := '0';
       signal lpGBT_IC_write_cmd_s            : std_logic := '0';
             	     
       -- IC FIFO control
       signal ipBUS_to_IC_tx_FIFO_Data_s     : std_logic_vector(31 downto 0);
       signal ipBUS_to_IC_tx_Config_s        : std_logic_vector(31 downto 0);
       signal ipBUS_to_IC_tx_NWRead_s        : std_logic_vector(31 downto 0);
       
       signal data_to_gbtic_si               : std_logic_vector(7 downto 0) :=(others => '0');
       signal wr_to_gbtic_Trig_si            : std_logic := '0';
       signal wr_to_gbtic_si                 : std_logic := '0';
       
       signal IC_rx_FIFO_Data_to_ipBUS_s     : std_logic_vector(31 downto 0) :=(others => '0');
       
       signal data_from_gbtic_so             : std_logic_vector(7 downto 0) :=(others => '0');
       signal rd_to_gbtic_Trig_si            : std_logic := '0';
       signal rd_to_gbtic_si                 : std_logic := '0';
        
       -- IC Status
       signal ic_ready                       : std_logic;
       signal ic_empty                       : std_logic;
       
       signal rd_to_gbtic_si_l1                       : std_logic;
       signal rd_to_gbtic_si_l2                       : std_logic;
       signal rd_to_gbtic_si_l3                       : std_logic;
       signal ipb_IC_read_Status_En_s_l1              : std_logic;
       
       signal ipb_IC_read_Status_strb_s      : std_logic;
       signal ipb_IC_read_Status_En_s        : std_logic;
       signal Status_from_IC_Reg_s           : std_logic_vector(31 downto 0);
       
begin

	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_emp_scc_ic_ctrl(ipb_in.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );
 
 
------------------------------ control signals for IC moduls  --------------------------------------------------------------
---------------------------------------------- write operations -------------------------------------------------------------
-- IC TxRAM       < Config reg  >                                       <  Config reg (for Write) > 
-- IC TxFIFO                                            <fifo pointer >                             <  Data  >
--          <SOF> <lpGBT address> <not used> <not used> <nword> <nword> <int address> <int address> <  data  > <parity> <EOF>
-- nbit      8          8             8          8        8       8          8             8         nword x 8    8       8
-- IC RxFIFO                                                                                        <  Data  >
---------------------------------------------- Read operations----------------------------------------------------------------
-- IC TxRAM        < Config reg  >                       
-- IC_TxRAM                                              <Number of words> 
--                                                       <  to be read   > 
-- IC TxFIFO                                                                                           <  Data  >
--           <SOF> <lpGBT address> <not used> <not used> <nword>   <nword> <int address> <int address> <  data  > <parity> <EOF>
-- nbit       8          8             8          8        8         8          8             8         nword x 8    8       8
-- IC RxFIFO                                                                                           <  Data  >
------------------------------------------------------------------------------------------------------------------------------
-- Slave 15: IC Tx Data register
    
    tx_data_o <= data_to_gbtic_si;
    tx_wr_o   <= wr_to_gbtic_si;
    
	slave15: entity work.ipbus_reg_v
	    generic map ( N_REG => 1 )
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_IC_TxDATA_REG),
			ipbus_out => ipbr(N_SLV_IC_TxDATA_REG),
			q(0) => ipBUS_to_IC_tx_FIFO_Data_s
			-- q(0) FIFO data
  		);
		data_to_gbtic_si          <= ipBUS_to_IC_tx_FIFO_Data_s(7 downto 0); -- Tx FIFO data
		
		Write_data_to_IC_FIFO: process(Tx_clk_i)
	          begin
	             if rising_edge(Tx_clk_i) then 
	                  wr_to_gbtic_Trig_si <= ipbr(N_SLV_IC_TxDATA_REG).ipb_ack;
                      wr_to_gbtic_si <= ipbr(N_SLV_IC_TxDATA_REG).ipb_ack and not wr_to_gbtic_Trig_si;
                 end if;
	    end process;
  
  
-- IC Tx Configuration register

    tx_GBTx_address_o <= GBTx_address_to_gbtic_si;
    tx_register_addr_o <= Register_addr_to_gbtic_si;
    
		slave16: entity work.ipbus_reg_v
	    generic map ( N_REG => 1 )
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_IC_TxConf_REG),
			ipbus_out => ipbr(N_SLV_IC_TxConf_REG),
			q(0) => ipBUS_to_IC_tx_Config_s
    	-- q(0) IC Tx Configuration: I2C slave address 0..7bits, Internal address 23..8bits = 24bits
		);
		GBTx_address_to_gbtic_si  <= ipBUS_to_IC_tx_Config_s(7 downto 0);    -- I2C slave address
        Register_addr_to_gbtic_si <= ipBUS_to_IC_tx_Config_s(23 downto 8);   -- Internal address

-- IC Tx Configuration register: Number of words/bytes to be read (only for read transactions)

    tx_nb_to_be_read_o <= nb_to_be_read_to_gbtic_si;

		slave17: entity work.ipbus_reg_v
	    generic map ( N_REG => 1 )
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_IC_TxNWRead_REG),
			ipbus_out => ipbr(N_SLV_IC_TxNWRead_REG),
			q(0) => ipBUS_to_IC_tx_NWRead_s
    	-- q(0) IC Tx Configuration2: Number of words/bytes to be read (only for read transactions)
		);
		nb_to_be_read_to_gbtic_si  <= ipBUS_to_IC_tx_NWRead_s(15 downto 0);    -- Number of words/bytes to be read (only for read transactions)
  
-- IC Rx Data register

--        rx_rd_o   <= rd_to_gbtic_si;  -- to increment the read pointer. 
--        data_from_gbtic_so <= rx_data_i;
        
--        Read_data_from_rxIC_FIFO: process(Rx_clk_i, ipb_clk)
--	          begin
--	             if rising_edge(Rx_clk_i) then 
--	                  rd_to_gbtic_Trig_si <= ipbw(N_SLV_IC_RxDATA_REG).ipb_strobe;
--                      rd_to_gbtic_si <= ipbw(N_SLV_IC_RxDATA_REG).ipb_strobe and not rd_to_gbtic_Trig_si;
--                      if rd_to_gbtic_si = '1' then
--                           IC_rx_FIFO_Data_to_ipBUS_s(7 downto 0) <= data_from_gbtic_so;
--                           IC_rx_FIFO_Data_to_ipBUS_s(31 downto 8) <= (others => '0');
--                      end if;
--                 end if;
                 
--                 if rising_edge(ipb_clk) then 
--                      ipbr(N_SLV_IC_RxDATA_REG).ipb_ack <= ipbw(N_SLV_IC_RxDATA_REG).ipb_strobe;
--                      ipbr(N_SLV_IC_RxDATA_REG).ipb_rdata <= IC_rx_FIFO_Data_to_ipBUS_s;
--                      ipbr(N_SLV_IC_RxDATA_REG).ipb_err <= '0'; 
--                 end if;
--	    end process;  


        rx_rd_o   <= rd_to_gbtic_si_l1;  -- to increment the read pointer. 
        data_from_gbtic_so <= rx_data_i;
        
        Read_data_from_rxIC_FIFO_kosmas: process(ipb_clk)
	          begin
	             if rising_edge(ipb_clk) then 
	                  rd_to_gbtic_Trig_si <= ipbw(N_SLV_IC_RxDATA_REG).ipb_strobe;
                      
                      rd_to_gbtic_si_l1 <= ipbw(N_SLV_IC_RxDATA_REG).ipb_strobe and not rd_to_gbtic_Trig_si;
                      rd_to_gbtic_si_l2 <= rd_to_gbtic_si_l1;
                      rd_to_gbtic_si_l3 <= rd_to_gbtic_si_l2;

                      if rd_to_gbtic_si_l3 = '1' then
                           IC_rx_FIFO_Data_to_ipBUS_s(7 downto 0) <= data_from_gbtic_so;
                           IC_rx_FIFO_Data_to_ipBUS_s(31 downto 8) <= (others => '0');
                      end if;
 
                      ipbr(N_SLV_IC_RxDATA_REG).ipb_ack <= rd_to_gbtic_si_l3;
                      ipbr(N_SLV_IC_RxDATA_REG).ipb_rdata <= IC_rx_FIFO_Data_to_ipBUS_s;
                      ipbr(N_SLV_IC_RxDATA_REG).ipb_err <= '0'; 
                 end if;
	    end process;  


-----------------------------------------------------------------------------------------------------
-- IC control commands:
-- Status - read status 2 bits: bit 0 - IC tx module ready for transmit
--                              bit 1 - IC rxFIFO status (1 - empty/0 - has data word)

-- Read command - Read operations
-- Write command - Write operations
-----------------------------------------------------------------------------------------------------
-- IC Status register
 
    ic_ready <= tx_ready_i;
    ic_empty <= rx_empty_i;
 
        Read_Status_from_IC: process( ipb_clk)
	          begin
	              -- IC Read Status
	             if rising_edge(ipb_clk) then 
	                 ipb_IC_read_Status_strb_s  <= ipbw(N_SLV_IC_Status_REG).ipb_strobe;
	                 ipb_IC_read_Status_En_s <= ipbw(N_SLV_IC_Status_REG).ipb_strobe and not ipb_IC_read_Status_strb_s;
	                 ipb_IC_read_Status_En_s_l1 <= ipb_IC_read_Status_En_s;
                     if ipb_IC_read_Status_En_s = '1' then
                         Status_from_IC_Reg_s(0) <= ic_ready;
                         Status_from_IC_Reg_s(1) <= ic_empty; 
                         Status_from_IC_Reg_s(31 downto 2) <= (others => '0');
                     end if;  
       
                      ipbr(N_SLV_IC_Status_REG).ipb_ack <= ipb_IC_read_Status_En_s_l1 ;
                      ipbr(N_SLV_IC_Status_REG).ipb_rdata <= Status_from_IC_Reg_s;--(others => '0');
                      ipbr(N_SLV_IC_Status_REG).ipb_err <= '0'; 
                 end if;
	    end process;
	      	
 -- IC control commands: Read, Write

 tx_start_read_o <= start_read_to_gbtic_si;
 tx_start_write_o <= start_write_to_gbtic_si;   

  slave20_21: process(ipb_clk, Tx_clk_i)
	          begin
	              if rising_edge(Tx_clk_i) then  
	                         -- IC Read command
	                          lpGBT_IC_read_cmd_strb0_s <= ipbw(N_SLV_IC_Tx_start_read).ipb_strobe;
		                      lpGBT_IC_read_cmd_s <= ipbw(N_SLV_IC_Tx_start_read).ipb_strobe and not lpGBT_IC_read_cmd_strb0_s; 
		                      start_read_to_gbtic_si <= lpGBT_IC_read_cmd_s;
		                     -- IC Write command
	                          lpGBT_IC_write_cmd_strb0_s <= ipbw(N_SLV_IC_Tx_start_write).ipb_strobe;
		                      lpGBT_IC_write_cmd_s <= ipbw(N_SLV_IC_Tx_start_write).ipb_strobe and not lpGBT_IC_write_cmd_strb0_s;
		                      start_write_to_gbtic_si <= lpGBT_IC_write_cmd_s;
		          end if;
                -- answer to ipBUS
                  -- IC Read command
		          if rising_edge(ipb_clk) then  
		              ipbr(N_SLV_IC_Tx_start_read).ipb_ack <= ipbw(N_SLV_IC_Tx_start_read).ipb_strobe;           
		          end if;
                  ipbr(N_SLV_IC_Tx_start_read).ipb_rdata <= (others => '0');  
                  ipbr(N_SLV_IC_Tx_start_read).ipb_err <= '0'; 
                   -- IC Write command
		          if rising_edge(ipb_clk) then  
		              ipbr(N_SLV_IC_Tx_start_write).ipb_ack <= ipbw(N_SLV_IC_Tx_start_write).ipb_strobe;         
		          end if;
                  ipbr(N_SLV_IC_Tx_start_write).ipb_rdata <= (others => '0');  
                  ipbr(N_SLV_IC_Tx_start_write).ipb_err <= '0';         
      	     end process;
 
end Behavioral;
