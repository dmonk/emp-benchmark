-------------------------------------------------------
--! @file
--! @author Kirika Uchida <kirika.uchida@cern.ch> 
--! @version 0.0
--! @brief 

-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.lpgbtfpga_package.all;
use work.emp_device_decl.all;
package emp_data_framer_decl is
    
    constant UPLINK_USERDATA_MAX_LENGTH   : integer := 230;
    constant DOWNLINK_USERDATA_MAX_LENGTH : integer := 112;
    
    constant N_MAX_EC_SPARE : integer := 16;
    type data_framer_conf_t is 
    record
        ic_simple             : boolean;
        no_ec                 : boolean;
        n_ec_spare            : integer range 0 to N_MAX_EC_SPARE; -- 0 for no spare  
        ec_broadcast          : boolean;
    end record;
 
    type channel_data_framer_conf_array_t is array(0 to 3) of data_framer_conf_t;
    type region_data_framer_conf_array_t is array(0 to N_REGION - 1) of channel_data_framer_conf_array_t;
    
       
    constant kDummyChannelDataFramerConf : data_framer_conf_t := (false, true, 0, false);
    constant kDummyRegionDataFramer : channel_data_framer_conf_array_t := 
    (0=>kDummyChannelDataFramerConf,1=>kDummyChannelDataFramerConf,2=>kDummyChannelDataFramerConf,3=>kDummyChannelDataFramerConf);

end emp_data_framer_decl;