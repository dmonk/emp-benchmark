library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package ipbus_decode_emp_data_framer is

  constant IPBUS_SEL_WIDTH: positive := 2;
  subtype ipbus_sel_t is std_logic_vector(IPBUS_SEL_WIDTH - 1 downto 0);
  function ipbus_sel_emp_data_framer(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t;

  constant N_SLV_SCC: integer := 0;
  constant N_SLV_TX_UDF: integer := 1;
  constant N_SLV_RX_UDF: integer := 2;
  constant N_SLAVES: integer := 3;

end ipbus_decode_emp_data_framer;

package body ipbus_decode_emp_data_framer is

  function ipbus_sel_emp_data_framer(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t is
    variable sel: ipbus_sel_t;
  begin

   if    std_match(addr, "------------------------0-------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_SCC, IPBUS_SEL_WIDTH)); -- scc / base 0x00000000 / mask 0x00000080
   elsif std_match(addr, "------------------------10------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_TX_UDF, IPBUS_SEL_WIDTH)); -- txf / base 0x00000080 / mask 0x000000c0
   elsif std_match(addr, "------------------------11------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_RX_UDF, IPBUS_SEL_WIDTH)); -- rxf / base 0x000000c0 / mask 0x000000c0
    else
        sel := ipbus_sel_t(to_unsigned(N_SLAVES, IPBUS_SEL_WIDTH));
    end if;

    return sel;

  end function ipbus_sel_emp_data_framer;

end ipbus_decode_emp_data_framer;

