-------------------------------------------------------
--! @file
--! @author Kirika Uchida <kirika.uchida@cern.ch> 
--! @version 0.0
--! @brief this module contains slow dommand controller and user data framers. 
-------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.ipbus_decode_emp_data_framer.all;
use work.emp_data_framer_decl.all;

entity emp_data_framer is
	generic (
	         LPGBT          : integer range 0 to 1 := 1;
	         INDEX          : integer;
	         CHANNEL_INDEX  : integer := 0;
	         IC_SIMPLE       : boolean := false;
	         NO_EC          : boolean := false;
		     N_EC_SPARE     : integer range 0 to N_MAX_EC_SPARE := 0;
		     EC_BROADCAST   : boolean := false

	);
    port (
        -- ipbus
        clk_i                    : in std_logic;  --- for ipbus (31MHz)
        rst_i                    : in std_logic;  --- for ipbus
        ipb_in                   : in  ipb_wbus;
        ipb_out                  : out ipb_rbus; 
        
        -- channel buffer 
        clk_p_i                : in  std_logic;  --- payload clock 320MHz clock
	 	txdata_i                 : in  lword;         
	 	rxdata_o                 : out lword;
	 	
	 	-- link signals synchronized with clk_p_i
        mgt_tx_rdy_i             : in  std_logic;
        uplink_rdy_i             : in  std_logic;
        downlink_rdy_i           : in  std_logic;  --- ready signal from lpgbtfpga_downlink    
        uplink_clk_i             : in  std_logic;
        uplink_clken_i           : in  std_logic;  --- '1' for 1 clock every 8 clock.
        uplink_user_data_i       : in  std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        uplink_ic_data_i         : in  std_logic_vector(1 downto 0);
        uplink_ec_data_i         : in  std_logic_vector(1 downto 0);
        downlink_clk_i           : in  std_logic;
        downlink_clken_i         : in  std_logic;
        downlink_clken_o         : out std_logic;
        downlink_user_data_o     : out std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        downlink_ic_data_o       : out std_logic_vector(1 downto 0);
        downlink_ec_data_o       : out std_logic_vector(1 downto 0)
        );   
end emp_data_framer;

architecture interface of emp_data_framer is

 	signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
	signal ipbr: ipb_rbus_array(N_SLAVES - 1 downto 0);
 
    -- for output data
    signal downlink_user_data_l             : std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0);
    signal downlink_ic_data_l               : std_logic_vector(1 downto 0);
    signal downlink_ec_data_l               : std_logic_vector(1 downto 0);


    -- slow command control signals
    signal scc_downlink_clken_s             : std_logic;
    signal scc_downlink_ic_data_s           : std_logic_vector(1 downto 0);
    signal scc_downlink_ec_data_s           : std_logic_vector(1 downto 0);
    signal scc_downlink_ec_spare_data_s     : std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);

    -- tx framer signals
    signal tx_user_data_framer_clken_s          : std_logic;
    signal tx_user_data_framer_user_data_s      : std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0);
    
    -- rx framer signals
    signal rx_user_data_framer_uplink_ec_spare_data_s : std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);

begin

    -- ipbus address decode		
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_emp_data_framer(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
    );

    downlink_data_proc: process(mgt_tx_rdy_i, clk_p_i)
        variable cnter : integer range 0 to 7;
    begin
        if mgt_tx_rdy_i = '0' then
            cnter := 0;
            downlink_clken_o  <= '0';
        
        elsif rising_edge(clk_p_i) then
            cnter := cnter + 1;
            
            if cnter = 8 then
                cnter := 0;                              
            end if;
                        
            downlink_clken_o <= '0';
            if cnter = 0 then
                downlink_clken_o     <= '1'; 
                downlink_user_data_o <= downlink_user_data_l;
                downlink_ic_data_o   <= downlink_ic_data_l;
                downlink_ec_data_o   <= downlink_ec_data_l; 
            end if;
            if scc_downlink_clken_s = '1' then
                downlink_ic_data_l <= scc_downlink_ic_data_s;
                downlink_ec_data_l <= scc_downlink_ec_data_s;
            end if;
            if tx_user_data_framer_clken_s = '1' then
                downlink_user_data_l <= tx_user_data_framer_user_data_s;
            end if;
        
        end if;
        
    end process;

    slow_command_ctrl_inst : entity work.emp_slow_command_ctrl
    generic map(
        CHANNEL_INDEX     => CHANNEL_INDEX,
        g_ToLpGBT         => LPGBT,
        IC_SIMPLE         => IC_SIMPLE,
        g_NO_EC           => NO_EC,
        g_SPARE_SCA_COUNT => N_EC_SPARE,
        g_EC_BROADCAST    => EC_BROADCAST,
        g_Debug           => true
    )
    port map(
        clk_i                    => clk_i,
        rst_i                    => rst_i,
        ipb_in                   => ipbw(N_SLV_SCC),
        ipb_out                  => ipbr(N_SLV_SCC),  
        clk_p_i                  => clk_p_i,
        uplink_clk_i             => uplink_clk_i,
        uplink_rdy_i             => uplink_rdy_i,
        downlink_rdy_i           => downlink_rdy_i,
        uplink_clken_i           => uplink_clken_i,
        uplink_ic_data_i         => uplink_ic_data_i,
        uplink_ec_data_i         => uplink_ec_data_i,
        uplink_ec_spare_data_i   => rx_user_data_framer_uplink_ec_spare_data_s,
        downlink_clk_i           => downlink_clk_i,
        downlink_clken_i         => downlink_clken_i,
        downlink_clken_o         => scc_downlink_clken_s,
        downlink_ic_data_o       => scc_downlink_ic_data_s,
        downlink_ec_data_o       => scc_downlink_ec_data_s,
        downlink_ec_spare_data_o => scc_downlink_ec_spare_data_s       
        ); 

   tx_user_data_framer_inst: entity work.emp_tx_user_data_framer
       generic map(INDEX => INDEX, CHANNEL_INDEX => CHANNEL_INDEX, N_EC_SPARE => N_EC_SPARE)
       port map( 
            clk_i            => clk_i,
            rst_i            => rst_i,
            ipb_in           => ipbw(N_SLV_TX_UDF),
            ipb_out          => ipbr(N_SLV_TX_UDF),  
            downlink_rdy_i   => downlink_rdy_i,
            clk_p_i          => clk_p_i,
            data_i           => txdata_i,
            ec_spare_data_i  => scc_downlink_ec_spare_data_s,
            clken_o          => tx_user_data_framer_clken_s,
            user_data_o      => tx_user_data_framer_user_data_s
   );
   
    rx_user_data_framer_inst: entity work.emp_rx_user_data_framer
        generic map( INDEX            => INDEX,
                     CHANNEL_INDEX    => CHANNEL_INDEX,
                     N_EC_SPARE => N_EC_SPARE
            )
           port map(
                clk_i              => clk_i,
                rst_i              => rst_i,
                ipb_in             => ipbw(N_SLV_RX_UDF),
                ipb_out            => ipbr(N_SLV_RX_UDF),  
                clk_p_i            => clk_p_i, 
                uplink_rdy_i       => uplink_rdy_i,
                clken_i            => uplink_clken_i,        
                user_data_i        => uplink_user_data_i,
                data_o             => rxdata_o,
                ec_spare_data_o    => rx_user_data_framer_uplink_ec_spare_data_s
        );  

end interface;
 