-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;

--	constant LWORD_WIDTH: integer := 64;
--	type lword is
--		record
--			data: std_logic_vector(LWORD_WIDTH - 1 downto 0); 
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
--      the data should be updated every 8 clock cycle in 320 MHz clock.
--      the strobe should be '1' for three clock cycles.
--      the data should not change during the strobe is '1'.

entity emp_tx_user_data_framer is
	generic (
	         INDEX          : integer;
	         CHANNEL_INDEX  : integer := 0;
		 N_EC_SPARE     : integer range 0 to 16 := 0
	);
	port (
	    clk_i              : in std_logic;  --- for ipbus (31MHz)
	    rst_i              : in std_logic;  --- for ipbus
	    ipb_in             : in ipb_wbus;   --- ipbus data in (from PC)
	    ipb_out            : out ipb_rbus;  --- ipbus data out (to PC)
            downlink_rdy_i     : in  std_logic;
            clk_p_i            : in  std_logic;
            data_i             : in  lword; 
            ec_spare_data_i    : in  std_logic_vector(N_MAX_EC_SPARE * 2 -1 downto 0);
            clken_o            : out std_logic;
            user_data_o        : out std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0)
        );   
end emp_tx_user_data_framer;

architecture interface of emp_tx_user_data_framer is    
   
end interface;
