library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package ipbus_decode_emp_fe_mgt is

  constant IPBUS_SEL_WIDTH: positive := 2;
  subtype ipbus_sel_t is std_logic_vector(IPBUS_SEL_WIDTH - 1 downto 0);
  function ipbus_sel_emp_fe_mgt(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t;

  constant N_SLV_LPGBT: integer := 0;
  constant N_SLV_GBT: integer := 1;
  constant N_SLV_DATA_FRAMER: integer := 2;
  constant N_SLAVES: integer := 3;

end ipbus_decode_emp_fe_mgt;

package body ipbus_decode_emp_fe_mgt is

  function ipbus_sel_emp_fe_mgt(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t is
    variable sel: ipbus_sel_t;
  begin

   if    std_match(addr, "-----------------------00-------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_LPGBT, IPBUS_SEL_WIDTH)); -- scc / base 0x00000000 / mask 0x00000110
   elsif std_match(addr, "-----------------------01-------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_GBT, IPBUS_SEL_WIDTH)); -- txf / base 0x00000010 / mask 0x00000010
   elsif std_match(addr, "-----------------------1--------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_DATA_FRAMER, IPBUS_SEL_WIDTH)); -- txf / base 0x00000100 / mask 0x00000100   
    else
        sel := ipbus_sel_t(to_unsigned(N_SLAVES, IPBUS_SEL_WIDTH));
    end if;

    return sel;

  end function ipbus_sel_emp_fe_mgt;

end ipbus_decode_emp_fe_mgt;

