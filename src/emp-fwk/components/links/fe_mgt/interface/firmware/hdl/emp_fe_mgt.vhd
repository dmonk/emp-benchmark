-------------------------------------------------------
--! @file
--! @author Kirika Uchida <kirika.uchida@cern.ch> 
--! @version 0.0
--! @brief this module contains data framer and lpgbtfpga interface.
-------------------------------------------------------

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

use work.emp_framework_decl.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_data_types.all;
use work.emp_project_decl.all;
use work.ipbus_decode_emp_fe_mgt.all;

entity emp_fe_mgt is
  generic (
     INDEX          : integer;
     N_CHANNELS     : integer := 4
     ); 
port 
(  
  --usrclk         : in  std_logic;
  clk              : in  std_logic; 
  rst              : in  std_logic; 
  csel_in          : in  std_logic_vector(2 downto 0);
  fe_mgt_ipb_in    : in  ipb_wbus;
  fe_mgt_ipb_out   : out ipb_rbus;
  refclk           : in  std_logic;
  clk_p            : in  std_logic;    
  clk40            : in  std_logic; 
  rxclk_mon        : out std_logic;
  txclk_mon        : out std_logic;
  txdata_in        : in  ldata(N_CHANNELS - 1 DOWNTO 0); -- array of 64 bit vectors 
  rxdata_out       : out ldata(N_CHANNELS - 1 DOWNTO 0);
  cfg_hdr10        : out std_logic;
  cfg_hdr11        : out std_logic
);  
end emp_fe_mgt;

architecture rtl of emp_fe_mgt is
        signal ipbw : ipb_wbus_array(N_SLAVES-1 downto 0);
        signal ipbr : ipb_rbus_array(N_SLAVES-1 downto 0);
        signal ipbw_df : ipb_wbus_array(3 downto 0);
        signal ipbr_df : ipb_rbus_array(3 downto 0);
begin
         -- ipbus address decode		
        fabric_fe_mgt: entity work.ipbus_fabric_sel
        generic map(
            NSLV => N_SLAVES,
            SEL_WIDTH => IPBUS_SEL_WIDTH
            )
        port map(
          ipb_in          => fe_mgt_ipb_in,
          ipb_out         => fe_mgt_ipb_out,
          sel             => ipbus_sel_emp_fe_mgt(fe_mgt_ipb_in.ipb_addr),
          ipb_to_slaves   => ipbw,
          ipb_from_slaves => ipbr
        );
 
       fabric_df : entity work.ipbus_fabric_sel
          generic map(
            NSLV      => 4,
            SEL_WIDTH => 2
            )
          port map(
            sel             => csel_in(2 downto 1),
            ipb_in          => ipbw(N_SLV_DATA_FRAMER),
            ipb_out         => ipbr(N_SLV_DATA_FRAMER),
            ipb_to_slaves   => ipbw_df,
            ipb_from_slaves => ipbr_df
            ); 
            
        lpgbt_gen : if REGION_CONF(INDEX).mgt_i_kind = lpgbt generate    
            lpgbt_inst : entity work.emp_lpgbt
              generic map(
                INDEX          => INDEX,
                N_CHANNELS     => N_CHANNELS,
                DEBUG          => true
                )
              port map(
                clk             => clk,
                rst             => rst,          
                csel_in         => csel_in,
                lpgbt_ipb_in    => ipbw(N_SLV_LPGBT),
                lpgbt_ipb_out   => ipbr(N_SLV_LPGBT),
                df_ipb_in       => ipbw_df,
                df_ipb_out      => ipbr_df,
                refclk          => refclk,
                clk_p           => clk_p,
                rxclk_mon       => rxclk_mon,
                txclk_mon       => txclk_mon,
                txdata_in       => txdata_in,
                rxdata_out      => rxdata_out,
                cfg_hdr10       => cfg_hdr10,
                cfg_hdr11       => cfg_hdr11
                );
            
        end generate;    

    gbt_gen : if REGION_CONF(INDEX).mgt_i_kind = gbt generate
        gbt_inst: entity work.emp_gbt
        generic map(
            INDEX         => INDEX,
            N_CHANNELS     => N_CHANNELS
            )
        port map(
            clk        => clk,
            rst        => rst,
            csel_in    => csel_in,
            ipb_in     => ipbw(N_SLV_GBT),
            ipb_out    => ipbr(N_SLV_GBT),
            df_ipb_in  => ipbw_df,
            df_ipb_out => ipbr_df,
            mgtRefClk  => refclk,   
            clkp       => clk_p,
            clk40      => clk40,        
            rxclk_mon  => rxclk_mon,
            txclk_mon  => txclk_mon,   
            txdata_in  => txdata_in,
            rxdata_out => rxdata_out
            ); 
   end generate gbt_gen; 

end rtl;
