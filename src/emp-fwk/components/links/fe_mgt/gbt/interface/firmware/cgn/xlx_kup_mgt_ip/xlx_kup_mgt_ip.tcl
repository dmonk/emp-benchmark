generate_target synthesis [get_files xlx_kup_mgt_ip.xci]
set_property used_in_synthesis false [get_files -of_object [get_files xlx_kup_mgt_ip.xci] -filter {FILE_TYPE == XDC}]
set_property used_in_implementation false [get_files -of_object [get_files xlx_kup_mgt_ip.xci] -filter {FILE_TYPE == XDC}]
