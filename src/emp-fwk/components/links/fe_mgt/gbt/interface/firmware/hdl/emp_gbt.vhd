----------------------------------------------------------------------------------
--  emp gbt wrapper , kadamidi, 2020
--  modified by Kirika Uchida 14.09.2020
----------------------------------------------------------------------------------
-- reset_from_genRst is asserted from ipbus, vio, and when txFrameClkPllLocked = '0' goes into xlx_ku_gbt_example_design
-- this reset is extended for CLK_FREQ times txFrameClk with xlx_ku_reset
-- txAligned_latched synchronized with txFrameClk_from_txPll and reset after reset_from_genRst.
-- txAligned_from_gbtbank from gbtbank from gear box in gbtbank in xlx_ku_gbt_example_design.
-- txAlignComputed_from_gbtbank from gear box in gbtbank

-- mgtTxReset_s = '1' when genRstMgtClk_sync_s = '1' or genTxRstMgtClk_sync_s = '1' and goes to '0' at MGTCLK_I rising edge.
-- mgtRxReset_s	= '1' when MGT_TX_RSTDONE_S = '0' or genRstMgtClk_sync_s = '1'  or genRxRstMgtClk_sync_s = '1' and goes to '0' at MGTCLK_I rising edge.
-- gbtTxReset_s	= '1' when mgtTxReady_sync_s = '0' or genReset_tx = '1' and goes to '0' at TX_FRAMECLK_I rising edge.
-- gbtRxReset_s	= '1' when mgtRxReady_sync_s = '0' or genReset_rx = '1' and goes to '0' at RX_FRAMECLK_I rising edge.
-- mgtTxReady_sync_s goes to '1' at TX_FRAMECLK_I rising edge. 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_project_decl.all;
use work.emp_gbt_package.all;

use work.gbt_bank_package.all;
use ieee.numeric_std.all;
use work.emp_data_types.all;
use work.emp_data_framer_decl.all;
use work.emp_project_decl.all;

entity emp_gbt is
    generic( INDEX       : integer;
						 N_CHANNELS  : integer := 4 );
    Port( 

        clk         : in std_logic;
        rst         : in std_logic;
        csel_in     : in  std_logic_vector(2 downto 0);
        ipb_in      : in ipb_wbus;
        ipb_out     : out ipb_rbus;       
        df_ipb_in   : in  ipb_wbus_array(3 downto 0);
        df_ipb_out  : out ipb_rbus_array(3 downto 0); 
        mgtRefClk   : in std_logic; 
        clkp        : in std_logic;
        clk40       : in std_logic;       
        rxclk_mon   : out std_logic;
        txclk_mon   : out std_logic;     
        txdata_in   : in ldata(3 downto 0);
        rxdata_out  : out ldata(3 downto 0)
        );
end emp_gbt;

architecture Behavioral of emp_gbt is
     
   -- constants  
   constant TX_OPTIMIZATION  : integer := REGION_GBT_CONF(INDEX).tx_optimization; 
   constant RX_OPTIMIZATION  : integer := REGION_GBT_CONF(INDEX).rx_optimization;
   constant TX_ENCODING      : integer := REGION_GBT_CONF(INDEX).tx_encoding;
   constant RX_ENCODING      : integer := REGION_GBT_CONF(INDEX).rx_encoding;
     
   COMPONENT xlx_kup_vio
     PORT (
       clk : IN STD_LOGIC;
       probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_in8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
       probe_out2 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
     );
   END COMPONENT;

   constant NUM_LINK_Conf : integer := N_CHANNELS; 
   constant GBT_DEBUG : integer := 1;
 
   constant VIO         : string := "yes";
   constant N_CTRL      : integer := NUM_LINK_Conf * NUMBER_OF_CHAN_RW_REGS + NUMBER_OF_COMMON_RW_REGS;
   constant N_STAT      : integer := NUM_LINK_Conf * NUMBER_OF_CHAN_RO_REGS + NUMBER_OF_COMMON_RO_REGS;

   signal ctrl          : ipb_reg_v(NUM_LINK_Conf * NUMBER_OF_CHAN_RW_REGS + NUMBER_OF_COMMON_RW_REGS - 1 downto 0);
   signal stat          : ipb_reg_v(NUM_LINK_Conf * NUMBER_OF_CHAN_RO_REGS + NUMBER_OF_COMMON_RO_REGS - 1 downto 0);
   signal chan_ro_reg   : type_chan_ro_reg_array(3 downto 0);
   signal chan_rw_reg   : type_chan_rw_reg_array(3 downto 0);
   signal common_ro_reg : type_common_ro_reg;
   signal common_rw_reg : type_common_rw_reg;
   signal ctrl_stb      : std_logic_vector(N_CTRL-1 downto 0);  

   signal reset_from_genRst     : std_logic_vector(1 to NUM_LINK_Conf);
   signal txPllReset            : std_logic;
   signal txFrameClk_from_txPll : std_logic;
   signal txFrameClkPllLocked   : std_logic ;
   
   signal gbtErrorDetected                  : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbtModifiedBitFlag                : gbt_reg84_A(1 to NUM_LINK_Conf);
  
   signal txFrameClk    : std_logic_vector(1 to NUM_LINK_Conf);
   signal txWordClk     : std_logic_vector(1 to NUM_LINK_Conf);
   signal rxFrameClk    : std_logic_vector(1 to NUM_LINK_Conf);
   signal rxWordClk     : std_logic_vector(1 to NUM_LINK_Conf);
   signal rxFrameClkReady_from_gbtExmplDsgn    : std_logic_vector(1 to NUM_LINK_Conf);
   
   signal SFP_RX_P    : std_logic_vector(1 to NUM_LINK_Conf);
   signal SFP_RX_N    : std_logic_vector(1 to NUM_LINK_Conf);
   signal SFP_TX_P    : std_logic_vector(1 to NUM_LINK_Conf);
   signal SFP_TX_N    : std_logic_vector(1 to NUM_LINK_Conf);
   signal DEBUG_CLK_ALIGNMENT_debug      : std_logic_vector(2 downto 0);
   
   signal rxDataErrorSeen_from_gbtExmplDsgn : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_txclken_s                     : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_rxclken_s                     : std_logic_vector(1 to NUM_LINK_Conf);

   signal txAligned_from_gbtbank           : std_logic_vector(1 to NUM_LINK_Conf);
   signal txAlignComputed_from_gbtbank     : std_logic_vector(1 to NUM_LINK_Conf);
   signal txAligned_latched                : std_logic_vector(1 to NUM_LINK_Conf);
   
   -- Control:
   -----------   
   signal resetgbtfpga                                   : std_logic_vector(1 to NUM_LINK_Conf);
   signal generalReset_from_user                         : std_logic_vector(1 to NUM_LINK_Conf);     
   signal manualResetTx_from                             : std_logic_vector(1 to NUM_LINK_Conf);
   signal manualResetTx_from_user                        : std_logic_vector(1 to NUM_LINK_Conf);
   signal manualResetRx_from_user                        : std_logic_vector(1 to NUM_LINK_Conf);
   signal testPatterSel_from_user                        : gbt_reg2_A(1 to NUM_LINK_Conf);
   signal loopBack_from_user                             : gbt_reg3_A(1 to NUM_LINK_Conf);
   signal txIsDataSel_from_user                          : std_logic_vector(1 to NUM_LINK_Conf); 
   signal mgtReady                                       : std_logic_vector(1 to NUM_LINK_Conf);

   signal gbtRxReady                                     : std_logic_vector(1 to NUM_LINK_Conf);  
   signal rxIsData_from_gbt                              : std_logic_vector(1 to NUM_LINK_Conf);       
   signal rxExtrDataWidebusErSeen_from_gbtExmplDsgn      : std_logic_vector(1 to NUM_LINK_Conf);
   signal rxBitSlipRstOnEvent_from_user                  : std_logic_vector(1 to NUM_LINK_Conf);
   
   -- Data:

   --------------------------------------------------          
   signal txMatchFlag_from_gbtExmplDsgn                  : std_logic;

   signal manualResetRx_from_vio   : std_logic_vector(1 to NUM_LINK_Conf);
   signal manualResetTx_from_vio   : std_logic_vector(1 to NUM_LINK_Conf);
   signal manualResetTx            : std_logic_vector(1 to NUM_LINK_Conf);
   signal manualResetRx            : std_logic_vector(1 to NUM_LINK_Conf);

   signal resetgbtfpga_vio         : std_logic_vector(1 to NUM_LINK_Conf);
   signal testPatterSel            : gbt_reg2_A(1 to NUM_LINK_Conf);
   signal manualResetRx_from       : std_logic_vector(1 to NUM_LINK_Conf);
   signal resetgbtfpga_from        : std_logic_vector(1 to NUM_LINK_Conf);
   signal generalReset_from_user_n : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_tx_pol               : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_rx_pol               : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_tx_pol_from_user     : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_rx_pol_from_user     : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbtRxReady_s             : std_logic_vector(1 to NUM_LINK_Conf);
   signal mgtReady_s               : std_logic_vector(1 to NUM_LINK_Conf);
   signal cplllock_out             : std_logic_vector(1 to NUM_LINK_Conf);

   signal gbt_output_data          : gbt_reg80_A(1 to NUM_LINK_Conf);
   signal gbt_input_data           : gbt_reg80_A(1 to NUM_LINK_Conf);
   signal gbt_wb_output_data       : gbt_reg112_A(1 to NUM_LINK_Conf);
   signal gbt_wb_input_data        : gbt_reg112_A(1 to NUM_LINK_Conf);
   
   signal gbt_input_ic             : gbt_reg2_A(1 to NUM_LINK_Conf);
   signal gbt_input_ec             : gbt_reg2_A(1 to NUM_LINK_Conf);
   signal gbt_output_ic            : gbt_reg2_A(1 to NUM_LINK_Conf);
   signal gbt_output_ec            : gbt_reg2_A(1 to NUM_LINK_Conf);
   

   signal gbttx_ready              : std_logic_vector(1 to NUM_LINK_Conf);
   
   -- debug
   signal mgt_tx_reset_s           : std_logic_vector(1 to NUM_LINK_Conf);
   signal mgt_rx_reset_s           : std_logic_vector(1 to NUM_LINK_Conf);   
   signal gbt_tx_reset_s           : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_rx_reset_s           : std_logic_vector(1 to NUM_LINK_Conf);    
   signal mgt_tx_rstdone_s         : std_logic_vector(1 to NUM_LINK_Conf);    
   signal mgt_rx_rstdone_s         : std_logic_vector(1 to NUM_LINK_Conf);    
   signal mgt_rxready_nobitslip_s  : std_logic_vector(1 to NUM_LINK_Conf);    
   signal pll_locked_s             : std_logic_vector(1 to NUM_LINK_Conf);    
   signal mgt_headerflag_s         : std_logic_vector(1 to NUM_LINK_Conf);  
   signal rx_reset_sig_s           : std_logic_vector(1 to NUM_LINK_Conf);  
   signal rxbitslip_s              : std_logic_vector(1 to NUM_LINK_Conf);     
   signal mgt_headerlocked_s       : std_logic_vector(1 to NUM_LINK_Conf);     
   signal done_from_rxBitSlipControl_s       : std_logic_vector(1 to NUM_LINK_Conf);     
   signal mgt_rxword_s                       : word_mxnbit_A (1 to NUM_LINK_Conf); 
   signal resetGtxTx_from_rxBitSlipControl_s : std_logic_vector(1 to NUM_LINK_Conf);
   signal resetGtxRx_from_rxBitSlipControl_s : std_logic_vector(1 to NUM_LINK_Conf);
   signal gbt_output_user_data_s             : gbt_reg112_A(1 to NUM_LINK_Conf);

   attribute keep : string;
--   attribute keep of chan_ro_reg : signal is "true";
   attribute keep of manualResetTx_from_vio : signal is "true";
   attribute keep of manualResetRx_from_vio : signal is "true";
   attribute keep of resetgbtfpga_vio       : signal is "true";
   
   attribute keep of gbtRxReady_s : signal is "true";   
   attribute keep of rxIsData_from_gbt : signal is "true";   
   attribute keep of mgtReady_s        : signal is "true";
begin
   
   
   rxBitSlipRstOnEvent_from_user <= (others => '0');
   
  clk_div: entity work.freq_ctr_div
    generic map(
      N_CLK => 2
    )
    port map(
      clk(0) => rxWordClk(1),
      clk(1) => txWordClk(1),
      clkdiv(0) => rxclk_mon,
      clkdiv(1) => txclk_mon
    );

    debug_gen : if GBT_DEBUG = 1 generate
    -- txFrameClk -- 40MHz
    -- rxFrameClk -- 40MHz    
    -- txWordClk -- 120MHz
    -- rxWordClk -- 120MHz
        ila_gen : for i in 1 to 1 generate
            
            ila_sc_inst : entity work.ila_sc
                PORT MAP (
                clk => clk40,
                probe0 => gbt_input_IC(i),
                probe1 => gbt_output_IC(i),
                probe2 => gbt_input_EC(i),
                probe3 => gbt_output_EC(i)
                );
        
             ila_gbt_inst : entity work.ila_gbt       
                port map (
                clk => rxWordClk(i),
                probe0(0) => reset_from_genRst(i),
                probe1(0) => mgt_tx_reset_s(i),
                probe2(0) => mgt_rx_reset_s(i),
                probe3(0) => gbt_tx_reset_s(i),
                probe4(0) => gbt_rx_reset_s(i),
                probe5(0) => mgt_tx_rstdone_s(i), -- tx_reset_done(i) and txfsm_reset_done(i); 
                probe6(0) => mgt_rx_rstdone_s(i), -- rx_reset_done(i) and rxfsm_reset_done(i) and done_from_rxBitSlipControl(i);
                probe7(0) => gbttx_ready(i), -- not(gbt_txreset_s(i));
                probe8(0) => mgtReady(i),    -- mgt_txready_s(i) and mgt_rxready_s(i) == rx_reset_done(i) and rxfsm_reset_done(i) and done_from_rxBitSlipControl(i);
                probe9(0) => gbtRxReady(i),  -- mgt_rxready_s(i) and gbt_rxready_s(i);            
                probe10(0) => rxbitslip_s(i),           
                probe11(0) => done_from_rxBitSlipControl_s(i),
                probe12(0) => mgt_rxready_nobitslip_s(i), -- mgt_txready_s(i) and mgt_rxready_s(i), these signals are '1' if the last reset completed.  May not be the current status.
                probe13(0) => mgt_headerlocked_s(i),
                probe14(0) => mgt_headerflag_s(i),
                probe15(0) => rx_reset_sig_s(i),
                probe16    => mgt_rxword_s(i),
                probe17(0) => resetGtxTx_from_rxBitSlipControl_s(i),
                probe18(0) => resetGtxRx_from_rxBitSlipControl_s(i)
                
                );
                -- pll_locked_s(i), -- phalgnr out == not rx_reset == mgt_ready
                -- rxFrameClkReady_from_gbtExmplDsgn(i),--phalgnr out == not rx_reset == mgt_ready
        end generate;
    end generate;


    data_framers_gen : for i in 1 to NUM_LINK_Conf generate

        signal mgt_tx_rdy_s             : std_logic;
        signal uplink_rdy_s             : std_logic;
        signal downlink_rdy_s           : std_logic;
        signal uplink_clk_s             : std_logic;
        signal uplink_clken_s           : std_logic;  --- '1' when the data is stable.  Should be in 40 MHz.
        signal uplink_user_data_s       : std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        signal uplink_ic_data_s         : std_logic_vector(1 downto 0);
        signal uplink_ec_data_s         : std_logic_vector(1 downto 0);
        signal downlink_clk_s           : std_logic;
        signal downlink_clken_in_s      : std_logic;
        signal downlink_clken_out_s     : std_logic;
        signal downlink_is_data_s       : std_logic;
        signal downlink_user_data_s     : std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        signal downlink_ic_data_s       : std_logic_vector(1 downto 0);
        signal downlink_ec_data_s       : std_logic_vector(1 downto 0);       
    begin
    
        mgt_tx_rdy_s   <= mgt_tx_rstdone_s(i);
        uplink_rdy_s   <= gbtRxReady(i);
        downlink_rdy_s <= gbttx_ready(i);
        uplink_clk_s   <= clkp;
        downlink_clk_s <= clkp;

        gbt_output_user_data_s(i) <= x"00000000" & gbt_output_data(i) when RX_ENCODING = GBT_FRAME else
                                     gbt_wb_output_data(i);
        to_clkp : process ( clkp ) 
            variable timer : natural range 0 to 7 := 0;
        begin
            
            if uplink_rdy_s = '0' then
            
                timer := 0;
                uplink_clken_s <= '0';
                uplink_user_data_s <= (others => '0');
                downlink_clken_in_s <= '0';
                
            elsif rising_edge(clkp) then
                
                uplink_user_data_s(111 downto 0) <=  gbt_output_user_data_s(i);   
                uplink_ic_data_s <= gbt_output_IC(i);
                uplink_ec_data_s <= gbt_output_EC(i);
                
                uplink_clken_s <= '0';
                downlink_clken_in_s <= '0';
                if timer = 0 then
                    uplink_clken_s <= '1';
                    downlink_clken_in_s <= '1';
                end if;
                
                if timer = 7 then
                    timer := 0;
                else
                    timer := timer + 1;
                end if;
            
            end if;
        
        end process;
        to_txframeclk : process( txFrameClk_from_txPll )
        begin
          if downlink_rdy_s = '0' then
               
               txIsDataSel_from_user(i) <= '0';
               
          elsif rising_edge( txFrameClk_from_txPll ) then
 
              txIsDataSel_from_user(i) <= '1';
              
              if TX_OPTIMIZATION = GBT_FRAME then
                gbt_input_data(i) <= downlink_user_data_s(79 downto 0);  
              else
                gbt_wb_input_data(i) <= downlink_user_data_s(111 downto 0);  
              end if;

              gbt_input_IC(i) <= downlink_ic_data_s;
              gbt_input_EC(i) <= downlink_ec_data_s;  
                     
         end if; 
          
        end process;
        
        data_framer_inst: entity work.emp_data_framer
        generic map(
                    LPGBT => 0,
                    INDEX => INDEX, 
                    CHANNEL_INDEX => i-1,
                    IC_SIMPLE     => REGION_DATA_FRAMER_CONF(INDEX)(i-1).IC_SIMPLE,
                    NO_EC => REGION_DATA_FRAMER_CONF(INDEX)(i-1).NO_EC,
                    N_EC_SPARE => REGION_DATA_FRAMER_CONF(INDEX)(i-1).N_EC_SPARE, 
                    EC_BROADCAST => REGION_DATA_FRAMER_CONF(INDEX)(i-1).EC_BROADCAST )
        port map( 
            clk_i                    => clk,
            rst_i                    => rst,
            ipb_in                   => df_ipb_in(i-1),
            ipb_out                  => df_ipb_out(i-1),  
            clk_p_i                  => clkp,
            txdata_i                 => txdata_in(i-1),
            rxdata_o                 => rxdata_out(i-1),
            mgt_tx_rdy_i             => mgt_tx_rdy_s,
            uplink_rdy_i             => uplink_rdy_s,
            downlink_rdy_i           => downlink_rdy_s,    
            uplink_clk_i             => uplink_clk_s,
            uplink_clken_i           => uplink_clken_s,
            uplink_user_data_i       => uplink_user_data_s,
            uplink_ic_data_i         => uplink_ic_data_s,
            uplink_ec_data_i         => uplink_ec_data_s,
            downlink_clk_i           => downlink_clk_s,
            downlink_clken_i         => downlink_clken_in_s,
            downlink_clken_o         => downlink_clken_out_s,
            downlink_user_data_o     => downlink_user_data_s,
            downlink_ic_data_o       => downlink_ic_data_s,
            downlink_ec_data_o       => downlink_ec_data_s
           );

    end generate;
    
--    strobe: for i in 3 downto NUM_LINK_Conf generate
        
--        rxdata_out(i).strobe <= '1';
--        rxdata_out(i).data   <= (others=>'0');
--        rxdata_out(i).valid  <= '0';
    
--    end generate;   
          


    --=========================--
    -- GBT Bank example design --
    --=========================-- 
    gbtExmplDsgn_inst: entity work.xlx_ku_gbt_example_design
      generic map(
        NUM_LINKS                          => NUM_LINK_Conf,            -- Up to 4
        TX_OPTIMIZATION                    => TX_OPTIMIZATION,     -- LATENCY_OPTIMIZED or STANDARD
        RX_OPTIMIZATION                    => RX_OPTIMIZATION,     -- LATENCY_OPTIMIZED or STANDARD
        TX_ENCODING                        => TX_ENCODING,         -- GBT_FRAME or WIDE_BUS
        RX_ENCODING                        => RX_ENCODING,         -- GBT_FRAME or WIDE_BUS
       
        DATA_GENERATOR_ENABLE              => DATA_GENERATOR_ENABLE_Conf,
        DATA_CHECKER_ENABLE                => DATA_CHECKER_ENABLE_Conf,
        MATCH_FLAG_ENABLE                  => MATCH_FLAG_ENABLE_Conf,
        CLOCKING_SCHEME                    => CLOCKING_SCHEME_Conf
        )
      port map (

        --==============--
        -- Clocks       -
        --==============-
        FRAMECLK_40MHZ                     => txFrameClk_from_txPll,
        XCVRCLK                            => mgtRefClk,
 
        TX_FRAMECLK_O                      => txFrameClk,        
        TX_WORDCLK_O                       => txWordClk,          
        RX_FRAMECLK_O                      => rxFrameClk,
        RX_WORDCLK_O                       => rxWordClk,      
 
--        gbt_txclken_out                    => gbt_txclken_s,
--        gbt_rxclken_out                    => gbt_rxclken_s,
                
        RX_FRAMECLK_RDY_O                  => rxFrameClkReady_from_gbtExmplDsgn,
 
        --==========
        -- Reset    
        --==========
        GBTBANK_GENERAL_RESET_I            => reset_from_genRst,
        GBTBANK_MANUAL_RESET_TX_I          => manualResetTx,
        GBTBANK_MANUAL_RESET_RX_I          => manualResetRx,
     
        --=============
        -- Serial lanes
        --=============
        GBTBANK_MGT_RX_P                   => SFP_RX_P,
        GBTBANK_MGT_RX_N                   => SFP_RX_N,
        GBTBANK_MGT_TX_P                   => SFP_TX_P,
        GBTBANK_MGT_TX_N                   => SFP_TX_N,
     
        --=============
        -- Data        
        --==============--     
        GBTBANK_GBT_DATA_IN                => gbt_input_data,
        GBTBANK_WB_DATA_IN                 => gbt_wb_input_data,
        
        GBTBANK_IC_I                       => gbt_input_IC,
        GBTBANK_EC_I                       => gbt_input_EC,
     
        TX_DATA_O                          => open,            
        WB_DATA_O                          => open,
        GBTBANK_GBT_DATA_OUT               => gbt_output_data,
        GBTBANK_WB_DATA_OUT                => gbt_wb_output_data,
        
        GBTBANK_IC_O                       => gbt_output_IC,
        GBTBANK_EC_O                       => gbt_output_EC,
        
        --============
        -- Reconf.       
        --===========
        GBTBANK_MGT_DRP_RST                => '0',
        GBTBANK_MGT_DRP_CLK                => clk,
  
        --============
        -- TX ctrl     
        --=============
        TX_ENCODING_SEL_i                  => "0000",  -- encoding is static
        GBTBANK_TX_ISDATA_SEL_I            => txIsDataSel_from_user,
        GBTBANK_TEST_PATTERN_SEL_I         => testPatterSel_from_user, 
       
        --==============-
        -- RX ctrl      --
        --==============--
        RX_ENCODING_SEL_i                            => "0000",    -- encoding is static
        GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I         => "0000",    -- not used
        GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I          => "0000",     -- not used
        GBTBANK_RXFRAMECLK_ALIGNPATTER_I             => DEBUG_CLK_ALIGNMENT_debug,
        GBTBANK_RXBITSLIT_RSTONEVEN_I                => rxBitSlipRstOnEvent_from_user,
       
        --==============-
        -- TX Status    -
        --==============-
		GBTBANK_GBTTX_READY_O				         => gbttx_ready, -- not(gbt_txreset_s(i));
        GBTBANK_LINK_READY_O                         => mgtReady,    --  mgt_txready_s(i) and mgt_rxready_s(i) == rx_reset_done(i) and rxfsm_reset_done(i) and done_from_rxBitSlipControl(i);
        GBTBANK_TX_MATCHFLAG_O                       => txMatchFlag_from_gbtExmplDsgn,
        GBTBANK_TX_ALIGNED_O                         => txAligned_from_gbtbank,
        GBTBANK_TX_ALIGNCOMPUTED_O                   => txAlignComputed_from_gbtbank,
       
        --==============--
        -- RX Status    --
        --==============--
        GBTBANK_GBTRX_READY_O                        => gbtRxReady, --mgt_rxready_s(i) and gbt_rxready_s(i);
        GBTBANK_GBTRXREADY_LOST_FLAG_O               => open,       --coming from pattern checker
        GBTBANK_RXDATA_ERRORSEEN_FLAG_O              => open,       --coming from pattern checker
        GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O => open,       --coming from pattern checker
        GBTBANK_RX_MATCHFLAG_O                       => open,       --
        GBTBANK_RX_ISDATA_SEL_O                      => rxIsData_from_gbt, -- 
        GBTBANK_RX_ERRORDETECTED_O                   => gbtErrorDetected,
        GBTBANK_RX_BITMODIFIED_FLAG_O                => gbtModifiedBitFlag,  -- should it be connected to ipbud ? -- maybe count number of ones and connect this to ipbus
        GBTBANK_RXBITSLIP_RST_CNT_O                  => open,
        
        --==============--
        -- XCVR ctrl    --
        --==============--
        GBTBANK_LOOPBACK_I                           => loopBack_from_user,

        GBTBANK_TX_POL                               => gbt_tx_pol_from_user,
        GBTBANK_RX_POL                               => gbt_rx_pol_from_user,
        -- test
        MGT_TX_RESET_O                        => mgt_tx_reset_s,
        MGT_RX_RESET_O                        => mgt_rx_reset_s,
        GBT_TX_RESET_O                        => gbt_tx_reset_s,
        GBT_RX_RESET_O                        => gbt_rx_reset_s,

        MGT_TX_RSTDONE_O                      => mgt_tx_rstdone_s,
        MGT_RX_RSTDONE_O                      => mgt_rx_rstdone_s,
        mgt_rxready_nobitslip_o                         => mgt_rxready_nobitslip_s,
        pll_locked_o                          => pll_locked_s,
        mgt_headerflag_o                      => mgt_headerflag_s,
        rx_reset_sig_o                        => rx_reset_sig_s,
        rxbitslip_o                           => rxbitslip_s,
        mgt_headerlocked_o                    => mgt_headerlocked_s,
        done_from_rxBitSlipControl_o => done_from_rxBitSlipControl_s,
        mgt_rxword_o => mgt_rxword_s,
        resetGtxTx_from_rxBitSlipControl_o => resetGtxTx_from_rxBitSlipControl_s,
        resetGtxRx_from_rxBitSlipControl_o => resetGtxRx_from_rxBitSlipControl_s
        ); 


    txFrameclkGen_inst: entity work.xlx_ku_tx_phaligner
      port map( 
        -- Reset
        RESET_IN              => txPllReset,    
        
        -- Clocks
        CLK_IN                => clk40,
        CLK_OUT               => txFrameClk_from_txPll, -- 40MHz
        
        -- Control
        SHIFT_IN              => '0',
        SHIFT_COUNT_IN        => (others=>'0'), 
        
        -- Status
        LOCKED_OUT            => txFrameClkPllLocked 
        );
      
    --===============--
    -- Resets -- 
    --===============--      
    interface_gen: for i in 1 to NUM_LINK_Conf generate
    
    
       generalReset_from_user_n <= not generalReset_from_user;
       
       genRst: entity work.xlx_ku_reset
          generic map (
             CLK_FREQ                                    => 40e6)
          port map (     
             CLK_I                                       => txFrameClk_from_txPll, 
             RESET1_B_I                                  => generalReset_from_user_n(i), 
             RESET2_B_I                                  => generalReset_from_user_n(i),
             RESET_O                                     => reset_from_genRst(i) 
          ); 
     
            
        generalReset_from_user(i) <= resetgbtfpga(i) or not(txFrameClkPllLocked) or resetgbtfpga_vio(1);
        manualResetTx(i)          <= manualResetTx_from_user(i) or manualResetTx_from_vio(i);
        manualResetRx(i)          <= manualResetRx_from_user(i) or manualResetRx_from_vio(i);
            
        alignmenetLatchProc: process(txFrameClk_from_txPll)
        begin
        
            if reset_from_genRst(i) = '1' then
                txAligned_latched(i) <= '0';
                
            elsif rising_edge(txFrameClk_from_txPll) then
            
               if txAlignComputed_from_gbtbank(i) = '1' then
                    txAligned_latched(i) <= txAligned_from_gbtbank(i);
               end if;
               
            end if;
        end process;
     
     end generate;
 
    --===============--    
    -- IPBUS
    --===============--
     csr: entity work.ipbus_ctrlreg_v
         generic map(
                N_CTRL => NUM_LINK_Conf * NUMBER_OF_CHAN_RW_REGS + NUMBER_OF_COMMON_RW_REGS,
                N_STAT => NUM_LINK_Conf * NUMBER_OF_CHAN_RO_REGS + NUMBER_OF_COMMON_RO_REGS
                )
         port map(
                clk       => clk,
                reset     => rst,
                ipbus_in  => ipb_in,
                ipbus_out => ipb_out,
                d         => stat,
                q         => ctrl
                );

    chan_reg_gen : for i in NUM_LINK_Conf-1 downto 0 generate
        
        chan_ro_gen : for j in NUMBER_OF_CHAN_RO_REGS - 1 downto 0 generate
            stat(NUMBER_OF_CHAN_RO_REGS * i + j) <= chan_ro_reg(i)(j);
        end generate;
        
        chan_rw_gen : for j in NUMBER_OF_CHAN_RW_REGS - 1 downto 0 generate
            chan_rw_reg(i)(j) <= ctrl(NUMBER_OF_CHAN_RW_REGS * i + j);
        end generate;
    end generate;

    common_ro_gen : for j in NUMBER_OF_COMMON_RO_REGS - 1 downto 0 generate
        stat(NUM_LINK_Conf * NUMBER_OF_CHAN_RO_REGS + j) <= common_ro_reg(j);
    end generate;
    common_rw_gen : for j in NUMBER_OF_COMMON_RW_REGS - 1 downto 0 generate
        common_rw_reg(j) <= ctrl(NUM_LINK_Conf * NUMBER_OF_CHAN_RW_REGS + j);
    end generate;


    -- Loop over all channels
    reg_chan_gen : for i in 1 to NUM_LINK_Conf generate
  
        ---------------------------------------------------------------------------- 
        -- ReadOnly Regs. All TTC domain
        -----------------------------------------------------------------------------
        chan_ro_reg(i-1)(0) <= x"10000" & "000" & 
                               cplllock_out(i) &                              -- mask 0x00000100
                               txAligned_latched(i) &                         -- mask 0x00000080
                               rxExtrDataWidebusErSeen_from_gbtExmplDsgn(i) & -- mask 0x00000040
                               rxDataErrorSeen_from_gbtExmplDsgn(i) &         -- mask 0x00000020
                               "0" &                        -- mask 0x00000010
                               gbtRxReady_s(i) &                              -- mask 0x00000008
                               mgtReady_s(i) &                                -- mask 0x00000004
                               gbtErrorDetected(i) &                          -- mask 0x00000002   Pulsed when error has been corrected by the decoder - latch it?
                               rxIsData_from_gbt(i);                          -- mask 0x80000001
         chan_ro_reg(i-1)(1) <= x"000000"& "000" &
                                rxbitslip_s(i) &
                                gbt_rx_reset_s(i)&
                                gbt_tx_reset_s(i)&
                                mgt_rx_rstdone_s(i)&
                                mgt_tx_rstdone_s(i);
         chan_ro_reg(i-1)(2) <=  mgt_rxword_s(i)(31 downto 0);
         chan_ro_reg(i-1)(3) <=  x"0000000" & mgt_rxword_s(i)(39 downto 32);    
         chan_ro_reg(i-1)(4) <= gbt_output_user_data_s(i)(31 downto 0);
         chan_ro_reg(i-1)(5) <= gbt_output_user_data_s(i)(63 downto 32);
         chan_ro_reg(i-1)(6) <= gbt_output_user_data_s(i)(95 downto 64);
         chan_ro_reg(i-1)(7) <= x"000" & gbt_output_user_data_s(i)(111 downto 96);                                            
        -----------------------------------------------------------------------------
        -- ReadWrite Regs. 
        -----------------------------------------------------------------------------
        -- loopback is async input
        loopBack_from_user(i)                <= chan_rw_reg(i-1)(0)(2 downto 0); -- mask 0x07
        resetgbtfpga(i)                      <= chan_rw_reg(i-1)(0)(3);          -- mask 0x08 
        manualResetTx_from_user(i)           <= chan_rw_reg(i-1)(0)(4);          -- mask 0x10
        manualResetRx_from_user(i)           <= chan_rw_reg(i-1)(0)(5);          -- mask 0x20 
        testPatterSel(i)                     <= chan_rw_reg(i-1)(0)(7 downto 6); -- mask 0xc0
        gbt_tx_pol(i)                        <= chan_rw_reg(i-1)(0)(11);         -- mask 0x0800
        gbt_rx_pol(i)                        <= chan_rw_reg(i-1)(0)(12);         -- mask 0x1000

    end generate;
 
    csr_sync: entity work.gbt_synchronizer
    generic map( NUM_LINK_Conf => NUM_LINK_Conf)
    port map(
            ipb_clk => clk,
            txFrameClk => txFrameClk,
            txWordClk  => txWordClk,
            rxWordClk  => rxWordClk,
            testPatterSel_i => testPatterSel,
            gbt_tx_pol_i => gbt_tx_pol,
            gbt_rx_pol_i => gbt_rx_pol,
            gbtRxReady_i => gbtRxReady,
            mgtReady_i   => mgtReady,
            
            testPatterSel_o => testPatterSel_from_user,
            gbt_tx_pol_o => gbt_tx_pol_from_user,
            gbt_rx_pol_o => gbt_rx_pol_from_user,
            gbtRxReady_o => gbtRxReady_s,
            mgtReady_o   => mgtReady_s
            );

    --===============--    
    -- VIO
    --===============--  
    vio_gen : if VIO = "yes"  generate 
        interface_gen: for i in 1 to NUM_LINK_Conf generate
           vio : xlx_kup_vio
             PORT MAP (
               clk => clk,
                        
               probe_in0(0) => rxIsData_from_gbt(i),
               probe_in1(0) => txFrameClkPllLocked,
               probe_in2(0) => txAlignComputed_from_gbtbank(i),
               probe_in3(0) => mgtReady_s(i),
               probe_in4(0) => gbtRxReady_s(i),
               probe_in5(0) => txAligned_latched(i),
               probe_in6(0) => rxExtrDataWidebusErSeen_from_gbtExmplDsgn(i),
               probe_in7(0) => rxDataErrorSeen_from_gbtExmplDsgn(i),
               probe_in8(0) => '0',
        
               probe_out0(0)  => resetgbtfpga_vio(i),
               probe_out1(0)  => manualResetTx_from_vio(i), 
               probe_out2(0)  => manualResetRx_from_vio(i)
             );
                   
         end generate;
    end generate;
  
end Behavioral;
