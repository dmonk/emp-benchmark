----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/01/2020 02:35:31 PM
-- Design Name: 
-- Module Name: gbt_synchronizer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.emp_gbt_package.all;
use work.gbt_bank_package.all;

entity gbt_synchronizer is
  Generic (NUM_LINK_Conf : integer );
  Port (
        ipb_clk    : in std_logic;
        txFrameClk : in std_logic_vector(1 to NUM_LINK_Conf);
        txWordClk  : in std_logic_vector(1 to NUM_LINK_Conf);
        rxWordClk  : in std_logic_vector(1 to NUM_LINK_Conf);
        
        testPatterSel_i : in  gbt_reg2_A(1 to NUM_LINK_Conf);
        gbt_tx_pol_i : in  std_logic_vector(1 to NUM_LINK_Conf);
        gbt_rx_pol_i : in  std_logic_vector(1 to NUM_LINK_Conf);
        gbtRxReady_i : in  std_logic_vector(1 to NUM_LINK_Conf);
        mgtReady_i   : in  std_logic_vector(1 to NUM_LINK_Conf);
       
        testPatterSel_o : out gbt_reg2_A(1 to NUM_LINK_Conf);
        gbt_tx_pol_o : out std_logic_vector(1 to NUM_LINK_Conf);
        gbt_rx_pol_o : out std_logic_vector(1 to NUM_LINK_Conf);
        gbtRxReady_o : out std_logic_vector(1 to NUM_LINK_Conf);
        mgtReady_o   : out std_logic_vector(1 to NUM_LINK_Conf)
        
         );
end gbt_synchronizer;

architecture Behavioral of gbt_synchronizer is

begin

 bit_sync_gen : for i in 1 to NUM_LINK_Conf generate

   bit_synchronizer_txpol_inst : entity work.bit_synchronizer
    port map(
        clk_in => txWordClk(i),
        i_in   => gbt_tx_pol_i(i),
        o_out  => gbt_tx_pol_o(i)
        );  

   bit_synchronizer_rxpol_inst : entity work.bit_synchronizer
    port map(
        clk_in => rxWordClk(i),
        i_in   => gbt_rx_pol_i(i),
        o_out  => gbt_rx_pol_o(i)
        );  

   bit_synchronizer_rxready_inst : entity work.bit_synchronizer
    port map(
        clk_in => ipb_clk,
        i_in   => gbtRxReady_i(i),
        o_out  => gbtRxReady_o(i)
        );  

   bit_synchronizer_mgtready_inst : entity work.bit_synchronizer
    port map(
        clk_in => ipb_clk,
        i_in   => mgtReady_i(i),
        o_out  => mgtReady_o(i)
        );  


  pattSel_sync : for j in 1 downto 0 generate
    
     bit_synchronizer_testPatterSe_inst : entity work.bit_synchronizer
      port map(
          clk_in => txFrameClk(i),
          i_in   => testPatterSel_i(i)(j),
          o_out  => testPatterSel_o(i)(j)
          );    
     
  end generate;

end generate;

end Behavioral;
