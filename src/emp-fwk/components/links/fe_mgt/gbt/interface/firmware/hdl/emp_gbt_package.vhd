-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @modified for emp - kadamidi
--! @brief GBT-FPGA IP - Common package
-------------------------------------------------------

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.emp_device_decl.all;

package emp_gbt_package is  

   constant NUMBER_OF_CHAN_RW_REGS : integer := 1;
   constant NUMBER_OF_CHAN_RO_REGS : integer := 8;
   constant NUMBER_OF_COMMON_RW_REGS : integer := 0;
   constant NUMBER_OF_COMMON_RO_REGS : integer := 0;

   -- CHANNEL register space
   subtype type_chan_ro_reg is gbt_reg32_A(NUMBER_OF_CHAN_RO_REGS-1 downto 0);
   subtype type_chan_rw_reg is gbt_reg32_A(NUMBER_OF_CHAN_RW_REGS-1 downto 0);
   
   type type_chan_ro_reg_array is array(natural range <>) of type_chan_ro_reg;
   type type_chan_rw_reg_array is array(natural range <>) of type_chan_rw_reg;    
   -- COMMON register space
   subtype type_common_ro_reg is gbt_reg32_A(NUMBER_OF_COMMON_RO_REGS-1 downto 0);
   subtype type_common_rw_reg is gbt_reg32_A(NUMBER_OF_COMMON_RW_REGS-1 downto 0);  

   --=============================== Constant Declarations ===============================--
   constant ENABLED								: integer := 1;                              --! Enable constant definition
   constant DISABLED							: integer := 0;                              --! Disable constant definition
   
   constant GATED_CLOCK			                : integer := 0;
   constant PLL					                : integer := 1;
	
   constant BC_CLOCK                            : integer := 0;
   constant FULL_MGTFREQ                        : integer := 1;    

   --================================= Configuration ================================-- 
--   constant TX_OPTIMIZATION_Conf                : integer := STANDARD;
--   constant RX_OPTIMIZATION_Conf                : integer := STANDARD;
--   constant TX_ENCODING_Conf                    : integer := GBT_FRAME; --GBT_FRAME;
--   constant RX_ENCODING_Conf                    : integer := WIDE_BUS; --WIDE_BUS;

   constant DATA_GENERATOR_ENABLE_Conf          : integer := DISABLED;
   constant DATA_CHECKER_ENABLE_Conf            : integer := DISABLED;
   constant MATCH_FLAG_ENABLE_Conf              : integer := DISABLED;
   constant CLOCKING_SCHEME_Conf                : integer := BC_CLOCK ; --BC_CLOCK;

    type region_gbt_conf_t is
    record
        tx_optimization   : integer;
        rx_optimization   : integer;
        tx_encoding       : integer;
        rx_encoding       : integer;
    end record;

    type region_gbt_conf_array_t is array(0 to N_REGION - 1) of region_gbt_conf_t;
    constant kDummyRegionGbt : region_gbt_conf_t := (STANDARD, STANDARD, GBT_FRAME, GBT_FRAME);


--   	type gword is
--		record
--			data: std_logic_vector(84 downto 0);
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
		
--	type gdata is array(natural range <>) of gword;	
   --=====================================================================================--   
   
       
end emp_gbt_package;
