-- emp_ttc_cmd_sim
--
-- Minimal placeholder for emp_ttc_cmd in simulation
-- without external TTC input
--
-- Tom Williams, July 2019


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.VComponents.all;

use work.emp_ttc_decl.all;

-------------------------------------------------------------------------------
entity emp_ttc_cmd_sim is
  generic(
    LHC_BUNCH_COUNT : integer
    );
  port(
    clk         : in  std_logic;         -- Main TTC clock
    rst         : in  std_logic;
    sclk        : in  std_logic;         -- Sampling clock for TTC data
    sclk_locked : in  std_logic;
    ttc_in_p    : in  std_logic;         -- TTC datastream from AMC13
    ttc_in_n    : in  std_logic;
    l1a         : out std_logic;         -- L1A output
    cmd         : out ttc_cmd_t;  -- B-command output (zero if no command)
    sinerr_ctr  : out std_logic_vector(15 downto 0);
    dberr_ctr   : out std_logic_vector(15 downto 0);
    c_delay     : in  std_logic_vector(4 downto 0);  -- Coarse delay for TTC signals
    en_ttc      : in  std_logic;         -- enable TTC inputs
    err_rst     : in  std_logic          -- Err ctr reset
    );

end emp_ttc_cmd_sim;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture rtl of emp_ttc_cmd_sim is

begin

  l1a <= '0';
  cmd <= TTC_BCMD_NULL;

  sinerr_ctr <= X"0000";
  dberr_ctr  <= X"0000";

end rtl;
-------------------------------------------------------------------------------
