-- ttc_clocks
--
-- Clock generation for LHC clocks
--
-- Dave Newbold, June 2013

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.emp_framework_decl.all;

use work.emp_project_decl.all;

-------------------------------------------------------------------------------
entity emp_ttc_clocks_sim is
  port(
    clko_40  : out std_logic;
    clko_p   : out std_logic;
    clko_aux : out std_logic_vector(2 downto 0);
    rsto_40  : out std_logic;
    rsto_p   : out std_logic;
    rsto_aux : out std_logic_vector(2 downto 0);
    stopped  : out std_logic;
    locked   : out std_logic;
    rst_mmcm : in  std_logic;
    rsti     : in  std_logic
    );

end emp_ttc_clocks_sim;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture rtl of emp_ttc_clocks_sim is

  signal clk40, clk_p : std_logic                    := '1';
  signal clk_aux      : std_logic_vector(2 downto 0) := (others => '1');

  constant TRGT_CLK_PERIOD : real := 12500.0;
  constant DIVIDEND : integer := CLOCK_RATIO * CLOCK_AUX_RATIO(0) * CLOCK_AUX_RATIO(1) * CLOCK_AUX_RATIO(2);
  constant CLK_PERIOD : integer := integer(TRGT_CLK_PERIOD/real(DIVIDEND))*DIVIDEND;

begin


  clk40   <= not clk40 after CLK_PERIOD * ps when rst_mmcm='0' else '0';
  clko_40 <= clk40;
  
  process(clk40)
  begin
    if rising_edge(clk40) then
      rsto_40 <= rsti;
    end if;
  end process;

  clk_p   <= not clk_p after (CLK_PERIOD / CLOCK_RATIO) * ps when rst_mmcm='0' else '0';
  clko_p  <= clk_p;

  process(clk_p)
  begin
    if rising_edge(clk_p) then
      rsto_p <= rsti;
    end if;
  end process;

  gen : for i in 2 downto 0 generate

    clk_aux(i)  <= not clk_aux(i) after (CLK_PERIOD / CLOCK_AUX_RATIO(i)) * ps when rst_mmcm='0' else '0';
    clko_aux(i) <= clk_aux(i);

    process(clk_aux(i))
    begin
      if rising_edge(clk40) then
        rsto_aux(i) <= rsti;
      end if;
    end process;

  end generate;

  stopped <= '0';
  locked  <= '1';

end rtl;
-------------------------------------------------------------------------------
