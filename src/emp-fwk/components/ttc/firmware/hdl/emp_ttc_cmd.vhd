-- emp_ttc_cmd
--
-- Decoder for TTC commands
--
-- All signals synchronous to clk unless stated
-- Priority for commands is:
--      external TTC
--      internal BC0 (highest priority)
--      internal
--      no action (default)
--
-- Dave Newbold, July 2013

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.VComponents.all;

use work.emp_ttc_decl.all;
use work.ttc_codec_pkg.all;

-------------------------------------------------------------------------------
entity emp_ttc_cmd is
  generic(
    ENABLE_EXTERNAL_INPUT : boolean;
    LHC_BUNCH_COUNT : integer
    );
  port(
    clk         : in  std_logic;         -- Main TTC clock
    rst         : in  std_logic;
    sclk        : in  std_logic;         -- Sampling clock for TTC data
    sclk_locked : in  std_logic;
    ttc_in_p    : in  std_logic;         -- TTC datastream from AMC13
    ttc_in_n    : in  std_logic;
    l1a         : out std_logic;         -- L1A output
    cmd         : out ttc_cmd_t;  -- B-command output (zero if no command)
    sinerr_ctr  : out std_logic_vector(15 downto 0);
    dberr_ctr   : out std_logic_vector(15 downto 0);
    c_delay     : in  std_logic_vector(4 downto 0);  -- Coarse delay for TTC signals
    en_ttc      : in  std_logic;         -- enable TTC inputs
    err_rst     : in  std_logic          -- Err ctr reset
    );

end emp_ttc_cmd;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture rtl of emp_ttc_cmd is

  signal ttc_info, ttc_info_sclk : ttc_info_type;
  signal ttc_stat, ttc_stat_sclk : ttc_stat_type;
  signal ttc_in, ttc_in_d, sinerr, dberr, stb_ttc, ttc_in_pre_iob : std_logic;
  signal cmd_ttc, cmda, cmdb, cmdc, ext_cmd_i        : ttc_cmd_t;
  signal ttc_l1a, ext_cmd_pend, ext_cmd_rx, cmd_slot : std_logic;
  signal sinerr_ctr_i, dberr_ctr_i                   : unsigned(15 downto 0);

  attribute keep : boolean;
  attribute keep of ttc_in  : signal is true;


begin

  gen_ext: if ENABLE_EXTERNAL_INPUT=true generate
    buf: IBUFDS
      port map(
        i => ttc_in_p,
        ib => ttc_in_n,
        o => ttc_in_pre_iob
      );
  end generate gen_ext;

  gen_int: if ENABLE_EXTERNAL_INPUT=false generate
    ttc_in_pre_iob <= '0';
  end generate gen_int;

  iob_in: process(sclk)
  begin
    if rising_edge(sclk) then
      ttc_in <= ttc_in_pre_iob;
    end if;
  end process;


  --ddr: IDDRE1
  --    generic map(
  --            DDR_CLK_EDGE => "SAME_EDGE"
  --    )
  --    port map(
  --            q1 => ttc_data(0),
  --            q2 => ttc_data(1),
  --            c => sclk,
  --            cb => not sclk, --ce => '1',
  --            d => ttc_in,
  --            r => '0' --,
  --            --s => '0'
  --    );
  ttc_in_d <= ttc_in;

--  cdel : SRLC32E
--    port map(
--      q   => ttc_in_d,
--      d   => ttc_in,
--      clk => sclk,
--      ce  => '1',
--      a   => c_delay
--      );

  decode : entity work.ttc_decoder
    port map
    (
      rst_i     => not sclk_locked,
      clk160_i  => sclk,
      data_i    => ttc_in_d,
      info_o    => ttc_info_sclk,
      stat_o    => ttc_stat_sclk
    );

  process (clk)
  begin
    if rising_edge(clk) then
      ttc_info <= ttc_info_sclk;
      ttc_stat <= ttc_stat_sclk;
    end if;
  end process;

  ttc_l1a <= ttc_info.l1accept;
  sinerr  <= ttc_stat.err_sng;
  dberr   <= ttc_stat.err_dbl;
  stb_ttc <= ttc_info.brc_strobe;
  cmd_ttc <= ttc_info.brc_t2 & ttc_info.brc_d4 & ttc_info.brc_e & ttc_info.brc_b;

  l1a <= ttc_l1a and en_ttc;
  cmd <= cmd_ttc when en_ttc = '1' and stb_ttc = '1' else TTC_BCMD_NULL;

  process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' or err_rst = '1' then
        sinerr_ctr_i <= (others => '0');
        dberr_ctr_i  <= (others => '0');
      else
        if sinerr = '1' and sinerr_ctr_i /= X"ffff" then
          sinerr_ctr_i <= sinerr_ctr_i + 1;
        end if;
        if dberr = '1' and dberr_ctr_i /= X"ffff" then
          dberr_ctr_i <= dberr_ctr_i + 1;
        end if;
      end if;
    end if;
  end process;

  sinerr_ctr <= std_logic_vector(sinerr_ctr_i);
  dberr_ctr  <= std_logic_vector(dberr_ctr_i);

end rtl;
-------------------------------------------------------------------------------
