-- emp_slim_infra: Clocks & control I/O module for PCIe-based boards with external oscillator
--
-- Raghunandan Shukla (TIFR), Kristian Harder (RAL), Tom Williams (RAL)
-- based on code from Dave Newbold

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.ipbus_axi_decl.all;

library UNISIM;
use UNISIM.VComponents.all;

entity emp_slim_infra is
  generic (
    OSC_CLK_FREQ: real
  );
  port (
    -- PCIe clock and reset (active low)
    pcie_sys_clk_p : in  std_logic;
    pcie_sys_clk_n : in  std_logic;
    pcie_sys_rst_n : in  std_logic;
    -- PCIe lanes
    pcie_rxp       : in  std_logic_vector(0 downto 0);
    pcie_rxn       : in  std_logic_vector(0 downto 0);
    pcie_txp       : out std_logic_vector(0 downto 0);
    pcie_txn       : out std_logic_vector(0 downto 0);
    -- external oscillator
    osc_clk_p      : in  std_logic;
    osc_clk_n      : in  std_logic;
    -- External resets
    nuke           : in std_logic;
    soft_rst       : in std_logic;
    -- IPbus clock and reset (out)
    ipb_clk        : out std_logic;
    ipb_rst        : out std_logic;
    -- IPbus (from / to slaves)
    ipb_in         : in  ipb_rbus;
    ipb_out        : out ipb_wbus;
    -- Pseudo clock 40, for standalone 
    clk_40ext      : out std_logic;
    clk_40ish      : out std_logic;
    -- Heartbeat signal
    onehz          : out std_logic
  );
end emp_slim_infra;


architecture rtl of emp_slim_infra is

  signal ipb_clk_i : std_logic;
  signal locked, clk_locked, pcie_user_lnk_up, rst125, rst_ipb, rst_ipb_ctrl, rst_axi, onehz_i : std_logic;

  signal pcie_sys_rst_n_i : std_logic;

  signal axi_ms: axi4mm_ms(araddr(63 downto 0), awaddr(63 downto 0), wdata(63 downto 0));
  signal axi_sm: axi4mm_sm(rdata(63 downto 0));

  signal ipb_pkt_done : std_logic;
  signal trans_in  : ipbus_trans_in;
  signal trans_out : ipbus_trans_out;

begin

  sys_rst_n_ibuf : IBUF
    port map (
      O => pcie_sys_rst_n_i,
      I => pcie_sys_rst_n
      );

  clocks : entity work.clocks_us_serdes
    port map (
      clki_fr => axi_ms.aclk,
      clki_125 => axi_ms.aclk,
      clko_ipb => ipb_clk_i,
      clko_aux => clk_40ish,
      -- clko_200: out std_logic; -- 200MHz clock for idelayctrl
      eth_locked => pcie_user_lnk_up,
      locked => clk_locked,
      nuke => nuke,
      soft_rst => soft_rst,
      rsto_125 => rst125,
      rsto_ipb => rst_ipb,
      rsto_eth => rst_axi,
      rsto_ipb_ctrl => rst_ipb_ctrl,
      onehz => onehz_i
    );

  ipb_clk <= ipb_clk_i;
  ipb_rst <= rst_ipb;

  locked <= clk_locked and pcie_user_lnk_up;
  onehz <= onehz_i and locked;


  osc_clock : entity work.emp_oscclk
    generic map (
      OSC_FREQ => OSC_CLK_FREQ
    )
    port map (
      osc_clk_p    => osc_clk_p,
      osc_clk_n    => osc_clk_n,
      clk_40ext => clk_40ext
    );


  dma: entity work.pcie_xdma_axi_us_if
    port map (
      pcie_sys_clk_p => pcie_sys_clk_p,
      pcie_sys_clk_n => pcie_sys_clk_n,
      pcie_sys_rst_n => pcie_sys_rst_n_i,

      pcie_tx_p => pcie_txp,
      pcie_tx_n => pcie_txn,
      pcie_rx_p => pcie_rxp,
      pcie_rx_n => pcie_rxn,

      pcie_user_lnk_up => pcie_user_lnk_up,

      axi_ms => axi_ms,
      axi_sm => axi_sm,

      pcie_int_event0 => ipb_pkt_done
    );


  ipbus_transport_axi: entity work.ipbus_transport_axi_if
    generic map (
      ADDRWIDTH => 13
    )
    port map (
      ipb_clk => ipb_clk_i,
      rst_ipb => rst_ipb_ctrl,
      axi_in => axi_ms,
      axi_out => axi_sm,

      pkt_done => ipb_pkt_done,

      ipb_trans_rx => trans_in,
      ipb_trans_tx => trans_out
    );


  ipbus_transactor : entity work.transactor
    port map (
      clk            => ipb_clk_i,
      rst            => rst_ipb_ctrl,
      ipb_out        => ipb_out,
      ipb_in         => ipb_in,
      ipb_req        => open,
      ipb_grant      => '1',
      trans_in       => trans_in,
      trans_out      => trans_out,
      cfg_vector_in  => (others => '0'),
      cfg_vector_out => open
    );


end rtl;
