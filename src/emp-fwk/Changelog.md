# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- README and ChangeLog.
- Working name for the framework - `emp`: Extensible Modular Processor (or ElectroMagnetic Pulse, if you prefer)
- `mpultra`: Area constraints for `infra` `ttc` `info`: Dedicated pblock for MMCMs.
- Desing-level description of regions equipped with MGTs. Used in synthsis-level checks.

### Changed
- `sim`: Declaration files rationalised and factorised in common vs `sim` specific.
- `mpultra`: Factorisation of declaration files propagated to `mpultra` design