-- emp_project_decl
--
-- Defines constants for the whole device

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_device_types.all;
use work.lpgbtfpga_package.all;
use work.emp_lpgbt_decl.all;
use work.emp_data_framer_decl.all;
use work.emp_gbt_package.all;
-------------------------------------------------------------------------------
package emp_project_decl is

  constant PAYLOAD_REV         : std_logic_vector(31 downto 0) := X"12345678";

  -- Number of LHC bunches 
  constant LHC_BUNCH_COUNT    : integer             := 3564;
  -- Latency buffer size
  constant LB_ADDR_WIDTH      : integer             := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer             := 32;
  constant CLOCK_RATIO        : integer             := 8;
  constant CLOCK_AUX_RATIO    : clock_ratio_array_t := (2, 4, 8);

   
  -- Only used by nullalgo   
  constant PAYLOAD_LATENCY    : integer             := 5;

  -- mgt -> chk -> buf -> fmt -> (algo) -> (fmt) -> buf -> chk -> mgt -> clk -> altclk
  constant REGION_CONF : region_conf_array_t := (
--    0  => (gbt, u_crc32, no_buf, no_fmt, no_buf, u_crc32, gbt),   --Bank 225 -- Right Column
--    0  => (gbt, u_crc32, buf, no_fmt, buf, u_crc32, gbt),   --Bank 225
--    1  => (gbt, u_crc32, buf, no_fmt, buf, u_crc32, gbt),   --Babk 226
    2  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 227
--    3  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 228
--    4  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 229
--    5  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 230  
--    6  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 231 
--    7  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 232
--    8  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 233
--    9  => (lpgbt, u_crc32, buf, no_fmt, buf, u_crc32, lpgbt),   --Bank 234
--    -- Cross-chip
--    10 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 134 -- Left Column
--    11 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 133
--    12 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 132
--    13 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 131
--    14 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 130
--    15 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 129
--    16 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 128
--    17 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Bank 127
    others => kDummyRegion
    );

    -- for data framer (ic_simple, no_ec, n_ec_spare, ec_broadcast)
    constant REGION_DATA_FRAMER_CONF : region_data_framer_conf_array_t := (
    2 => ( 0=>(false, false, 1, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    others => kDummyRegionDataFramer
    );

		-- for gbt
		constant REGION_GBT_CONF : region_gbt_conf_array_t := (
		others => kDummyRegionGbt
		);
    
    -- for lpgbt
   constant REGION_LPGBT_CONF : region_lpgbt_conf_array_t := ( 
     2  => (FEC5, DATARATE_5G12, PCS),
     others => kDummyRegionLpgbt
   );

end emp_project_decl;
-------------------------------------------------------------------------------
