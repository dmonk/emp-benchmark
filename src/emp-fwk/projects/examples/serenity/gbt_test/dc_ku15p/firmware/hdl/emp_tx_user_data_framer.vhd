-- Author : Kirika Uchida
-- Date   : 11/09/2020

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;

--	constant LWORD_WIDTH: integer := 64;
--	type lword is
--		record
--			data: std_logic_vector(LWORD_WIDTH - 1 downto 0); 
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
--  
-- data_i in this example.  (Not 100 % sure how it should be used)
-- I defined the data_i.start = '1' for the first 32 bits of 40 MHz data.
-- data_i.valid = '1' when the data is there.
-- data_i.strobe is ignored.
-- data_i.data(31 downto 0) carry the data to be sent to gbt at user_data field.
-- The first data_i.data with data_i.start = '1' are user_data_o(31 downto 0), the next data are user_data_o(63 downto 32), the third data (17 downto 0) are user_data_o(79 downto 62). 
-- N_EC_SPARE is 0 for this example, so ec_spare_data_i is not filling user_data_o.

-- the user_data_o is going to be latched with clken_o signal, which is a single clock pulse in every 8 clock cycle in clk_p_i domain.

entity emp_tx_user_data_framer is
	generic (
	         INDEX          : integer;
	         CHANNEL_INDEX  : integer := 0;
	    	 N_EC_SPARE     : integer range 0 to 16 := 0
	);
	port (
	    clk_i              : in std_logic;  --- for ipbus (31MHz)
	    rst_i              : in std_logic;  --- for ipbus
	    ipb_in             : in ipb_wbus;   --- ipbus data in (from PC)
	    ipb_out            : out ipb_rbus;  --- ipbus data out (to PC)
        downlink_rdy_i     : in  std_logic;
        clk_p_i            : in  std_logic;
        data_i             : in  lword; 
        ec_spare_data_i    : in  std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);
        clken_o            : out std_logic;
        user_data_o        : out std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0) -- for GBT, data should be at (79 downto 0)
        );   
end emp_tx_user_data_framer;

architecture interface of emp_tx_user_data_framer is    

    signal user_data_s : std_logic_vector(79 downto 0);
					    
begin                 --========####   Architecture Body   ####========-- 

    user_data_o(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 80) <= (others => '0');
    user_data_o(79 downto 0) <= user_data_s;

	process(downlink_rdy_i, clk_p_i)
		variable cntr     : integer range 0 to 2;
		variable timer    : integer range 0 to 7;		
		variable dlatched : boolean;
	begin

		if downlink_rdy_i = '0' then

			cntr  := 0;
			timer := 0;
            user_data_s <= (others => '0');
            clken_o <= '0';

		elsif rising_edge(clk_p_i) then
		                
            clken_o <= '0';
            

            if timer = 7 then
                user_data_o <= (others => '0');
                if dlatched then
                    user_data_o(79 downto 0) <= user_data_s;
                    dlatched := false;
                end if;
                timer := 0;
            else
                timer := timer + 1;
            end if;
            
            if data_i.valid = '1' then
            
                if data_i.start = '1' then
                
                    user_data_s(31 downto 0) <= data_i.data(31 downto 0);
                    cntr := 1;
                elsif cntr /= 0 then
                    
                    if cntr = 1 then
                        user_data_s(63 downto 32) <= data_i.data(31 downto 0);
                         cntr := cntr + 1;
                    elsif cntr = 2 then
                        user_data_s(79 downto 64) <= data_i.data(15 downto 0);
                        dlatched := true;
                        cntr := 0;
                    end if;
                                
                end if;
            
            else
                user_data_s <= (others => '0');
            end if;
            
        end if;

	end process;
        
   
   
end interface;
