-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;


--	constant LWORD_WIDTH: integer := 64;
--	type lword is
--		record
--			data: std_logic_vector(LWORD_WIDTH - 1 downto 0); 
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
--
-- data_i in this example.  (Not 100 % sure how it should be used)
-- I defined the data_i.start = '1' for the first 32 bits of 40 MHz data.
-- data_i.valid = '1' when the data is there.
-- data_i.strobe = '1'.

-- user_data_i(111 downto 0) carry the data from gbt at user_data field.

-- user_data_i are latched at clken_i = '1'
-- latched data are assigned to data_o(31 downto 0) in three clocks.
-- N_EC_SPARE is 0 for this example, so ec_spare_data_i is not filling user_data_o.

--      the user_data_i is in clk_p_i domain with clken_i signal (single clock pulse in every 8 clock).

entity emp_rx_user_data_framer is
    generic ( INDEX             : integer;
              CHANNEL_INDEX     : integer range 0 to 3;
    	      N_EC_SPARE        : integer range 0 to 16 := 0
    );
   port (
        clk_i                        : in std_logic;  --- for ipbus (31MHz)
        rst_i                        : in std_logic;  --- for ipbus
        ipb_in                       : in ipb_wbus;   --- ipbus data in (from PC)
        ipb_out                      : out ipb_rbus;  --- ipbus data out (to PC)  
        clk_p_i                      : in  std_logic;
        uplink_rdy_i                 : in  std_logic;
        clken_i                      : in  std_logic;
        user_data_i                  : in  std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0); -- For GBT, WIDE_BUS data are at (111 downto 0), GBT_FRAME data are at (79 downto 0)
        data_o                       : out  lword;
        ec_spare_data_o              : out std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0)
   );   
end emp_rx_user_data_framer;

architecture interface of emp_rx_user_data_framer is    

    signal data_s           : lword; 
 
begin       

    data_o <= data_s;
    ec_spare_data_o(N_EC_SPARE * 2 - 1 downto 0) <= user_data_i(N_EC_SPARE * 2 - 1 downto 0);
   
    data_proc : process (rst_i, uplink_rdy_i, clk_p_i)
        variable timer            : natural range 0 to 7 := 0;
        variable uplink_data      : std_logic_vector(255 downto 0);
    begin
        if uplink_rdy_i = '0' then

            data_s.start  <= '0';
            data_s.strobe <= '0'; 
            data_s.valid  <= '0';    
            data_s.data   <= (others => '0'); 
            timer := 0;
            
        elsif rising_edge(clk_p_i) then   
 
            data_s.start  <= '0';
            data_s.strobe <= '0';
            data_s.valid  <= '0';
            data_s.data <= (others => '0'); 

            if timer /= 0 then
                data_s.start  <= '0';
                data_s.strobe <= '0';
                data_s.valid  <= '1';
                data_s.data(31 downto 0) <=  uplink_data(32 * (timer+1) - 1 downto 32 * timer);
                if timer = 7 then
                    timer := 0;
                else
                    timer := timer + 1;
                end if;
            end if;
            if clken_i = '1' then
                uplink_data := x"000000" & "00" & user_data_i; -- 24 + 2 + 230
                data_s.start  <= '1';
                data_s.strobe <= '1';
                data_s.valid  <= '1';
                data_s.data(31 downto 0) <= uplink_data(31 downto 0);
                timer := 1;
            end if;
        end if;
    end process;

end interface;
