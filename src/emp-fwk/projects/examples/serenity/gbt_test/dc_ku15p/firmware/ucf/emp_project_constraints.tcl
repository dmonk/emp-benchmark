set_clock_groups -asynchronous -group rxoutclk_out[0]
#set_clock_groups -asynchronous -group rxoutclk_out[1]
#set_clock_groups -asynchronous -group rxoutclk_out[2]
#set_clock_groups -asynchronous -group rxoutclk_out[3]
set_clock_groups -asynchronous -group txoutclk_out[0]
#set_clock_groups -asynchronous -group txoutclk_out[1]
#set_clock_groups -asynchronous -group txoutclk_out[2]
#set_clock_groups -asynchronous -group txoutclk_out[3]

set_clock_groups -asynchronous -group rxoutclk_out[0]_1
set_clock_groups -asynchronous -group rxoutclk_out[0]_2
set_clock_groups -asynchronous -group rxoutclk_out[0]_3
#set_clock_groups -asynchronous -group rxoutclk_out[0]_4
set_clock_groups -asynchronous -group txoutclk_out[0]_1
set_clock_groups -asynchronous -group txoutclk_out[0]_2
set_clock_groups -asynchronous -group txoutclk_out[0]_3
#set_clock_groups -asynchronous -group txoutclk_out[0]_4

#set_clock_groups -asynchronous -group rxoutclk_out[0]_5
#set_clock_groups -asynchronous -group rxoutclk_out[0]_6
#set_clock_groups -asynchronous -group rxoutclk_out[0]_7
#set_clock_groups -asynchronous -group rxoutclk_out[0]_8
#set_clock_groups -asynchronous -group txoutclk_out[0]_5
#set_clock_groups -asynchronous -group txoutclk_out[0]_6
#set_clock_groups -asynchronous -group txoutclk_out[0]_7
#set_clock_groups -asynchronous -group txoutclk_out[0]_8

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
  
set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]


set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]


set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]


set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]


set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
  
set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/reg_no_destuffing_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]



set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]


set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_ec_data_s_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]



#set_multicycle_path 8 -setup \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
#set_multicycle_path 7 -hold \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]

#set_multicycle_path 8 -setup \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]
#set_multicycle_path 7 -hold \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/D}]


#set_multicycle_path 8 -setup \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
#set_multicycle_path 7 -hold \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

#set_multicycle_path 8 -setup \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
#set_multicycle_path 7 -hold \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

#set_multicycle_path 8 -setup \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
#set_multicycle_path 7 -hold \
#-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].uplink_user_data_reg[*]/C}] \
#-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]



set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/write_o_reg/D}]


set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/cnter_reg[*]/R}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/D}]

set_multicycle_path 8 -setup \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]
set_multicycle_path 7 -hold \
-from [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/ongoing_reg/C}] \
-to   [get_pins {datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/data_framers_gen[*].data_framer_inst/slow_command_ctrl_inst/sc_inst/sca_inst/sca_gen[*].rx_inst/sca_deserializer_inst/data_o_reg[*]/CE}]

