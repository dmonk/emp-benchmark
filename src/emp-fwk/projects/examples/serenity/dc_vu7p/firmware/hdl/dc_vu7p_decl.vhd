-- emp_project_decl
--
-- Defines constants for the whole device
--
-- Dave Newbold, June 2014

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.emp_framework_decl.all;
use work.emp_device_types.all;

-------------------------------------------------------------------------------
package emp_project_decl is

  constant PAYLOAD_REV        : std_logic_vector(31 downto 0) := X"12345678";

  -- Number of LHC bunches 
  constant LHC_BUNCH_COUNT    : integer             := 3564;
  -- Latency buffer size
  constant LB_ADDR_WIDTH      : integer             := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer             := 36;
  constant CLOCK_RATIO        : integer             := 9;
  constant CLOCK_AUX_RATIO    : clock_ratio_array_t := (2, 4, 9);
   
  -- Only used by nullalgo   
  constant PAYLOAD_LATENCY    : integer             := 5;

  -- mgt -> chk -> buf -> fmt -> (algo) -> (fmt) -> buf -> chk -> mgt -> clk -> altclk
  constant REGION_CONF : region_conf_array_t := (
     0 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 225 -- Right Column
     1 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 226
     2 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 227
     3 => (no_mgt, u_crc32, buf, no_fmt, buf, u_crc32, no_mgt),   --Quad 228
     4 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 229
     5 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 230  
     6 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 231 
     7 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 232
     8 => (gty25, u_crc32, buf, no_fmt, buf, u_crc32, gty25),   --Quad 233
     -- Cross-chip
     9 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 133 -- Left Column
    10 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 132
    11 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 131
    12 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 130
    13 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 129
    14 => (no_mgt, u_crc32, buf, no_fmt, buf, u_crc32, no_mgt),   --Quad 128
    15 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 127
    16 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 126
    17 => (gty16, u_crc32, buf, no_fmt, buf, u_crc32, gty16),   --Quad 125
    others => kDummyRegion
    );

end emp_project_decl;
-------------------------------------------------------------------------------
