## Generated SDC file "M:/svn_repositories/GBT_project/svn_work/trunk/example_designs/altera_sv/amc40/sdc/amc40_gbt_ref_design.sdc"

## Copyright (C) 1991-2013 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Full Version"

## DATE    "Thu Mar 27 13:32:54 2014"

##
## DEVICE  "5SGXEA7N2F45C3"
##

#**************************************************************
# Time Information
#**************************************************************
set_time_format -unit ns -decimal_places 3

#**************************************************************
# Create Clock
#**************************************************************
create_clock -period 4.166 [get_ports {REF_CLOCK}]
create_clock -period 10 [get_ports {SYS_CLK_100MHz}]

create_clock -name altera_reserved_tck [get_ports {altera_reserved_tck}] -period "24 MHz"
#**************************************************************
# Create Generated Clock
#**************************************************************
derive_pll_clocks

create_generated_clock -name {*gbtExmplDsgn_inst*gatedclock_gen*tmp} -source {*gbtExmplDsgn_inst*gatedclock_gen*RX_WORDCLK} -divide_by 6
#**************************************************************
# Set Clock Latency
#**************************************************************

#**************************************************************
# Set Clock Uncertainty
#**************************************************************

#**************************************************************
# Set Input Delay
#**************************************************************
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tdi]
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tms]
set_output_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tdo]

#**************************************************************
# Set Output Delay
#**************************************************************

#**************************************************************
# Set Clock Groups
#**************************************************************

#**************************************************************
# Set False Path
#**************************************************************
set_false_path -from {*gbt_inst*r_reset}

# DONE_o is a "static" signal that does not need timing constraints
set_false_path -from {*gbt_bank*rxBitSlipControl|DONE_o}
set_false_path -to {*gbt_bank*rxBitSlipControl|DONE_o} 

# RX_RSTONBITSLIP_o computing is based on "static" signal (RX_BITSLIPEVENT_o). 
# No timing constraint to this signal is required.
set_false_path -to {*gbt_bank*rxBitSlipControl|RX_RSTONBITSLIP_o}
set_false_path -from {*gbt_bank*rxBitSlipControl|DONE_o} 

#No timing constraint to the resetAddrManager but only from.
set_false_path -to {*gbt_bank*readControl|resetAddrManager}

#Timnig issue (1 clock cycle metastability is already taken into
#account with the memory offset.
set_false_path -from {*gbt_bank*readControl|resetAddrManager}

#No timing constraints on this reset
set_false_path -from {*gbtExmplDsgn_inst*genReset_s}

#False path to debug instances
set_false_path -from *signaltap*
set_false_path -to *signaltap*
set_false_path  -from *jtagToAvmm_inst*
set_false_path  -to *jtagToAvmm_inst*
set_false_path -to [get_ports {SMA_CLK_OUT}]

#**************************************************************
# Set Multicycle Path
#**************************************************************

#Only when the BCClock clock scheme is used:
set_multicycle_path -from [get_clocks {*frameclk_pll_inst*outclk_0}] -to [get_clocks {*gbt_inst*tx_clkout}] -hold -start 5

#Only when the FULL MGT clock scheme is used:
	#Hold multicycle of 1 to enabled driven destination registers
	set_multicycle_path 5 -from {*gbt_inst*RX_DATA_O*} -end -setup
	set_multicycle_path 4 -from {*gbt_inst*RX_DATA_O*} -end -hold
	
	#Values depends on the RX_GEARBOXSYNCSHIFT_COUNT constant value defined into the alt_ax_gbt_bank_package.vhd file
	set_multicycle_path 3 -from {*gbt_rx_gearbox*reg*} -end -setup
	set_multicycle_path 2 -from {*gbt_rx_gearbox*reg*} -end -hold
	
	#Values depends on the RX_GEARBOXSYNCSHIFT_COUNT constant value defined into the alt_ax_gbt_bank_package.vhd file
	set_multicycle_path 4 -from {*gbt_rx_gearbox*|*ram_block*} -end -setup
	set_multicycle_path 3 -from {*gbt_rx_gearbox*|*ram_block*} -end -hold

#**************************************************************
# Set Maximum Delay
#**************************************************************

#**************************************************************
# Set Minimum Delay
#**************************************************************

#**************************************************************
# Set Input Transition
#**************************************************************

#**************************************************************
# Set Max Skew
#**************************************************************
set_max_skew -from *gx_reset_tx:gxResetTx*tx_digitalreset*r_reset -to *pld_pcs_interface* 2.08