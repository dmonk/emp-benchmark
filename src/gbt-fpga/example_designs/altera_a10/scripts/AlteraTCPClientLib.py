#!/usr/bin/env python

import socket
import logging
import time
from driver_comm import DriverComm
# -------------------------------------------------------------
#  ------------------- Class LPGBT (low-layer) ---------------
# -------------------------------------------------------------


class AlteraTCPClientLib(DriverComm):

    def __init__(self, ip='127.0.0.1', port=8555):
        DriverComm.__init__(self, ip, port)
        self.c_ADDR_OFFSET = 0x00000000 

    def bitfield(self, n):
        arr = [1 if digit=='1' else 0 for digit in bin(n)[2:]]
        return arr[::-1]

    def bitToInt(self, arr):
        out = 0
        for bit in reversed(arr):
            out = (out << 1) | bit
        return out

    def replace(self, a,b,position):
        return a[:position]+b+a[position+len(b):]
    
    def read_issp(self, signal):
        message = "getprobes %d\n" %(signal.getISSPAddr())
        data = self.query(message)
        datalng = int(data, 16)

        probesValArr = self.bitfield(datalng);
        signalArr = probesValArr[signal.getBusPosition():(signal.getBusPosition()+signal.getBusSize())]
        
        return self.bitToInt(signalArr);

    def write_issp(self, signal , value):
        message = "getsources %d\n" %(signal.getISSPAddr())
        data = self.query(message)        
        currDataSources = int(data, 16)

        currDataSourcesBin = self.bitfield(currDataSources);

        #Create array of busLen (to set bus val)
        setArr = []
        for i in range(0, signal.getBusSize()):
            setArr.append(0)
        setArr = self.replace(setArr, self.bitfield(value), 0);

        #Prepare array for new val
        toSource = []
        for i in range(0, signal.getBusPosition() + signal.getBusSize()):
            toSource.append(0)
        toSource = self.replace(toSource, currDataSourcesBin, 0)

        #Insert bus value in current arr
        toSource = self.replace(toSource, setArr, signal.getBusPosition())
        
        dataSource = self.bitToInt(toSource);

        message = "setsources %d 0x%x\n" %(signal.getISSPAddr(), dataSource)
        data = self.query(message)        
        return int(data, 16)
        
        return 0
    
    def write_ctrl_reg(self, reg_addr, reg_value):
        message = "w 0x%0.8X 0x%0.8X\n" % (reg_addr + self.c_ADDR_OFFSET, reg_value)	
        #print("%s" %(message))
        data = self.query(message)
        data = int(data)	
        return data

    def read_ctrl_reg(self, reg_addr):
        message = "r 0x%0.8X\n" % (reg_addr + self.c_ADDR_OFFSET)	
        data = self.query(message)
        data = int(data, 16)	
        return data
